#!/usr/bin/python
""" Iridium ring alert test code."""

import serial
import time

# set up the serial port

port = '/dev/ttyIridium'
baud = 19200

ser = serial.Serial()
ser.port = port
ser.baudrate = baud
ser.parity = serial.PARITY_NONE
ser.stopbits = serial.STOPBITS_ONE
ser.bytesize = serial.EIGHTBITS
ser.timeout = 0.2

try:
    ser.open()
except Exception as e:
    print 'Error opening serial port ' + port
    print str(e)

configure = 0
if configure:
    ser.write('AT\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT +SBDREG?\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT Q0\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT V1\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT E1\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT &K0\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT +SBDST=120\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT +SBDAREG=0\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT +SBDMTA=1\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()
    ser.write('AT +CIER=0,0,0,0,0\r')
    print ser.readline().rstrip()
    print ser.readline().rstrip()

ser.write('AT +SBDSX\r')
print ser.readline().rstrip()
sx = ser.readline().rstrip()
print sx
print ser.readline().rstrip()
print ser.readline().rstrip()
commasplit = sx.split(',')
numwaiting = int(commasplit[-1])
print str(numwaiting)


# ser.write('AT +SBDIXA\r')
# print 'sending SBDRT command ...'
# ser.write('AT +SBDRT\r')
# for i in range(10):
#    print str(i)
#    print ser.readline()

ser.close()
