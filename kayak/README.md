# README #

*Jasmine Nahorniak (CEOAS, Oregon State University)*

### THE PROJECT ###

These are custom scripts created for an AUV project led by Jonathan Nash at Oregon State University.  The AUVs are kayaks with motors, autopilot, and oceanographic sensors on board.  The scripts in this repository are being developed to enable commands to be passed from a nearby research vessel to the AUV kayak over a 900 MHz radio link.  The hardware components involved on the vessel include a Raspberry Pi 2 (the brains), PixHawk (autopilot), 9xtens (modem and antenna).  The software presented here utilizes pymavlink, python and bash.


### How do I get set up? ###

Raspberry Pi 2

1. Install Raspbian on the microSD card (e.g. using NOOBS)
2. From a terminal window on the RPi, run "sudo raspi-config" (to set up the time and keyboard)
3. Install all dependencies listed below

PixHawk

1. Attach a powered USB hub to the RPi (e.g. Plugable USB2-HUB4BC) or other power source.
2. Attach the switch, buzzer and GPS to the PixHawk
3. Attach the PixHawk to the hub using the microUSB cable (do not attach the PixHawk directly to the RPi - the RPi doesn't provide enough USB power)
4. Small LEDs should light, and after a few seconds, main (large) LED should turn on, along with buzzer noise
5. Attach all peripherals (PixHawk, keyboard, mouse, monitor, modem) to the RPi. Do not power on the RPi yet.
6. Power on the monitor.
7. Power on the RPi.

Dependencies on Raspberry Pi 2

* Raspbian
* bash
* python 2.7.9
* pymavlink 1.1.62
* python-opencv python-wxgtk2.8 python-pip python-dev
* python-matplotlib python-numpy python-serial python-pil

Dependencies on Laptop/Desktop

* Matlab
* open port 14550 (for UDP connections)
* Mission Planner (optional)


### CONTACTS ###

* Jasmine Nahorniak (software developer), Oregon State University
* Nick McComb (hardware developer), Oregon State University
* Jonathan Nash (project lead), Oregon State University
