#!/usr/bin/python
"""Main kayak script.
Test keel ctd
"""


# import modules
import subprocess
import time
import sys
import kayak
import serial

print ''
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'
print 'TESTING KEEL CTD'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'

# sleep first to give the pixhawk time to warm up
time.sleep(2)

# user can select which connections are to be made
# conntypes =
# ["pixhawk", "winch", "modem", "gps", "adcp", "pdb", "keelctd", "iridium"]
conntypes = ["keelctd", "modem"]

# initialize the dictionary of parameters to store
# we can add any new parameters we want here
params = {}

# open the connections
for conntype in conntypes:
    print conntype
    try:
        conn = eval('kayak.' + conntype + '_connect()')
        params[conntype] = conn
    except Exception:
        print 'No connection found for: ' + conntype

# set up the deployment folder
params['outfolder'] = kayak.make_output_folder()

# save the keel CTD data (do not forward over antenna)
if 'keelctd' in params:
    print 'Saving the keel CTD data in the background ...'
    # close the connection temporarily;
    # we will open this again in the background script
    params['keelctd'].close()
    with open('/home/ross/logs/keelctd.log', 'w') as foutkeelctd:
        subprocess.Popen(
            ['/home/ross/kayak/kayak_keelctd.py', params['outfolder']],
            stdout=foutkeelctd, stderr=foutkeelctd)

# main loop to get the kayak status and run any commands received
while True:
    try:

        time.sleep(1)

        # transmit the latest keel CTD data
        if 'keelctd' in params:
            try:
                params = kayak.send_keelctd_data(params)
            except Exception as e:
                print 'Unable to transmit keel CTD data.'
                print str(e)

    except KeyboardInterrupt:
        print '\n *** Interrupt received ... exiting. ***'

        for conntype in conntypes:
            conn = params[conntype]
            try:
                conn.close()
            except serial.serialutil.SerialException as e:
                print 'Unable to close connection for : ' + conntype
                print str(e)

        sys.exit()
