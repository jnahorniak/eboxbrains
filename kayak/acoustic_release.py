#!/usr/bin/python
"""
Acoustic release code
Sends acoustic release commands on a repetitive cycle

Jasmine S Nahorniak
Sep 17 2018
"""

import serial
import time


def deckbox_connect():
    """Open the USB (serial) port."""

    # set up the serial port
    ser = serial.Serial()
    ser.port = "/dev/ttyUSB9"
    ser.baudrate = 9600
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port to acoustic release.'
        print str(e)

    return ser


def send_release_command(conn, command):
    """Send a command to the acoustic release."""
    try:
        conn.write(command)
    except Exception:
        print 'Unable to send command.'


conn = deckbox_connect()
# while 1:
send_release_command(conn, 'CM277334\n')
time.sleep(4)
