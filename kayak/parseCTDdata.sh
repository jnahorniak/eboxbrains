#!/bin/bash

# usage:
#   ./parseCTDdata 6 /home/ross/data/CTD/myCTDfile.bin

# location of vendor-provided software that does the actual parsing
ctdpath="/home/ross/CTD/"

if [ $# = 2 ]
then
    numChannels=$1
    binfile=$2
    txtfile=${binfile/.bin/.txt}
    if [[ "$numChannels" -ge 1 && "$numChannels" -le 8 ]]
    then
        cat $binfile | $ctdpath/ParseReader/a.out $numChannels > $txtfile
    else
      	echo "Valid numbers of channels are 1 - 8."
    fi
else
  echo "Provide number of channels and name of binary CTD file to parse."
fi
