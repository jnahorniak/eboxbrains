#!/usr/bin/python

import serial
import os
import time

print "[Shutdown Script Init]"


def pdb_connect():
    """ Opens the modem USB (serial) port. """
    
    # get the port label 
    port = '/dev/ttyPDB'
    baud = '57600'

    # set up the 9XTend serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1
    
    try:  
        ser.open() 
    except Exception:
        print 'Error opening serial port ' + port
        exit()   

    return ser


PDB = pdb_connect()

# TODO: Set LED to green

while 1:
    msg = PDB.readline()
    splitMsg = msg.split('|')
    if len(splitMsg) > 1:
        target = int(splitMsg[-1])
        # print target
        if target == 2:
            print "[! SHUTDOWN !]"
            os.system('touch SYSTEMSHUTDOWN')
            time.sleep(.1)
            # PDB.print(char(33)) # Set status LED to red
            os.system('sudo shutdown -h 0')
        if target == 1:
            print "[! REBOOT !]"
            os.system('touch SYSTEMREBOOT')
            time.sleep(.1)
            os.system('sudo reboot')
    # time.sleep(.5)
