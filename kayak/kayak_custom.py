#!/usr/bin/python
#
# this script is called by kayak_startup_script.py
# it starts kayak_main that need to be running continuously
# the output goes to a screen titled "custom"
# to see the output, type "screen -r"
#
# usage:
#   kayak_custom start
#   kayak_custom stop
#
# modified from code by Nick McComb
# jasmine nahorniak
# oregon state university
# jan 29 2016
# modified to save root screen.0 log output from previous run

import subprocess
import sys
import os
from datetime import datetime

now = datetime.now().strftime('%Y%m%d%H%M%S')
outfile = '/home/ross/logs/screen_root_' + now + '.log'
print outfile

# Test if command line args are used
if len(sys.argv) < 2:
    print "Please use the syntax ( " + sys.argv[0] + " start ) or (" + sys.argv[0] + " stop )."
    quit()

# move any old screen output to a new file
oldscreen = 'screenlog.0'
if os.path.exists(oldscreen):
    subprocess.call(["mv", oldscreen, outfile])
    subprocess.call(["touch", oldscreen])

# If they are, check for (start) or (stop)
if sys.argv[1] == "start":
    print "Starting Kayak Custom."
    subprocess.call(["screen", "-dm", "-L", "-S", "custom", "/home/ross/kayak/kayak_main.py"])
    # subprocess.call(["screen", "-dm", "-S", "shutdown", "/home/ross/kayak/kayak_shutdown_script.py"])

if sys.argv[1] == "stop":
    print "Stopping Kayak Custom."
    subprocess.call(["screen", "-X", "-S", "custom", "kill"])
