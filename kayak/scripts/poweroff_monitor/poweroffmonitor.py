
from os.path import exists
from os import makedirs
from time import sleep
import subprocess
from datetime import datetime
import time
import sys

SCRIPT_START_TIME = time.time()
FIRST_CHECK_TIME = 10  # Number of seconds where if there are no devices after script start, assume debug and exit

PORT_NAMES = {
    "/dev/ttyModem": 0,
    "/dev/ttyPDB": 0,
    "/dev/ttyPixHawk": 0,
    "/dev/ttyWinch": 0
}

NUM_PORTS = len(PORT_NAMES)

LOG_FOLDER = "/home/ross/logs"
LOG_FILENAME = "powerdown_logs.txt"
LOG_FULL_PATH = LOG_FOLDER + "/" + LOG_FILENAME

DEVICE_LIST_COMMAND = "ls"
POWEROFF_COMMANDS = ["/home/ross/kayak/scripts/poweroff_monitor/poweroff_script.sh"]

log_file = None  # type: TextIOWrapper

EXISTS_EXIT_CODE = 0  # This is the exit code from the process meaning the device exists
TIMEOUT_BEFORE_SHUTDOWN = 5  # In seconds


def do_poweroff():
    global log_file

    print("Powering off")
    sys.stdout.flush()

    try:
        log_file.write("POWER OFF: " + str(datetime.now()) + "\n")
        log_file.flush()
        sleep(0.25)
        log_file.close()
        sleep(0.25)
    except Exception as e:
        print("Failed to write to log file. Continuing to power down.")
        print(e)

    try:
        subprocess.Popen(POWEROFF_COMMANDS)
    except Exception as e:
        print("Failed to powerdown. See error below: ")
        print(e)

    exit()


def open_log_file():
    global log_file

    if not exists(LOG_FOLDER):
        makedirs(LOG_FOLDER)

    try:
        log_file = open(LOG_FULL_PATH, "a")
        log_file.write("POWER ON: " + str(datetime.now()) + "\n")
        log_file.flush()
    except Exception as e:
        print("Log file open failed...")
        print("Continuing without logs!!!")
        print(e)


def monitor_state():
    print("Monitor started")

    current_time = time.time()

    for port in PORT_NAMES:
        PORT_NAMES[port] = current_time
        print(port)
        sys.stdout.flush()

    while True:
        missing_devices_count = 0

        for port in PORT_NAMES:
            process = subprocess.Popen(["ls", port], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            process.communicate()

            if process.returncode != EXISTS_EXIT_CODE:
                if (time.time() - PORT_NAMES[port]) >= TIMEOUT_BEFORE_SHUTDOWN:
                    missing_devices_count += 1
                    print("Missing device.")
                    sys.stdout.flush()
            else:
                PORT_NAMES[port] = time.time()

        if missing_devices_count == NUM_PORTS:
            if (time.time() - SCRIPT_START_TIME) <= FIRST_CHECK_TIME:
                print("All ports missing on startup. Assuming debug. Script exiting.")
                sys.stdout.flush()
                exit()

            print("Calling power off.")
            sys.stdout.flush()
            sleep(0.25)
            do_poweroff()

        sleep(0.25)


if __name__ == "__main__":
    open_log_file()
    monitor_state()
