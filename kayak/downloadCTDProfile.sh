#!/bin/bash

# usage:
#   ./downloadCTD 6 /home/ross/data/CTD/
# where the first argument is the number of parameters in the file (6 or 8)

ctdpath="/home/ross/CTD/"

# create a file to indicate that downloading is in progress
touch $ctdpath/LogFiles/ctddownloadinprogress

# wait until the connection with the CTD is established
error=0
pingcount=0
while ! ping -c 1 -W 1 1.2.3.4; do
    sleep 1
    let pingcount=$pingcount+1
    if [ $pingcount -ge 120 ]
      then
       error=1
       touch $ctdpath/LogFiles/noPingFound.txt
       break
    fi
done

if [ $error = 0 ]
 then
  echo "Got ping."
  touch $ctdpath/LogFiles/PingFound.txt
  if [ $# = 2 ]
    then
        numChannels=$1
        outpath=$2
        if [[ "$numChannels" -eq 6 || "$numChannels" -eq 8 ]]
            then
		        packetSize=$((8+(4*numChannels)))
            touch $outpath/combinedProfile.bin
		        profileSize=$(wc --bytes < $outpath/combinedProfile.bin)
		        profileSize=$(((profileSize/packetSize)*packetSize)) #Ensure no extraneous bits produce garbage values
		        echo $profileSize
		        timeStamp=$(date +%Y%m%d%H%M%S)
		        $ctdpath/Downloader/bin/logger2wifidownloader -offset=$profileSize -length=all > $outpath/placeHolder.bin
		        cat $outpath/placeHolder.bin > $outpath/Profile-$timeStamp.bin
		        cat $outpath/placeHolder.bin >> $outpath/combinedProfile.bin
		        cat $outpath/combinedProfile.bin | $ctdpath/ParseReader/a.out $numChannels > $outpath/combinedParsedProfiles.txt
		        cat $outpath/Profile-$timeStamp.bin | $ctdpath/ParseReader/a.out $numChannels > $outpath/parsedProfile-$timeStamp.txt
		        cp $outpath/parsedProfile-$timeStamp.txt $outpath/latestparsedProfile.txt
	    else
          	echo "Valid numbers of channels are 6 or 8."
	    fi
  else
    echo "Provide number of channels and output folder destination."
  fi



else
  echo "Timed out trying to connect to CTD wifi."
fi

rm $ctdpath/LogFiles/ctddownloadinprogress
