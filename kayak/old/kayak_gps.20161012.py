#!/usr/bin/python
"""Collect and save GPS data to a file."""

import sys
import os
from datetime import datetime
import kayak

basefolder = sys.argv[1] + '/GPS'

# create the folder if it doesn't already exist
if not os.path.exists(basefolder):
    os.makedirs(basefolder)

# open the connection to the GPS
try:
    conn = kayak.gps_connect()
    conns = {}
    conns['gps'] = conn
except:
    print 'Unable to connect to GPS.'
    exit()

# mount the thumbdrive if needed
# kayak.mount_thumbdrive()

count = 0
while True:

    # the counter is used to define when to start a new file
    count +=1

    # open/close the output file every x lines
    if count == 1:
        now = datetime.now()
        outfile = basefolder + '/GPS_' + now.strftime("%Y%m%d%H%M%S") + '.log'
        print outfile
        f = open(outfile, 'w', 1)

    gpsline = kayak.get_gps_data(conns)
    f.write(gpsline + '\n')

    # if there are 9 lines per second, an hourly file contains 32400 records
    if count == 32400:
        f.close()
        count = 0
