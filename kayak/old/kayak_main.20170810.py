#!/usr/bin/python
"""Main kayak script.

 1) STATUS
    gets the status from PixHawk using pymavlink
    save the status to a file
    sends the status via 900 MHz to a PC
 2) COMMANDS
    receives kayak commands sent from a PC over 9XTend and attached via serial/USB
    sends the commands to the PixHawk
    returns any results to the PC

 This script runs continuously.

 usage:
   kayak_main.py

 WARNING: this script should NOT be run at the same time as mavproxy;
    it will confuse the pixhawk and require a reboot


 jasmine s nahorniak
 january 29, 2016
 oregon state university
"""


# import modules
import subprocess
import time
import sys
import kayak
import os

print ''
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'
print 'STARTING KAYAK MAIN SCRIPT'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'

# sleep first to give the pixhawk time to warm up
time.sleep(2)

# user can select which connections are to be made
conntypes = ["pixhawk", "winch", "modem", "gps", "adcp", "pdb"]

# initialize the dictionary of parameters to store
# we can add any new parameters we want here
params = {}
params['winchcastsleft'] = 0
params['winchcommand'] = ''
params['winchready'] = 0
params['winchstatus'] = 0
params['winchrev'] = 0
params['winchres'] = 0
params['winchspd'] = 0
params['ctddownloading'] = 'no'
params['winchdir'] = 'stationary'
params['currentspeed'] = 0
params['outfolder'] = ''
params['gpsline'] = 'ERROR'
params['timeset'] = 0
params['kayakspeedtype'] = 'fast'
params['kayakchangespeed'] = 0
params['winchstatuscount'] = 0
params['steeringoverride'] = 0
params['steeringvalue'] = 70 # PDB expects 70 - 86, with 70 being stop

# open the connections
for conntype in conntypes:
    print conntype
    try:
        conn = eval('kayak.' + conntype + '_connect()')
        params[conntype] = conn
    except:
        print 'No connection found for: ' + conntype

# mount the thumbdrive
# kayak.mount_thumbdrive()

# remove the ctddownloadinprogress file if it is still there
downfile = '/home/ross/CTD/LogFiles/downloadinprogress'

if os.path.exists(downfile):
    os.system("rm " + downfile)

# set the system time based on the first GPS datetime received
if 'gps' in params:
    params = kayak.handle_system_time(params)
    # close the GPS serial connection - the next GPS function will open it again
    params['gps'].close()

# set up the deployment folder
params['outfolder'] = kayak.make_output_folder()

# save the GPS data (do not forward over antenna)
# if ('gps' in params) & (params['gpsline'] != 'ERROR'):
if 'gps' in params:
    print 'Saving the GPS data to a file in the background ...'
    with open('/home/ross/logs/GPS.log', 'w') as foutgps:
        subprocess.Popen(['/home/ross/kayak/kayak_gps.py', params['outfolder']], stdout=foutgps, stderr=foutgps)

# save the ADCP data (do not forward over antenna)
if 'adcp' in params:
    print 'Saving the ADCP data in the background ...'
    with open('/home/ross/logs/ADCP.log', 'w') as foutadcp:
        subprocess.Popen(['/home/ross/kayak/kayak_adcp.py', params['outfolder']], stdout=foutadcp, stderr=foutadcp)

# get the initial parameter values from the PixHawk
if 'pixhawk' in params:
    params = kayak.set_initial_parameters(params)

# main loop to get the kayak status and run any commands received
while True:    	    
    try:

        # get the pixhawk status
        if 'pixhawk' in params:
            try:
                params = kayak.check_pixhawk_connection(params)
                params = kayak.get_pixhawk_status(params)
            except Exception as e:
                print 'PixHawk communication failed.'
                print str(e)
        else:
            print 'No pixhawk connection ... exiting.'
            sys.exit()
 
        # run commands sent from the PC
        if 'modem' in params:
            try:
                params = kayak.check_serial_connection(params, 'modem')
                params = kayak.get_serial_message(params)
            except Exception as e:
                print 'Modem communication failed.'
                print str(e)

        # get the winch status
        if 'winch' in params:
            try:
                params = kayak.check_serial_connection(params, 'winch')
                params = kayak.get_winch_status(params)
                params = kayak.manage_kayak_speed_settings(params)
            except Exception as e:
                print 'Winch communication failed.'
                print str(e)

            try:
                params = kayak.manage_winch_commands(params)
            except Exception as e:
                print 'Unable to manage winch command.'
                print str(e)
        
        # pdb 
        if 'pdb' in params:
            try:
                params = kayak.check_serial_connection(params, 'pdb')
                params = kayak.steering_override(params)
            except Exception as e:
                print 'PDB communication failed.'
                print str(e)

    except KeyboardInterrupt:
        print '\n *** Interrupt received ... exiting. ***'

        for conntype in conntypes:
            conn = params[conntype]
            try:
                conn.close()
            except serial.serialutil.serialexception as e:
                print 'Unable to close connection for : ' + conntype
                print str(e)
 
        sys.exit()
