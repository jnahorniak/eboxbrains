#!/usr/bin/python
"""Collect and save ADCP data to a file.

Read in serial ADCP data.

Output two files:
1) raw
2) timestamped with server time.
"""

import sys
import os
from datetime import datetime 
import binascii
import kayak

print 'Collecting ADCP data ...'
sys.stdout.flush()

# open the connection to the ADCP
try:
    conn = kayak.adcp_connect()
    params = {}
    params['adcp'] = conn
    print 'Connected to ADCP.'
except Exception as e:
    print 'Unable to connect to ADCP.'
    print str(e)
    exit()

# mount the thumbdrive if needed
# kayak.mount_thumbdrive()

# create the output folder if it doesn't already exist
basefolder = sys.argv[1] + '/ADCP'
if not os.path.exists(basefolder):
    os.makedirs(basefolder)

# set some initial parameters
current_time = datetime.now()
old_hour = -1
new_hour = current_time.hour

sys.stdout.flush()

# loop continuously
while True:

    # check the adcp serial connection
    try:
        params = kayak.check_serial_connection(params, 'adcp')
    except Exception as e:
        print 'ADCP communication failed.'
        print str(e)

    sys.stdout.flush()

    # create new output files approximately hourly
    if new_hour != old_hour:

        # clear the serial buffer to make sure we're not reading old data from the buffer
        # params['adcp'].flushInput()

        # create new output files
        current_time = datetime.now()
        outfile_raw = basefolder + '/ADCP_raw_' + current_time.strftime("%Y%m%d%H%M%S") + '.bin'
        outfile_timestamped = basefolder + '/ADCP_timestamped_' + current_time.strftime("%Y%m%d%H%M%S") + '.bin'
        print outfile_raw
        f_raw = open(outfile_raw, 'wb', 1)
        f_timestamped = open(outfile_timestamped, 'wb', 1)

    # The data are in binary format.
    # Read and write the data byte by byte so we don't lose anything.
    # Locate the main header ID (7f7f when translated to hex)
    # to know when the ensemble begins, and write a timestamp just after that header
    # in the timestamped version of the file
    hex_new = '00' 
    hex_pair = '0000'
    bcount = 0
    while hex_pair != '7f7f':
        bcount += 1
        hex_old = hex_new
        try:
            databyte = params['adcp'].read(1)
        except Exception as e:
            print 'Unable to read from ADCP.'
            print str(e)
        hex_new = binascii.hexlify(databyte)
        hex_pair = hex_old + hex_new
        f_raw.write(databyte)
        f_timestamped.write(databyte)
        if bcount > 100:
            bcount = 0
            try:
                params = kayak.check_serial_connection(params, 'adcp')
            except Exception as e:
                print 'ADCP communication failed.'
                print str(e)
            sys.stdout.flush()

    # add a timestamp to the timestamped file whenever we see the 7F7F header
    # this adds 7 bytes (year, month, day, hour, minute, second, hundsec)
    current_time = datetime.now()
    yy = current_time.year - 2000
    f_timestamped.write(chr(yy))
    f_timestamped.write(chr(current_time.month))
    f_timestamped.write(chr(current_time.day))
    f_timestamped.write(chr(current_time.hour))
    f_timestamped.write(chr(current_time.minute))
    f_timestamped.write(chr(current_time.second))
    hundsec = int(current_time.microsecond/10000)
    f_timestamped.write(chr(hundsec))

    # save the hours so we know when to create a new hourly file
    old_hour = new_hour
    new_hour = current_time.hour

    # close the files every hour
    if new_hour != old_hour:
        f_raw.close()
        f_timestamped.close()

    # flush the stdout buffer so it prints to the log file
    sys.stdout.flush()
