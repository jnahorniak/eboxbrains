﻿#!/usr/bin/python
"""
Kayak module.

jasmine s nahorniak
dec 11 2015
oregon state university
"""

# import modules
import re  # regular expression
from datetime import datetime
import serial
import subprocess
from pymavlink import mavutil, mavwp, mavparm
import time
import os
import pynmea2
import glob
import string


def readconfig(param):
    """Returns the relevant info from the config file.
    param needs to be one of the parameters listed in the config file, such as
    portlabel or baudrate"""

    # read the config file	
    f = open('/home/pi/kayak/kayak_config.txt', 'r')
    for line in f:
        m = re.match('^' + param + ':(.+)', line)
        if m:
            match = m.group(1)
    f.close()
    # print param + ": " + match
    return match

# this kayak name (in case there are multiple kayaks communicating) 
kayakname = readconfig('kayakname')


def pixhawk_connect():
    """Establish and verify the USB connection to the Pixhawk."""

    # get the port label and baud rate
    nav_port = readconfig('nav_port')
    nav_baud = int(readconfig('nav_baud')) 

    # connection from USB port on PixHawk
    # conn = mavutil.mavlink_connection('/dev/ttyACM0',retries=3)
    # connection from TELEM1 port on PixHawk
    try:
        conn = mavutil.mavlink_connection(nav_port, baud=nav_baud, retries=3)
    except Exception:
        print 'Error opening serial port ' + nav_port
        exit()

    # make sure we get a heartbeat before proceeding
    print 'Waiting for a heartbeat ...'
    conn.wait_heartbeat()
    print 'Got heartbeat.'
 
    return conn


def modem_connect():
    """Open the modem USB (serial) port."""
    
    # get the port label 
    port = readconfig('modem_port')
    baud = int(readconfig('modem_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1
    
    try:  
        ser.open() 
    except Exception:
        print 'Error opening serial port ' + port
        exit()   

    return ser


def PDB_connect():
    ''' opens the Power Distribution Board port '''
    
    # get the port label 
    port = readconfig('PDB_port')
    baud = int(readconfig('PDB_baud'))

    # set up the serial port
    ser=serial.Serial()
    ser.port=port
    ser.baudrate=baud
    ser.parity=serial.PARITY_NONE
    ser.stopbits=serial.STOPBITS_ONE
    ser.bytesize=serial.EIGHTBITS
    ser.timeout=0.1
    
    try:  
        ser.open() 
    except Exception, e:
        print 'Error opening serial port ' + port
        exit()   

    return ser


def winch_connect():
    ''' opens the winch USB (serial) port '''
    
    # get the port and baud 
    port = readconfig('winch_port')
    baud = int(readconfig('winch_baud'))
 
    # set up the serial port
    ser=serial.Serial()
    ser.port=port
    ser.baudrate=baud
    ser.parity=serial.PARITY_NONE
    ser.stopbits=serial.STOPBITS_ONE
    ser.bytesize=serial.EIGHTBITS
    ser.timeout=0.1
    
    try:  
        ser.open() 
    except Exception, e:
        print 'Error opening serial port ' + port
        exit()   

    return ser


def gps_connect():
    ''' opens the GPS USB (serial) port '''

    # get the port and baud
    port = readconfig('gps_port')
    baud = int(readconfig('gps_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1

    try:
        ser.open()
    except Exception, e:
        print 'Error opening serial port ' + port
        exit()

    return ser


def adcp_connect():
    ''' opens the ADCP (serial) port '''
    
    # get the port and baud
    port = readconfig('adcp_port')
    baud = int(readconfig('adcp_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 5

    try:
        ser.open()
    except Exception, e:
        print 'Error opening serial port ' + port
        exit()

    return ser


def conns_connect(conntypes):
    """Connect all serial connections listed in conntypes."""
   
    # initialize the dictionary of connections  
    conns = {}

    # open the connections
    for conntype in conntypes:
        print conntype
        try:
            conn = eval(conntype + '_connect()')
            conns[conntype] = conn
        except:
            print 'No connection found for: ' + conntype

    return conns


def conn_disconnect(conntype, conns):
    """Disconnects the specified serial connection."""

    conn = conns[conntype]
    
    if conn.isOpen():
        print 'Closing the open serial connection for: ' + conntype  
        conn.close()
    else:
        print 'Serial connection already closed: ' + conntype  


def conn_reconnect(conntype, conns):
    """Closes then opens a serial connection."""
 
    try:
        conn_disconnect(conntype, conns)
    except:
        print 'Unable to disconnect serial port for: ' + conntype

    try: 
        newconn = eval(conntype + '_connect()')
        conns[conntype] = newconn
    except:
        print 'Unable to reopen serial port for: ' + conntype
   
    return conns


def conns_reconnect(conntypes, conns):
    """Reconnect all serial connections listed in conntypes."""
   
    # open the connections
    for conntype in conntypes:
        print 'Reconnecting: ' + conntype
        try:
            conns = conn_reconnect(conntype, conns)
        except:
            print 'Unable to reconnect: ' + conntype

    return conns


def meta_message(message,msgtype):
    ''' adds metadata to a message '''    

    # current date and time
    now = datetime.now().strftime('%Y/%m/%d %H:%M:%S UTC')

    # message
    # NOTE: The message must start with the kayak name (e.g. kayak1) 
    # so that the receiving code on the PC recognizes it.
    metamessage = kayakname + " -- " + msgtype + " -- " + now + " -- " + message 
    return metamessage 


def get_serial_message(conns,params):
    ''' opens and reads messages from a serial port '''
    
    modem=conns['modem']  

    # NOTE - don't flush the input; we want to keep any commands that were sent and run them in the order they were received
 
    # read the message arriving on the serial port 
    msg = "" 
    try:
        rcv = modem.readline()
        msg = rcv.decode("utf-8","ignore")
        # msg="kayak1 getparam THR_MAX"
        # msg="kayak1 setparam THR_MAX 30"
        # msg="kayak1 mode MANUAL"
        # msg="kayak1 goto 1 49.126499,-123.2817 49.1249,-123.2789 49.1259,-123.2839"
        # msg="kayak1 listwp"
        # msg="kayak1 winch 24 5 10 255"
        # msg="kayak1 setcurwp 2"
    except Exception as e:
        print 'Unable to read message from modem.'
        print str(e)
 
    if msg=="": 
       print "No command received."
    else: 
       # proceed only if the tag we need is in the command
       print msg 
       if (kayakname in msg): 
          command1=msg.replace(kayakname + " ","",1)
          command=command1.rstrip()
          params=handle_incoming_message(conns,params,command) 

    # return the changed params variable
    return params

def save_message(message):
    ''' saves a message to a file '''
    
    # date 
    fulldate=datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-2]
    fulldate=fulldate+"Z"
    #print fulldate
    shortdate=datetime.now().strftime("%Y%m%dT%H")
   
    # create the filename
    outfile='/home/pi/kayak/kayak_custom_out/kayak_' + shortdate + '.txt'
    
    # open and write to the file	
    f=open(outfile,'a')
    f.write(fulldate + ' ' + message + '\n')
    f.close()
    

def send_serial_message(conns,message):
    ''' sends a message via serial/USB (and 9XTend) to a PC '''
  
    modem=conns['modem']
 
    print message
 
    # add a line feed to the end of the message
    msg = message + '\n'
    print msg 
 
    # send the message 
    print 'Sending message.' 
    try: 
        modem.write(msg.encode('utf-8'))
    except serial.serialutil.SerialException as e:
        print 'Problem with modem connection.' 
        print str(e)
        print str(modem.isOpen())
        # modem.close()
        # modem = modem_connect()
    print 'Message sent.' 

    
def handle_incoming_message(conns,params,message):
    ''' functions to run on the incoming commands '''
   
    # date 
    date=datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-2]
    date=date+"Z"
    print date
   
    # forward message to appropriate function
    print message
    if ("GOTO " in message.upper()):
       set_waypoints(conns,message)
    elif ("MODE " in message.upper()):
       set_mode(conns,message)
    elif ("SETPARAM " in message.upper()):
       set_parameter(conns,message)
    elif ("GETPARAM " in message.upper()):
       get_parameter(conns,message)
    elif ("LISTWP" in message.upper()):
       list_waypoints(conns,message)
    elif ("SETCURWP " in message.upper()):
       set_current_waypoint(conns,message)
    elif ("SETSPEED " in message.upper()):
       set_groundspeed(conns,message)
    elif ("WINCH " in message.upper()):
       params=initialize_winch_commands(params,message)

    # return the changed params variable
    return params

def set_waypoints(conns,command):
    ''' sets new waypoints on the Pixhawk  '''
       
    print 'Setting waypoints ...'

    pixhawk=conns['pixhawk'] 
    
    # set some parameters
    seq = 1  # the waypoint sequence number
    frame = mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT
    radius = 6
    
    # create the waypoint object
    wp = mavwp.MAVWPLoader()
	
    # add the first (dummy) waypoint - the PixHawk ignores this anyway
    # and sets it to the current (home) location 
    #wp.add(mavutil.mavlink.MAVLink_mission_item_message(pixhawk.target_system, pixhawk.target_component, seq, frame, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0,1,0,radius,0,0,0,0,0))
    wp.add(mavutil.mavlink.MAVLink_mission_item_message(pixhawk.target_system, pixhawk.target_component, seq, frame, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 1,1,0,radius,0,0,0,0,0))
    print 'Setting a blank home waypoint as waypoint 0.'
    print 'Adding new requested waypoints ...'

    # pull out the lats and lons from the command
    latlonlist=command.split(' ')

    # next desired waypoint
    nextwp=int(latlonlist[1]);

    # add each waypoint to the waypoint list
    for i in range(2,len(latlonlist)):
	
        seq += 1

        # separate the lat and lon values 
        latlon=latlonlist[i].split(',')
        lat = float(latlon[0])
        lon = float(latlon[1])
        print 'Adding waypoint ' + str(i-1) + ' : LAT ' + str(lat) + ' LON ' + str(lon)

        # set the current waypoint (the next to visit)
        #if i==1+nextwp:
        if i==2:
            curr=1
        else:
            curr=0

        # add each of the waypoints to wp
        wp.add(mavutil.mavlink.MAVLink_mission_item_message(pixhawk.target_system, pixhawk.target_component, seq, frame, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, curr,1,0,radius,0,0,lat,lon,0))
        #wp.add(mavutil.mavlink.MAVLink_mission_item_message(pixhawk.target_system, pixhawk.target_component, seq, frame, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,0,1,0,radius,0,0,lat,lon,0))

    print 'Next waypoint to visit : ' + str(nextwp)

    # delete all of the existing waypoints
    print 'Clearing waypoints ...'
    pixhawk.waypoint_clear_all_send()

    # tell the pixhawk how many waypoints to expect
    print 'Sending ' + str(wp.count()) + ' waypoints.'
    pixhawk.waypoint_count_send(wp.count())

    # send each of the waypoints when requested
    mseq=0 
    while mseq < wp.count()-1:
        msg = pixhawk.recv_match(type=['MISSION_REQUEST'],blocking=True)
        mseq=msg.seq
        pixhawk.mav.send(wp.wp(msg.seq))
        print 'Sending waypoint {0}'.format(msg.seq)
        #print wp.wp(msg.seq)

    
    print 'Completed ... waypoints set.'


def set_current_waypoint(conns,command):
    ''' sets the current waypoint on the Pixhawk  '''
       
    print 'Setting current waypoint ...'

    pixhawk=conns['pixhawk'] 
    
    # pull out the waypoint number from the command
    commandlist=command.split(' ')
    curwp=int(commandlist[1]);

    print 'Next waypoint to visit : ' + str(curwp)
    pixhawk.waypoint_set_current_send(curwp)

    print 'Completed ... current waypoint set.'


def list_waypoints(conns,message):
    ''' lists the waypoints on the Pixhawk  '''
    
    print 'Listing waypoints ...'
   
    pixhawk=conns['pixhawk']
 
    # request the number of waypoints 
    #print 'Requesting the total number of waypoints ...' 
    pixhawk.waypoint_request_list_send()
    countmsg = pixhawk.recv_match(type='MISSION_COUNT',blocking=True,timeout=5)

    wcounter=0
    while countmsg is None:
        wcounter=wcounter+1;
        time.sleep(0.5) 
        if wcounter==10:
            msg='ERROR: Number of waypoints unknown.' 
            metamessage = meta_message(msg,'error')
            send_serial_message(conns,metamessage)
            return 
        else:
            print 'Re-requesting the total number of waypoints ...' 
            pixhawk.waypoint_request_list_send()
            countmsg = pixhawk.recv_match(type='MISSION_COUNT',blocking=True,timeout=5)
   
    #print str(countmsg)
    wpcount=countmsg.count
    print 'Number of waypoints :'  + str(wpcount)
    msg= 'WPCOUNT '  + str(wpcount)
    metamessage = meta_message(msg,'wpcount')
    send_serial_message(conns,metamessage)
   
    # find the current waypoint number 
    wpcurrent = pixhawk.recv_match(type=['MISSION_CURRENT'],blocking=True)
    msg= 'CURWP '  + str(wpcurrent.seq)
    metamessage = meta_message(msg,'curwp')
    send_serial_message(conns,metamessage)
 
    # get the info for each waypoint 
    for seq in range(wpcount):
        #print 'Waypoint ' + str(seq) 
        pixhawk.waypoint_request_send(seq)
       
        wcounter=0; 
        msg = None
        while msg is None:
            wcounter=wcounter+1;
            if wcounter==10:
                msg='ERROR: Waypoint ' + str(seq) + ' not returned.' 
                metamessage = meta_message(msg,'error')
                send_serial_message(conns,metamessage)
                break 
            else:
                print 'Requesting waypoint ' + str(seq) + ' ...' 
                msg=pixhawk.recv_match(type='MISSION_ITEM',blocking=True,timeout=5)
                time.sleep(0.5) 
        
        this_wp = "WP " + str(msg.seq) + " LAT " + str(msg.x) + " LON " + str(msg.y) + " CUR " + str(msg.current)
        metamessage = meta_message(this_wp,'waypt')
        send_serial_message(conns,metamessage) 
    
    print 'Done listing waypoints.'
   

def list_parameters(conns):
    ''' lists the parameters on the Pixhawk  '''
    print 'Listing parameters ...'
   
    pixhawk=conns['pixhawk']
 
    # request the list 
    pixhawk.mav.param_request_list_send(pixhawk.target_system,pixhawk.target_component)
    
    # listen for the list 
    #paramind = 0 
    #while paramind < 402: 
    msg = pixhawk.recv_match(type=['PARAM_VALUE'],blocking=True)
    print msg
    #paramind = msg.param_index
    
    print 'Done listing parameters.'
 

def set_mode(conns,command):
    ''' sets the mode on the Pixhawk  '''
       
    print 'Setting the mode ...'

    pixhawk=conns['pixhawk']

    # list of acceptable input arguments
    inputs=["MANUAL","HOLD","AUTO","GUIDED","LEARNING","RTL","INITIALISING","STEERING"]

    # pull out the desired mode from the command
    modesplit=command.split(' ')
    mode=modesplit[1].upper()
 
    # continue only if an acceptable argument was received
    if mode in inputs:
            print 'Setting mode to ' + mode

	    # set the mode
	    if mode=='MANUAL':
		pixhawk.set_mode_manual()
	    elif mode=='AUTO':
		pixhawk.set_mode_auto()
	    elif mode=='RTL':
		pixhawk.set_mode_rtl()
	    elif mode=='HOLD':
		pixhawk.set_mode('HOLD')
	    elif mode=='INITIALISING':
		pixhawk.set_mode('INITIALISING')
	    elif mode=='STEERING':
		pixhawk.set_mode('STEERING')
	    elif mode=='GUIDED':
		pixhawk.set_mode('GUIDED')
            elif mode=='LEARNING':
                pixhawk.set_mode('LEARNING')

            print 'Mode set.'

    else:
            print 'Mode unknown: ' + mode


def set_parameter(conns,command):
    ''' sets a parameter on the Pixhawk 
        possible parameters include
        WP_RADIUS ''' 
       
    print 'Setting the parameter ...'

    pixhawk=conns['pixhawk']

    # pull out the parameter and value from the command 
    commandsplit = command.split(' ')
    param = commandsplit[1].upper()
    value = commandsplit[2]
    aparam = param.encode('ascii')
 
    # set the parameter value
    p1 = mavparm.MAVParmDict()
    p1.mavset(pixhawk,aparam,value)

    print 'Parameter set.'

def get_parameter(conns,command):
    ''' gets a parameter value from the Pixhawk 
        possible parameters include
        WP_RADIUS ''' 
       
    print 'Getting the parameter ...'
    
    pixhawk=conns['pixhawk']  

    # pull out the parameter and value from the command 
    commandsplit = command.split(' ')
    param = commandsplit[1].upper()
    aparam = param.encode('ascii')
 
    # read the value 
    notreceived=1;
    while notreceived<10:
       try: 
          #pixhawk.mav.param_request_read_send(pixhawk.target_system,pixhawk.target_component,'WP_RADIUS',-1)
          #pixhawk.mav.param_request_read_send(pixhawk.target_system,pixhawk.target_component,'THR_MAX',-1)
          pixhawk.mav.param_request_read_send(pixhawk.target_system,pixhawk.target_component,aparam,-1)
          #time.sleep(0.5)
          value=pixhawk.recv_match(type='PARAM_VALUE',blocking=True)
          gotvalue=str(value.param_value)
          print param + ' value: ' + gotvalue 
          msg=param + ' ' + gotvalue
          mm=meta_message(msg,'param') 
          send_serial_message(conns,mm)   
          notreceived=101 
          print 'Parameter retrieval complete.'
       except Exception:
          print 'Waiting for response ...'
          time.sleep(0.5)
          notreceived=notreceived+1 
               
    if notreceived==10:
        print 'Parameter not returned.'

def get_pixhawk_status(conns,params):
    ''' gets the current status from the pixhawk 
        saves it to a file and sends it via serial '''
   
    pixhawk=conns['pixhawk']  

    # note: there are a total of 16 different types of messages
    # comment out the messages you don't need 
    GPS=pixhawk.recv_match(type='GPS_RAW_INT',blocking=True)
    ATT=pixhawk.recv_match(type='ATTITUDE',blocking=True)
    VFR=pixhawk.recv_match(type='VFR_HUD',blocking=True)
    WPC=pixhawk.recv_match(type='MISSION_CURRENT',blocking=True)
    #SYS=pixhawk.recv_match(type='SYS_STATUS',blocking=True)
    
    # these next few lines are just here for testing purposes, if you want to see all available types of messages
    # for normal operation, all three lines should be commented 
    #for j in range(16):
       #line=pixhawk.recv_match()
       #print line
    
    # clear the serial buffer to get ready for next time  
    pixhawk.port.flushInput()
    
    # make sure we got a good message before proceeding
    if (GPS is not None) and (ATT is not None) and (VFR is not None) and (WPC is not None):

	    # concatonate the messages into one string to send 
	    GPSsum = "TIME " + str(GPS.time_usec) + " LAT " + str(GPS.lat/1.0e7) + " LON " + str(GPS.lon/1.0e7) + \
                     " ALT " + str(GPS.alt/1.0e3) + " SATVIS " + str(GPS.satellites_visible)
	    ATTsum = "ROLL " + "{0:.3f}".format(ATT.roll) + " PITCH " + "{0:.3f}".format(ATT.pitch) + " YAW " + "{0:.3f}".format(ATT.yaw)
	    VFRsum = "SP " + str(VFR.groundspeed) + " HD " + str(VFR.heading) + " TH " + str(VFR.throttle)
	    WPCsum = "CURWP " + str(WPC.seq)
	    status = GPSsum + " " + ATTsum + " " + VFRsum + " " + WPCsum
            # add metadata info to the message 
            metamessage = meta_message(status,'stats')
	 
    else:
            status = 'ERROR: No message received.'
            # add metadata info to the message 
            metamessage = meta_message(status,'error')

    # save the message to a local file
    save_message(metamessage)
    
    # send the message via serial 
    send_serial_message(conns,metamessage) 

    # we haven't changed the params variable, but this is here
    # in case we ever want to add something to params inside this function later
    return params


def winch_command(conns, message):
    """Send the winch command to the winch teensy."""

    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print 'Sending the winch command ...'
    print message  
 
    winch = conns['winch']

    # we first have to split the message into words for the call function to work
    m = message.split()
    print m   
    print int(m[1])
 
    ################# 
    # Robert's code

    parameters = [0, 0, 0, 0, 0, 0, 0]
    stop = int(m[4]) 
    
    if stop == 0:
        print stop 
        header = 255
        speedout = int(m[1])  # Collect runtime parameters
        speedin = int(m[2])
        depth = int(m[3])
        upperdepthbyte = depth >> 8
        lowerdepthbyte = depth & 0xFF
        checksum = (((speedout ^ speedin) ^ upperdepthbyte) ^ lowerdepthbyte)  # XOR all variables to create checksum
        footer = 255
        parameters = [header, speedout, speedin, upperdepthbyte, lowerdepthbyte, checksum, footer]
    elif stop == 1:
        # winch return fast	
        for x in range(len(parameters)):
            parameters[x] = 0xAA
        parameters[5] = 0
    elif stop == 2:
        # winch return slow
        for x in range(len(parameters)):
            parameters[x] = 0xBB
        parameters[5] = 0
    elif stop == 4:
        # winch hold position
        for x in range(len(parameters)):
            parameters[x] = 0xCC
        parameters[5] = 0
    elif stop == 8:
        # boat remote start
        for x in range(len(parameters)):
            parameters[x] = 0xDD
        parameters[5] = 0
    elif stop == 16:
        # boat remote stop
        for x in range(len(parameters)):
            parameters[x] = 0xEE
        parameters[5] = 0

    for x in parameters:
        x = chr(x)  # cast to char to make single byte
        try:
            winch.write(x)
        except Exception as e:
            print 'Unable to write to winch.'
            print str(e)  
        time.sleep(0.1)

    print 'Sent winch command.' 
    print parameters 


def get_winch_status(conns, params):
    """Get the current status from the winch.
    Saves it to a file and sends it via serial to the RPi.
    Returns the ready/notready status (1/0) and download status (yes/no)."""
    
    winch = conns['winch']

    # STATUS 0 (busy) or 1 (ready) Dir direction (up,down,stationary) Rev (number of revolutions) 
    statusline = winch.readline()
    status = statusline.rstrip('\r\n')
    # status = "STATUS 0 Dir up Rev 10003"

    # clear the serial buffer to be ready for next time
    winch.flushInput() 
   
    # initialize parameters 
    winchready = 0 
    winchstatus = 0 
    ctddownloading = 'no'
    
    # download status
    downloadfile = os.path.isfile('/home/pi/CTD/LogFiles/ctddownloadinprogress')
    if downloadfile:
        ctddownloading = 'yes'

    # if status != '':
    if "STATUS" in status:

        # append the download status to the message
        status = status + ' DOWNLOADING ' + ctddownloading

        # add metadata info to the message
        metamessage = meta_message(status, 'winchstatus')

        # save the message to a local file
        save_message(metamessage)

        # send the message via serial
        send_serial_message(conns, metamessage)
    
        # determine the winch status (ready to receive next cast command or not
        if "STATUS 1" in status:
            winchstatus = 1 
            # if the previous status was 0 (busy), 
            # then assume the CTD has just returned to the surface 
            # and is ready for data download
            if not params['winchstatus']:
                ctddownloading = 'start'
            elif (ctddownloading == 'no') and (params['ctddownloading'] == 'yes'):
                ctddownloading = 'finish'
            elif ctddownloading == 'no':
                winchready = 1
    
            params['winchstatus'] = winchstatus
            params['winchready'] = winchready
            params['ctddownloading'] = ctddownloading
        
        if "STATUS 0" in status:
            params['winchstatus'] = 0
            params['winchready'] = 0
            params['ctddownloading'] = 'no'

    # return the changed params variable
    return params


def initialize_winch_commands(params, message):
    """Initialize the winch commands so they can be run when appropriate."""

    print 'Initializing the winch command'

    # split the message
    m = message.split()
    numcasts = int(m[5])
    
    params['winchcommand'] = message
    params['winchcastsleft'] = numcasts
    
    print 'Command: ' + params['winchcommand']
    print 'Casts to run: ' + str(params['winchcastsleft'])

    return params


def manage_winch_commands(conns, params):
    """Manage when to run the winch commands."""

    # if need to run the command without a good status
    # uncomment this first line and comment the next
    # if (params['winchcommand'] != '') & (params['winchcastsleft'] > 0):
    if params['winchready'] & (params['winchcommand'] != '') & (params['winchcastsleft'] > 0):

        # first do a stop and hold 
        # winch_command(conns, 'winch 0 0 0 4')
        # pause 5 seconds to let winch be neutral
        # print 'Sleeping for 5 seconds ...' 
        # time.sleep(5)
        # print 'Sleep done.' 
        # send the actual profile command 
        winch_command(conns, params['winchcommand'])
        # sleep for 2 seconds to make sure profile is underway        
        time.sleep(2)
        params['winchcastsleft'] = params['winchcastsleft']-1
        params['winchready'] = 0
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print 'Running winch command ...'
        print params['winchcommand']  
        print 'Casts left to run: ' + str(params['winchcastsleft'])
        print '******************************************'
        print '******************************************'
        print '******************************************'
        msg = 'winchcasts ' + str(params['winchcastsleft'])
        metamessage = meta_message(msg, 'param')
        send_serial_message(conns, metamessage)
    
    if (params['winchcommand'] != '') & (params['winchcastsleft'] == -1):
        winch_command(conns, params['winchcommand'])
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print 'Interrupted winch with command: ' + params['winchcommand']
        print '******************************************'
        print '******************************************'
        print '******************************************'
        params['winchcastsleft'] = 0
        params['winchready'] = 0
        params['winchcommand'] = ''
    
    # if params['ctddownloading'] == 'start':
    if (params['ctddownloading'] == 'start') & (params['winchcastsleft'] == 0):
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print 'Downloading CTD data ...'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        params['ctddownloading'] = 'yes'
        params['winchready'] = 0
        # create the output CTD folder if it doesn't already exist
        ctdfolder = params['outfolder'] + '/CTD/'
        if not os.path.exists(ctdfolder):
            os.makedirs(ctdfolder)
        # subprocess.Popen(["/home/pi/kayak/downloadCTDProfile.sh","6",ctdfolder])

    if params['ctddownloading'] == 'finish':
        ctdfile = params['outfolder'] + '/CTD/latestparsedProfile.txt'
        # ctd_max_press(conns, ctdfile)
        print '******************************************'
        print 'Downloading complete.'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        params['ctddownloading'] = 'no'
        params['winchready'] = 1
 
    return params

def set_groundspeed(conns,command):
    ''' changes the desired boat groundspeed setting on the Pixhawk  '''
       
    print 'Setting ground speed ...'

    pixhawk=conns['pixhawk'] 
    
    # pull out the desired groundspeed from the command
    scommand=command.split(' ')
    gspeed=float(scommand[1]);
    
    pixhawk.mav.command_long_send(pixhawk.target_system, pixhawk.target_component, mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED,0,1,gspeed,50,1,0,0,0)

    
    print 'Completed ... groundspeed changed.'

def handle_ctd_data(conns,ctdfile):
    ''' pulls out a subset of the CTD data to decrease transfer size -  this is currently not used - instead I am transmitting only the max depth using ctd_max_press '''

    # the interval of datapoints to use
    # set this to 1 if you wish to send every data point  
    step=5 
    
    # the data columns to transmit 
    # note that datetime will always be transmitted    
    # 1: conductivity  : COND
    # 2: temperature  : TEMP 
    # 3: pressure (uncorrected) : UNPRESS
    # 4: air pressure  : AIRP
    # 5: pressure (corrected) : PRESS
    # 6: salinity  : SAL
    colhead=['PRESS','TEMP','SAL']
    colnum=[5,2,6]
           
    # read the CTD file	
    f=open(ctdfile,'r')
    
    # loop over the file
    counter=0;
    for line in f:
       counter=counter+1;
       m = line.split('\t')
       if counter == step:
           counter=0
           datesplit=m[0].split(' ') 
           outline = 'CTDDATE ' + datesplit[0] + ' CTDTIME ' + datesplit[1] 
           for c in range(len(colhead)): 
              outline = outline + ' ' + colhead[c] + ' ' + m[colnum[c]]
           msg=meta_message(outline,'ctd')
           send_serial_message(conns,msg)            
 
    # close the CTD file
    f.close()

def ctd_max_press(conns,ctdfile):
    ''' pulls out only the maximum pressure from the CTD data to decrease transfer size'''
       
    print 'Finding the maximum CTD pressure value from the last download.' 

    try:
        # read the CTD file	
        f=open(ctdfile,'r')
 
        # initialize parameters 
        outdatekeep=''
        outtimekeep=''
        outpresskeep=0 

        # loop over the file
        for line in f:
            m = line.split('\t')
            datesplit=m[0].split(' ')
            outdate = datesplit[0]
            outtime = datesplit[1]
            outpress = m[5]
            outtemp = m[2]
            outsal = m[6]
            if (outpress > outpresskeep):
                outdatekeep=outdate
                outtimekeep=outtime
                outpresskeep=outpress
                outtempkeep=outtemp
                outsalkeep=outsal

        outline = 'CTDDATE ' + outdatekeep + ' CTDTIME ' + outtimekeep + ' PRESS ' + outpresskeep + \
                  ' TEMP ' + outtempkeep + ' SAL ' + outsalkeep
        msg=meta_message(outline,'ctdpressmax')
        # wait before sending message so not too much info is sent at once 
        time.sleep(1) 
        send_serial_message(conns,msg)            
 
        # close the CTD file
        f.close()

    except:
       print 'No CTD data found.' 


def set_system_time(dateandtime):
    """ sets the system time to the input datestamp from the GPS """

    # change the system clock only if the time difference is greater than 2 seconds in either direction
    save_message('Setting the system time ...')

    print 'Setting the system time ...'
    print 'Current system time : ' + str(datetime.now())
    print 'Current GPS time    : ' + str(dateandtime)

    # set the system time
    os.system('sudo date --set="%s"' % str(dateandtime))

    print 'Updated system time : ' + str(datetime.now())
    print 'System time set.'

    save_message('Set server time to GPS time : ' + str(dateandtime))


def parse_gps_rmc(msg):
    """ parses GPS data
    output includes RMC.timestamp, RMC.datestamp, RMC.latitude, RMC.longitude"""

    # initialize the parameter logging whether the time has been set
    time_set=0

    # make sure it's the right kind of GPS data first
    if "$GPRMC" in msg:

        save_message(msg)

        # parse the data string
        RMC_exists = False
        try:
            RMC = pynmea2.parse(msg)
        except:
            errmsg = 'Unable to parse GPRMC string from GPS.'
            print errmsg
            save_message(errmsg)
        else:
            RMC_exists = True

        if RMC_exists:
            if (RMC.datestamp is not None) and (RMC.timestamp is not None):
                RMC.dateandtime = datetime.combine(RMC.datestamp, RMC.timestamp)
                set_system_time(RMC.dateandtime)
                time_set=1
            else:
                errmsg = 'Date and/or time empty in GPRMC string from GPS.'
                print errmsg
                save_message(errmsg)
            if (RMC.latitude != 0.0) and (RMC.longitude != 0.0):
                print 'GPS lat: ' + str(RMC.latitude)
                print 'GPS lon: ' + str(RMC.longitude)
            else:
                errmsg = 'Lat/lon empty in GPRMC string from GPS.'
                print errmsg
                save_message(errmsg)

    return time_set


def get_gps_data(conns):
    """ gets the gps data, parses RMC data, and sets the system clock """

    gps = conns['gps']

    # clear the serial buffer to make sure we're not reading old data from the buffer
    gps.flushInput()

    # wait until a $ is found before reading the rest of the line
    gpsbyte=''
    gcount = 0
    while gpsbyte != '$':
        gpsbyte = gps.read()
        if gpsbyte == '':
            gcount = gcount + 1
        if gcount == 20:
            break      
    if gcount == 20: 
        gpsline = "ERROR"
    else:
        gpsline = gps.readline()
        gpsline = '$' + gpsline.rstrip('\r\n')

    return gpsline


def mount_thumbdrive():
    """ mounts the thumbdrive USB port """

    # it is assumed the thumbdrive will be at this location
    #sdpath = '/dev/sda1'


    flashdrivedir = glob.glob('/dev/sd??')
    flashdrivedir.sort()
    
    if(flashdrivedir[0][-1:].isdigit()):
        print flashdrivedir[0]
    else:
        print "No Flashdrive Found..."

    sdpath = flashdrivedir[0]

    # the desired thumbdrive path
    thumbpath = '/home/pi/thumbdrive/'

    # create the folder if it doesn't already exist
    if not os.path.exists(thumbpath):
        os.makedirs(thumbpath)

    # check if already mounted before mounting
    cmd1='mountpoint -q ' + thumbpath
    notmounted = os.system(cmd1)
    if notmounted:
        cmd2 = 'sudo mount ' + sdpath + ' ' + thumbpath
        try:
            os.system(cmd2)
            print 'Thumbdrive mounted.'
        except:
            print 'Unable to mount thumbdrive at ' + sdpath + ' to ' + thumbpath + '.'
    else:
        print 'Thumbdrive already mounted.'


def make_output_folder():
    '''Create a folder to hold data for this deployment.

    Creates a new folder with an iterated number one higher than the existing folders.'''

    # outpath = '/home/pi/thumbdrive/'
    outpath = '/home/pi/data/'
    if not os.path.exists(outpath):
        os.makedirs(outpath)
    
    try:
        dirlist = glob.glob(outpath + 'deploy???')
        #print dirlist
        dirlist.sort()
        lastdeploy = dirlist[-1]
	#print dirlist
        print lastdeploy
        strnum = string.replace(lastdeploy, outpath + 'deploy','')
        print strnum
        thisdeploynum = int(strnum) + 1
    except Exception:
        thisdeploynum = 1

    newfolder = outpath + 'deploy' + str(thisdeploynum).zfill(3)

    os.makedirs(newfolder)

    print 'Deployment data folder: ' + newfolder

    return newfolder



