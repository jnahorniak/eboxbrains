#!/usr/bin/python
#
# TEST VERSION FOR GPS ONLY
#
# 1) STATUS
#    gets the status from PixHawk using pymavlink
#    save the status to a file
#    sends the status via 900 MHz to a PC
# 2) COMMANDS
#    receives kayak commands sent from a PC over 9XTend and attached via serial/USB
#    sends the commands to the PixHawk
#    returns any results to the PC
#
# This script runs continuously.
#
# usage:
#   kayak_main.py
#
# WARNING: this script should NOT be run at the same time as mavproxy;
#    it will confuse the pixhawk and require a reboot 
#
#
# Code status : IN PROGRESS 
#
# jasmine s nahorniak
# january 29, 2016
# oregon state university

# import modules
import kayak
import time
import sys

# sleep first to give the pixhawk time to warm up
time.sleep(2)

# user can select which connections are to be made
#conntypes=["pixhawk","winch","modem","gps"]
#conntypes=["pixhawk","modem"]
conntypes=["gps"]

# initialize the dictionary of connections
conns={}

# open the connections
try: 
    for conntype in conntypes:
      print conntype
      conn = eval('kayak.' + conntype + '_connect()')  
      conns[conntype] = conn 
except:
    for key in conns:
       conn=conns[key] 
       conn.close()
    sys.exit()

# initialize the dictionary of parameters to store
# we can add any new parameters we want here
params={}
params['winchcastsleft']=0
params['winchcommand']=''
params['winchready']=0
params['winchstatus']=0
params['ctddownloading']='no'


# main loop to get the kayak status and run any commands received 
while True:    	    
    try:

    # print a divider for easy reading purposes
        print ''
        print '*********************************'

        # get the pixhawk status
        stime=time.time()
        params=kayak.get_pixhawk_status(conns,params)
        etime=time.time()
        #print (etime-stime)
 
        # run commands sent from the PC
        stime=time.time()
        params=kayak.get_serial_message(conns,params)
        etime=time.time()
        #print (etime-stime)
         
        # get the winch status
        if 'winch' in conns:
            stime=time.time()
            params=kayak.get_winch_status(conns,params)
            etime=time.time()        
            #print (etime-stime)
            params=kayak.manage_winch_commands(conns,params)

        # get the gps data
        if 'gps' in conns:
            stime = time.time()
            params = kayak.get_gps_data(conns,params)
            etime = time.time()
            # print (etime-stime)
            params = kayak.manage_winch_commands(conns, params)
         
        # run commands sent from the PC
        #stime=time.time()
        #kayak.get_serial_message(conns)
        #etime=time.time()
        #print (etime-stime)

    except KeyboardInterrupt:
        print '\n *** Interrupt received ... exiting. ***'

        for key in conns:
            conn=conns[key]
            conn.close()
 
        sys.exit()
