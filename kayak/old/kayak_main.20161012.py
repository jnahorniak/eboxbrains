#!/usr/bin/python
"""Main kayak script.

 1) STATUS
    gets the status from PixHawk using pymavlink
    save the status to a file
    sends the status via 900 MHz to a PC
 2) COMMANDS
    receives kayak commands sent from a PC over 9XTend and attached via serial/USB
    sends the commands to the PixHawk
    returns any results to the PC

 This script runs continuously.

 usage:
   kayak_main.py

 WARNING: this script should NOT be run at the same time as mavproxy;
    it will confuse the pixhawk and require a reboot


 Code status : IN PROGRESS

 jasmine s nahorniak
 january 29, 2016
 oregon state university
"""


# import modules
import subprocess
import time
import sys
import kayak
import os

# sleep first to give the pixhawk time to warm up
time.sleep(2)

# user can select which connections are to be made
conntypes = ["pixhawk", "winch", "modem", "gps", "adcp"]
# conntypes = ["pixhawk", "winch", "modem"]
# conntypes=["pixhawk","winch","modem","adcp"]
# conntypes=["pixhawk","winch","modem","gps","adcp"]
# conntypes=["pixhawk","modem"]
# conntypes=["gps"]

# initialize the dictionary of connections
conns = {}

# open the connections
for conntype in conntypes:
    print conntype
    try:
        conn = eval('kayak.' + conntype + '_connect()')
        conns[conntype] = conn
    except:
        print 'No connection found for: ' + conntype


# mount the thumbdrive
# kayak.mount_thumbdrive()

# set up the deployment folder on the thumbdrive
outfolder = kayak.make_output_folder()

# initialize the dictionary of parameters to store
# we can add any new parameters we want here
params = {}
params['winchcastsleft'] = 0
params['winchcommand'] = ''
params['winchready'] = 0
params['winchstatus'] = 0
params['ctddownloading'] = 'no'
params['outfolder'] = outfolder
params['connstatus'] = 0

# remove the ctddownloadinprogress file if it is still there
downfile = '/home/pi/CTD/LogFiles/downloadinprogress'
if os.path.exists(downfile):
    os.system("rm " + downfile)

# set the system time based on the first GPS datetime received
time_set = 0
gcount = 0
gpsline = 'ERROR'
if 'gps' in conns:
    while time_set == 0:
        gcount += 1
        if gcount == 1:
            gpserr = 'Waiting for GPS data to set system time.'
            gmsg = kayak.meta_message(gpserr, 'gps')
            kayak.send_serial_message(conns, gmsg)
        if gcount == 20:
            gcount = 0
        stime = time.time()
        gpsline = kayak.get_gps_data(conns)
        print gpsline
        if gpsline != 'ERROR': 
            time_set = kayak.parse_gps_rmc(gpsline)
        else:
            break
        etime = time.time()
        # print (etime-stime)
    conns['gps'].close()

# save the GPS data (do not forward over antenna)
if ('gps' in conns) & (gpsline != 'ERROR'):
    print 'Saving the GPS data to a file in the background ...'
    subprocess.Popen(['/home/pi/kayak/kayak_gps.py', outfolder])

# save the ADCP data (do not forward over antenna)
if 'adcp' in conns:
    print 'Saving the ADCP data in the background ...'
    subprocess.Popen(['/home/pi/kayak/kayak_adcp.py', outfolder])

# main loop to get the kayak status and run any commands received 
while True:    	    
    try:

        # fix serial connections
        if params['connstatus']:
            conns = kayak.conns_reconnect(conntypes, conns)
            params['connstatus'] = 0

        # get the pixhawk status
        if 'pixhawk' in conns:
            stime = time.time()
            try: 
                params = kayak.get_pixhawk_status(conns, params)
            except:
                pass 
            etime = time.time()
            # print (etime-stime)
 
        # run commands sent from the PC
        if 'modem' in conns:
            stime = time.time()
            try:
                params = kayak.get_serial_message(conns, params)
            except:
                pass 
            etime = time.time()
            # print (etime-stime)
         
        # get the winch status
        if 'winch' in conns:
            stime = time.time()
            try:
                params = kayak.get_winch_status(conns, params)
            except:
                pass
            etime = time.time()
            # print (etime-stime)
            params = kayak.manage_winch_commands(conns, params)
         
        # run commands sent from the PC
        # stime=time.time()
        # kayak.get_serial_message(conns)
        # etime=time.time()
        # print (etime-stime)

        if not conns:
            print 'No connections made ... exiting.'
            sys.exit()

    except KeyboardInterrupt:
        print '\n *** Interrupt received ... exiting. ***'

        for key in conns:
            conn = conns[key]
            conn.close()
 
        sys.exit()
