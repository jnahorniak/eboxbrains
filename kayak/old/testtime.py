from datetime import datetime
import pynmea2
import os


def set_system_time(dateandtime):
    """ sets the system time to the input datestamp from the GPS """

    # calculate the difference in seconds between the GPS time and the system time
    timedelta = dateandtime - datetime.now()

    # change the system clock only if the time difference is greater than 2 seconds in either direction
    if abs(timedelta.total_seconds()) > 2:
        #save_message('Setting the system time ...')

        print 'Setting the system time ...'
        print 'Current system time : ' + str(datetime.now())
        print 'Current GPS time    : ' + str(dateandtime)

        print 'Difference in seconds : ' + str(timedelta.total_seconds())

        # set the system time
        os.system('sudo date --set="%s"' % str(dateandtime))

        print 'Updated system time : ' + str(datetime.now())
        print 'System time set.'

        #save_message('Set server time to GPS time : ' + str(dateandtime))



def parse_gps_rmc(msg):
    """ parses GPS data
    output includes RMC.timestamp, RMC.datestamp, RMC.latitude, RMC.longitude"""

    # parse the data string
    RMC_exists = False
    try:
        RMC = pynmea2.parse(msg)
    except:
        errmsg = 'Unable to parse GPRMC string from GPS.'
        print errmsg
        # save_message(errmsg)
    else:
        RMC_exists = True

    if RMC_exists:
        if (RMC.datestamp is not None) and (RMC.timestamp is not None):
            RMC.dateandtime = datetime.combine(RMC.datestamp, RMC.timestamp)
            set_system_time(RMC.dateandtime)
        else:
            errmsg = 'Date and/or time empty in GPRMC string from GPS.'
            print errmsg
            # save_message(errmsg)
        if (RMC.latitude != 0.0) and (RMC.longitude != 0.0):
            print RMC.latitude
            print RMC.longitude
        else:
            errmsg = 'Lat/lon empty in GPRMC string from GPS.'
            print errmsg
            # save_message(errmsg)

        return RMC



# msg = '''$GPRMC,181814.50,A,4433.4690706,N,12317.0380629,W,0.13,108.18,180716,15.3,E,D,C*7B'''
# msg = '''$GPRMC,,V,,,,,,,,,,N,U*2A'''
msg = '''$GPRMC,,,,,,,,N,U*2A'''

print msg

out = parse_gps_rmc(msg)
