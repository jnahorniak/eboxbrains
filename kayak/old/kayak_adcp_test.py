#!/usr/bin/python
"""Collect and save ADCP data to a file.

Read in serial ADCP data.

Output two files:
1) raw
2) timestamped with server time.
"""

import sys
import os
from datetime import datetime 
import binascii
import kayak

print 'Collecting ADCP data ...'
sys.stdout.flush()

# open the connection to the ADCP
try:
    conn = kayak.adcp_connect()
    params = {}
    params['adcp'] = conn
    print 'Connected to ADCP.'
except Exception as e:
    print 'Unable to connect to ADCP.'
    print str(e)
    exit()

# mount the thumbdrive if needed
# kayak.mount_thumbdrive()

sys.stdout.flush()
count = 0
# loop continuously
while True:


    # The data are in binary format.
    # Read and write the data byte by byte so we don't lose anything.
    # Locate the main header ID (7f7f when translated to hex)
    # to know when the ensemble begins, and write a timestamp just after that header
    # in the timestamped version of the file
    # hex_new = '00' 
    # hex_pair = '0000'
    #keepbyte_2 = bin(0)
    #keepbyte_3 = bin(0)
    # while hex_pair != '7f7f':
    #    hex_old = hex_new
    databyte = params['adcp'].read(1)
    hex_new = binascii.hexlify(databyte)
    count = count + 1
    if hex_new == '47':
        print '******************************'
        count = 0
    if count <= 10: 
        print hex_new 
        print databyte 
    #    except Exception as e:
    #        print 'Unable to read from ADCP.'
    #        print str(e)
    #    hex_new = binascii.hexlify(databyte)
    #    hex_pair = hex_old + hex_new
    #    print hex_pair 
        # delay the writing by two bytes 
        #keepbyte_1 = keepbyte_2;
        #keepbyte_2 = keepbyte_3;
        #keepbyte_3 = databyte;
        #f_raw.write(keepbyte_1)
    
    # flush the stdout buffer so it prints to the log file
    sys.stdout.flush()
