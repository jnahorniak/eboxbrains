#!/usr/bin/env python
'''
Script that gets called on startup. Put all custom init code in this file.

Nick
'''

#import os
import subprocess

# set the mode to passthrough if you want mavproxy to start on boot 
#mode = "passthrough"
mode = "custom"
#mode = "donothing"

if mode == "passthrough":
	subprocess.call(["/home/ross/kayak/kayak_mavproxy_bridge.py", "start"])

if mode == "custom":
	subprocess.call(["/home/ross/kayak/kayak_custom.py", "start"])

#if mode == "passthrough":
#	os.system("mavproxy.py --master=/dev/ttyACM0,115200 --out=/dev/ttyUSB0,57600")

#while 1:
#	pass
