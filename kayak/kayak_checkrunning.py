#!/usr/bin/python
#
# checks to see if a kayak script is running 
#   restarts the script if not
#
# usage:
#    kayak_checkrunning.py
#
# jasmine s nahorniak
# aug 17 2016
#

import os 
import subprocess 

running = subprocess.check_output("ps -ef",shell=True)
nowtime = subprocess.check_output("date +'%Y%m%d%H%M%S'",shell=True)
devlist = subprocess.check_output("ls -ltr /dev",shell=True)

# if "SCREEN -dm -S custom" in running:
if "SCREEN -dm -L -S custom" in running:
    # print nowtime 
    pass
else: 
    print ''
    print '********************************' 
    print 'Kayak script is not running ....'
    print nowtime 
    print ''
    print ''
    print running 
    print ''
    print ''
    print devlist 
    print ''
    print ''
    os.system('sudo cp /home/ross/screenlog.0 /home/ross/logs/screenlog.' + nowtime)
    os.system('sudo cp /screenlog.0 /home/ross/logs/screenlog_root_.' + nowtime)
    # os.system('sudo /home/ross/kayak/kayak_startup_script.py')
    # print 'Restarted kayak script.'
    print ''
     
