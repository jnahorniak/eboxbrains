#!/usr/bin/python
"""
Script to stop data logging by the keel CTD.
"""

# import modules
import time
import kayak
import serial
import sys

print ''
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'
print 'STOPPING KEEL CTD'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'
print ''

try:
    conn = kayak.keelctd_connect()
    params = {}
    params['keelctd'] = conn
except Exception:
    print 'No connection found for: keelctd'
    exit()

# empty command to get the CTD's attention
kayak.send_keelctd_command(params, '')
# must wait 10 ms for it to wake up
time.sleep(0.01)
# stop any logging currently in progress
kayak.send_keelctd_command(params, 'stop')
# wait to help clear buffers?
time.sleep(0.01)
# repeat to make sure message received
kayak.send_keelctd_command(params, 'stop')
# wait to help clear buffers?
time.sleep(0.01)
# check the status
kayak.send_keelctd_command(params, 'status')
# wait before closing connection
time.sleep(0.01)

try:
    conn.close()
except serial.serialutil.SerialException as e:
    print 'Unable to close connection for : keelctd'
    print str(e)

sys.stdout.flush()
