#!/usr/bin/python
#
# returns the mavlink version 
#
# usage:
#   kayak_get_mavlink_version.py
#
# Code status : FINAL 
#
# jasmine s nahorniak
# december 4, 2015
# oregon state university

# import modules
from pymavlink import mavutil

if mavutil.mavlink10():
    print 'MavLink version 1.0'
else:
    print 'MavLink version is not 1.0' 

