#!/usr/bin/python
"""Collect and save keel CTD data to a file.

Read in serial keel CTD data.

2018-02-28 21:43:20.333, -0.0142, 20.6531, 9.9966, -0.1358, -0.1350, 0.0000
Column 1: conductivity (mS/cm)
Column 2: temperature (degrees C)
Column 3: pressure (dbar)
Column 4: sea pressure (dbar)  (atmospheric pressure removed)
Column 5: depth (m)
Column 6: salinity (PSU) (PSS-78)

"""

import sys
import os
from datetime import datetime
import kayak

print 'Collecting keel CTD data ...'
sys.stdout.flush()

# open the connection to the keel CTD
try:
    conn = kayak.keelctd_connect()
    params = {}
    params['keelctd'] = conn
    print 'Connected to keel CTD.'
except Exception as e:
    print 'Unable to connect to keel CTD.'
    print str(e)
    exit()

# get the output folder
params['outfolder'] = sys.argv[1]

# create the output folder if it doesn't already exist
basefolder = params['outfolder'] + '/keelctd'
if not os.path.exists(basefolder):
    os.makedirs(basefolder)

# start the keelCTD
params = kayak.start_keelctd(params)

# the output file to hold only the most recent data line
# (to be used to send data back to the main vessel as needed)
latest = basefolder + '/keelctd_latest.txt'
print latest
foutlatest = open(latest, 'w')

# set some initial parameters
old_hour = -1
current_time = datetime.now()
new_hour = current_time.hour

sys.stdout.flush()

# loop continuously
acount = 0
bcount = 0
ccount = 0
while True:

    # check the keelCTD serial connection
    acount = acount + 1
    if acount > 100:
        acount = 0
        try:
            params = kayak.check_serial_connection(params, 'keelctd')
        except Exception as e:
            print 'Keel CTD communication failed.'
            print str(e)
        sys.stdout.flush()

    # create new output files approximately hourly
    if new_hour != old_hour:

        # create new output files
        outfile = basefolder + '/keelctd_' + \
            current_time.strftime("%Y%m%d%H%M%S") + '.txt'
        print outfile
        fout = open(outfile, 'w', 1)

    # The data are in ASCII format.
    # Read and write the data line by line.
    try:
        dataline = params['keelctd'].readline()
        if dataline == '':
            bcount = bcount + 1
            if bcount > 100:
                bcount = 0
                print 'No data are being received from the keel CTD.'
        else:
            fout.write(dataline)
            foutlatest.seek(0, 0)
            foutlatest.write(dataline)
            bcount = 0
    except Exception as e:
        ccount = ccount + 1
        if ccount > 100:
            ccount = 0
            print 'Error reading/writing keel CTD data.'
            print str(e)

    # save the hours so we know when to create a new hourly file
    old_hour = new_hour
    current_time = datetime.now()
    new_hour = current_time.hour

    # close the files every hour
    if new_hour != old_hour:
        fout.close()

    # flush the stdout buffer so it prints to the log file
    sys.stdout.flush()
