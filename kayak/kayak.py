﻿#!/usr/bin/python
"""
Kayak module.

jasmine s nahorniak
dec 12 2016
oregon state university
"""

# import modules
import re  # regular expression
from datetime import datetime, timedelta
import serial
import subprocess
from pymavlink import mavutil, mavwp, mavparm
import time
import os
import pynmea2
import glob
import string
from math import floor
import binascii
import zlib
import struct


def readconfig(param):
    """Returns the relevant info from the config file.
    param needs to be one of the parameters listed in the config file, such as
    portlabel or baudrate"""

    # read the config file
    f = open('/home/ross/kayak/kayak_config.txt', 'r')
    match = ''
    for line in f:
        m = re.match('^' + param + ':(.+)', line)
        if m:
            match = m.group(1)
    f.close()
    # print param + ": " + match

    return match


# this kayak name (in case there are multiple kayaks communicating)
kayakname = readconfig('kayakname')

################################################################
# USB CONNECTIONS ############################################
################################################################


def pixhawk_connect():
    """Establish and verify the USB connection to the Pixhawk."""

    # get the port label and baud rate
    nav_port = readconfig('nav_port')
    nav_baud = int(readconfig('nav_baud'))

    # connection from USB port on PixHawk
    # conn = mavutil.mavlink_connection('/dev/ttyACM0',retries=3)
    # connection from TELEM1 port on PixHawk
    conn = ''
    try:
        conn = mavutil.mavlink_connection(nav_port, baud=nav_baud, retries=3)
    except Exception as e:
        print 'Error opening serial port ' + nav_port
        print str(e)

    # make sure we get a heartbeat before proceeding
    if conn != '':
        print 'Waiting for a heartbeat ...'
        conn.wait_heartbeat()
        print 'Got heartbeat.'

    return conn


def modem_connect():
    """Open the modem USB (serial) port."""

    # get the port label
    port = readconfig('modem_port')
    baud = int(readconfig('modem_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def iridium_connect():
    """Open the iridium modem USB (serial) port."""

    # get the port label
    port = readconfig('iridium_port')
    baud = int(readconfig('iridium_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.2

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def pdb_connect():
    """Open the Power Distribution Board port."""

    # get the port label
    port = readconfig('PDB_port')
    baud = int(readconfig('PDB_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def winch_connect():
    """Open the winch USB (serial) port at the correct baud."""

    # get the port and baud
    port = readconfig('winch_port')
    baud = int(readconfig('winch_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def gps_connect():
    """Open the GPS USB (serial) port."""

    # get the port and baud
    port = readconfig('gps_port')
    baud = int(readconfig('gps_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def adcp_connect():
    """Opens the ADCP (serial) port."""

    # get the port and baud
    port = readconfig('adcp_port')
    baud = int(readconfig('adcp_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 5

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def imu_connect():
    """Opens the IMU (serial) port."""

    # get the port and baud
    port = readconfig('imu_port')
    baud = int(readconfig('imu_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 5

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def met_connect():
    """Opens the met station (serial) port."""

    # get the port and baud
    port = readconfig('met_port')
    baud = int(readconfig('met_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 5

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser


def keelctd_connect():
    """Opens the keel CTD (serial) port."""

    # get the port and baud
    port = readconfig('keelctd_port')
    baud = int(readconfig('keelctd_baud'))

    # set up the serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baud
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS
    ser.timeout = 0.1

    try:
        ser.open()
    except Exception as e:
        print 'Error opening serial port ' + port
        print str(e)

    return ser

################################################################
# CHECKS/FIXES CONNECTIONS ###################################
################################################################


def reconnect_serial(params, conntype):
    """Reconnects a dropped serial connection."""

    print 'Reconnecting serial port for: ' + conntype
    try:
        print 'Closing serial port.'
        params[conntype].close()
    except serial.serialutil.SerialException as e:
        print 'Unable to close serial port.'
        print str(e)

    try:
        conn = eval(conntype + '_connect()')
        params[conntype] = conn
        time.sleep(1)
    except serial.serialutil.SerialException as e:
        print 'Reconnection failed for: ' + conntype
        print str(e)

    return params


def check_pixhawk_connection(params):
    """Check the PixHawk connection."""

    print 'Checking the PiwHawk connection.'

    conn = params['pixhawk']

    status = 1
    ccount = 0
    while status == 1:
        ccount += 1
        if type(conn) is not str:
            try:
                print 'Waiting for a heartbeat ...'
                conn.wait_heartbeat()
                status = 0
                print 'Got heartbeat.'
                print 'PixHawk connection good.'
            except Exception as e:
                print 'No heartbeat received from PixHawk.'
                print str(e)
                status = 1

        if status == 1:
            conn = pixhawk_connect()

        if ccount > 10:
            print 'Unable to reconnect to PixHawk. Failed connecting 10 times.'
            status = 2

    # only update the params variable if the connection was successful
    if status == 0:
        params['pixhawk'] = conn

    return params


def check_serial_connection(params, conntype):
    """Check the serial connection."""

    print 'Checking the serial connection: ' + conntype

    status = 1
    ccount = 0
    while status == 1:
        ccount += 1
        try:
            conn = params[conntype]
            junk = conn.inWaiting()
            status = 0
            print 'Connection to ' + conntype + ' good.'
        except Exception as e:
            print 'Not connected to : ' + conntype
            print str(e)
            status = 1

        if status:
            params = reconnect_serial(params, conntype)

        if ccount > 10:
            print 'Unable to reconnect to ' + conntype + \
                    '. Failed connecting 10 times.'
            break

    return params

################################################################
# IRIDIUM ####################################################
################################################################


def configure_iridium(params):
    """Configures the iridium modem."""

    print ''
    print '********************************'
    print '********************************'
    print ' Configuring the Iridium Modem '
    print '********************************'
    print '********************************'
    # send the configuration commands
    # get the modem's attention
    send_iridium_command_loop(params, 'AT', 'OK')
    # Q0 : return ISU responses (result codes)
    send_iridium_command_loop(params, 'AT Q0', 'OK')
    # V1 : use text result codes, not numeric
    send_iridium_command_loop(params, 'AT V1', 'OK')
    # E0 : do not echo characters when entered
    send_iridium_command_loop(params, 'AT E0', 'OK')
    # &K0 : flow control must be OFF for 3-wire mode
    send_iridium_command_loop(params, 'AT &K0', 'OK')
    # +SBDC : clear MO message sequence number
    # this returns 0 so we're not sending the command
    # as 0 is also an unsolicited message we look for
    # send_iridium_command_loop(params, 'AT +SBDC', '0')
    # +SBDST=120 : set session timeout for SBD commands in seconds
    # ... applies to +SBDIX[A], +SBDREG, +SBDDET
    send_iridium_command_loop(params, 'AT +SBDST=120', 'OK')
    # +SBDAREG=0 : disable automatic registration checks (default)
    send_iridium_command_loop(params, 'AT +SBDAREG=0', 'OK')
    # +SBDMTA=1 : enable SBD ring indications
    send_iridium_command_loop(params, 'AT +SBDMTA=1', 'OK')
    # +CIER=1,0,1 : enable service indication reporting
    send_iridium_command_loop(params, 'AT +CIER=1,1,1', 'OK')

    print '********************************'
    print '********************************'
    print ''


def wait_for_iridium_network(params):
    """Waits until the iridium network is available.
    This should only be run outside of the main loop otherwise it
    will hold everything up."""

    # check for unsolicited messages
    params = check_iridium_response(params, 'junk')

    counter = 0
    while ((params['iridiumnetwork'] == 0) & (counter < 10)):
        counter += 1
        print 'Waiting for iridium network ...'
        time.sleep(0.5)
        params = check_iridium_response(params, 'junk')

    if counter == 10:
        print 'Iridium network still unavailable after 10 tries.  Giving up.'
    else:
        print 'Iridium network available.'


def register_iridium(params):
    """Iridium registration."""

    # +SBDREG : register current location (required for ring alerts)
    loc = current_iridium_location(params)
    command = 'AT +SBDREG=' + loc
    # command = 'AT +SBDREG=4433.472,-12317.058'

    # initialize parameters
    counter = 0
    sent = 0
    result = 0
    delay = 5  # delay in seconds between checks
    maxcounts = 20  # number of tries

    print 'Registering Iridium with command: ' + command

    sent = send_iridium_command(params, command)
    if sent:

        while counter < maxcounts and result == 0:
            counter += 1

            # check for the expected response
            print 'Attempt ' + str(counter) + ' of ' + str(maxcounts) + '.'
            time.sleep(delay)

            params = check_iridium_response_multi(params, '+SBDREG:2,x')
            result = params['iridiumresponsecode']

    if result == 2:
        print 'Registered to Iridium.'
    else:
        print 'Iridium registration failed.'

    print '********************************'
    print '********************************'
    print ''


def manage_iridium_status_messages(params):
    """Send routine status updates via Iridium."""

    # check the time since the last status message was sent
    now = datetime.now()
    minutes_elapsed = (now - params['iridiumlastsent']).total_seconds() / 60.0
    # print 'Minutes elapsed: ' + str(minutes_elapsed)
    # statusmsg = create_iridium_status_message(params)
    # print statusmsg

    # send the status message if it's time
    if minutes_elapsed >= params['iridiuminterval']:
        if params['iridiumnetwork']:
            print '*************************************************'
            print '*************************************************'
            send_iridium_command_loop(params, 'AT +SBDWT', '')
            time.sleep(0.1)
            statusmsg = create_iridium_status_message(params)
            send_iridium_message_text(params, statusmsg)
            print '*************************************************'
            print '*************************************************'
            params['iridiumlastsent'] = now
        else:
            print 'Iridium network not available. Aborted status send.'

    return params


def iridium_sbd_reply(params, message):
    """Send a reply to verify the message was received."""

    if params['iridiumnetwork']:
        print '*************************************************'
        print '*************************************************'
        print '******* Sending confirmation of message receipt. ***********'
        send_iridium_command_loop(params, 'AT +SBDWT', '')
        time.sleep(0.1)
        # truncate the message if needed, we don't need to send all
        if len(message) > 50:
            message = message[:50]
        reply = 'Message received: ' + message
        send_iridium_message_text(params, reply)
        print '*************************************************'
        print '*************************************************'
    else:
        print 'Iridium network not available. Aborted status send.'

    return params


def check_for_waiting_messages(params):
    """Checks to see if any messages are waiting to be downloaded."""

    print '******************************************'
    print '******************************************'
    print '******** Checking for waiting messages with SBDSX. ********'
    send_iridium_command(params, 'AT +SBDSX')
    time.sleep(0.1)
    print '******************************************'
    print '******************************************'

    return params


def number_of_waiting_messages(params, message):
    """Checks how many messages are waiting to be downloaded."""

    print '******************************************'
    print '******************************************'

    commasplit = message.split(',')
    numwaiting = int(commasplit[-1])

    if numwaiting > 0:
        params['iridiumwaiting'] = 1
    else:
        params['iridiumwaiting'] = 0

    print '******** Messages waiting: ' + str(numwaiting)
    print '******************************************'
    print '******************************************'

    return params


def create_iridium_status_message(params):
    """ Create a concise status message for Iridium comms.
    It must be less than 340 characters."""

    # general status (approx 48 chars)
    # ROSS10|023256|56.8241|-132.3843|8|0.63|113|60|5
    msg = kayakname \
        + '|' + datetime.now().strftime('%H%M%S') \
        + '|' + "{:.4f}".format(float(params['LAT'])) \
        + '|' + "{:.4f}".format(float(params['LON'])) \
        + '|' + params['SATVIS'] \
        + '|' + "{:.2f}".format(float(params['SP'])) \
        + '|' + params['HD'] \
        + '|' + params['TH'] \
        + '|' + params['CURWP']

    # append winch status (approx 11 chars)
    # |4|0|u|212
    if 'winch' in params:
        msg = msg \
            + '|W|' + str(params['winchcastsleft']) \
            + '|' + str(params['winchstatus']) \
            + '|' + params['winchdir'][0] \
            + '|' + str(params['winchrev'])

    # append keelctd status (approx 19 chars)
    # |145621|16.28|31.29
    if 'keelctd' in params:
        msg = msg \
            + '|C|' + params['KTIME'][0:2] \
            + params['KTIME'][3:5] + params['KTIME'][6:8] \
            + '|' + "{:.2f}".format(float(params['Temp'])) \
            + '|' + "{:.2f}".format(float(params['Sal']))

    # append adcp status (approx 24 chars)
    # |142319|12.34|8.12|1.09
    if 'adcp' in params:
        u_split = params['ADCPu'].split(',')
        v_split = params['ADCPv'].split(',')
        w_split = params['ADCPw'].split(',')
        msg = msg \
            + '|A|' + params['ATIME'] \
            + '|' + "{:.2f}".format(float(u_split[0])) \
            + '|' + "{:.2f}".format(float(v_split[0])) \
            + '|' + "{:.2f}".format(float(w_split[0]))

    # msg = 'hello'

    return msg

################################################################
# GET MESSAGES ###############################################
################################################################


def meta_message(message, msgtype):
    """Add metadata to a message."""

    # current date and time
    now = datetime.now().strftime('%Y/%m/%d %H:%M:%S UTC')

    # message
    # NOTE: The message must start with the kayak name (e.g. kayak1)
    # so that the receiving code on the PC recognizes it.
    metamessage = kayakname + " -- " + msgtype + " -- " + now + \
        " -- " + message

    return metamessage


def get_serial_message(params):
    """Open and reads messages from a serial port."""

    modem = params['modem']

    # NOTE - don't flush the input; we want to keep any commands that were sent
    # and run them in the order they were received

    # read the message arriving on the serial port
    msg = ''
    try:
        rcv = modem.readline()
        msg = rcv.decode("utf-8", "ignore")
        # msg="kayak1 getparam THR_MAX"
        # msg="kayak1 setparam THR_MAX 30"
        # msg="kayak1 mode MANUAL"
        # msg="kayak1 goto 1 49.126499,-123.2817 49.1249,-123.2789"
        # msg="kayak1 listwp"
        # msg="kayak1 winch 24 5 10 255"
        # msg="kayak1 setcurwp 2"
    except serial.serialutil.SerialException as e:
        print 'Unable to receive message from modem.'
        print str(e)
        # params = reconnect_serial(params, 'modem')

    if msg == "":
        print "No remote command received via modem."
    else:
        print msg
        params = handle_incoming_message(params, msg)

    # return the changed params variable
    return params


def get_iridium_response(params):
    """Reads the response returned from an Iridium modem command."""

    iridium = params['iridium']

    # Relevant solicited responses
    # OK
    # ERROR
    # READY
    # 0
    # +SBDIX:x,x,x,x,x,x
    # +SBDREG:x,x
    # +SBDIXA:x,x,x
    # +CRIS:x,x

    # Unsolicited responses
    # +SBDRING
    # HARDWARE FAILURE: X,X
    # +CIEV=X,X

    # read the message arriving on the serial port
    msg = ''
    try:
        rcv = iridium.readline()
        msg = rcv.rstrip()
    except serial.serialutil.SerialException as e:
        print 'Unable to read response from Iridium modem.'
        print str(e)

    if msg != '':
        print '000000000000000000000000000000000000000000000'
        print '000000000000000000000000000000000000000000000'
        print '********* Iridium response: ' + msg
        print '000000000000000000000000000000000000000000000'
        print '000000000000000000000000000000000000000000000'

    # return the response
    return msg


def request_iridium_message(params):
    """Requests an Iridium SBD message (response to ring alert).
    The message will be transferred from the satellite to the modem buffer."""

    # request the transfer of the SBD from the gateway to the ISU
    # must register our current location at the same time
    if params['iridiumwaiting']:
        if params['iridiumnetwork']:
            print '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
            print '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
            print '$$$$$$$$ Requesting an SBD message from Iridium. $$$$$$$$'
            loc = current_iridium_location(params)
            send_iridium_command_loop(params, 'AT +SBDIXA=' + loc, '')
            params['iridiumwaiting'] = 0
            print '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
            print '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
        else:
            print '$$$$$$$$$$$$ Iridium network not available. $$$$$$$$$$$$$'
            params['iridiumwaiting'] = 1

    return params


def get_iridium_message(params):
    """ Get the message from the Iridium modem buffer. """

    iridium = params['iridium']

    # give the SBDIX command time to send the next two responses below
    time.sleep(0.1)

    try:
        # read three junk lines that follow the main SBDIX response
        junk = iridium.readline()  # blank
        junk = iridium.readline()  # OK
        junk = iridium.readline()  # blank
    except serial.serialutil.SerialException as e:
        print 'Unable to receive message from Iridium modem.'
        print str(e)

    # request the SBD binary message from ISU buffer
    # the buffer can hold only one message at a time
    print '**********************************************'
    print '**********************************************'
    print '******* Sending Iridium command SBDRT ********'
    send_iridium_command_loop(params, 'AT +SBDRT', 'OK')
    print '**********************************************'
    print '**********************************************'

    return params


def extract_iridium_message(binmsg):
    """Extract the message from the binary SBD transmission."""

    ##########################
    # UNTESTED
    ##########################

    # the message is in binary format
    # first two bytes are the message length, high order byte first
    # last two bytes are for the checksum
    #     the checksum is the least signficant two bytes of the summation
    #     high order byte first
    # From the docs:
    # For example if the 9602 were to send the word “hello” encoded in ASCII
    # to the DTE the binary stream would be hex 00 05 68 65 6c 6c 6f 02 14.

    result = 0

    # the message
    middlebytes = binmsg[2:-2]
    textmsg = middlebytes.decode('utf-8')

    # the message length
    firsttwobytes = binmsg[:2]
    length_declared = struct.unpack(">H", firsttwobytes)[0]
    length_received = len(middlebytes)

    # the checksum
    lasttwobytes = binmsg[-2:]
    checksum_declared = struct.unpack(">H", lasttwobytes)[0]
    checksum_received = sum(map(ord, middlebytes))

    # verify the length and checksum
    if length_received == length_declared:
        if checksum_received == checksum_declared:
            result = 1

    if result == 0:
        print 'Iridium message failed validity checks.'
        print 'Message: ' + textmsg
        print 'Checksum declared: ' + str(checksum_declared)
        print 'Checksum received: ' + str(checksum_received)
        print 'Length declared: ' + str(length_declared)
        print 'Length received: ' + str(length_received)
        textmsg = ''

    return textmsg


def save_message(message):
    """Save a message to a file."""

    # date
    fulldate = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-2]
    fulldate += "Z"
    # print fulldate
    shortdate = datetime.now().strftime("%Y%m%dT%H")

    # create the filename
    outfile = '/home/ross/kayak/kayak_custom_out/kayak_' + shortdate + '.txt'

    # open and write to the file
    f = open(outfile, 'a')
    f.write(fulldate + ' ' + message + '\n')
    f.close()


################################################################
# SEND MESSAGES ##############################################
################################################################


def send_serial_message(params, message):
    """Send a message via serial/USB (and 9XTend) to a PC."""

    modem = params['modem']

    print message

    # add a line feed to the end of the message
    msg = message + '\n'

    # send the message
    try:
        modem.write(msg.encode('utf-8'))
        print 'Message sent.'
    except Exception as e:
        print 'Unable to deliver message to modem.'
        print str(e)

    return params


def send_iridium_command(params, command):
    """Send a command to the ISU (onboard Iridium modem)."""

    iridium = params['iridium']

    # add a carriage return to the end of the message
    cmd = command + '\r'

    sent = 0
    try:
        iridium.write(cmd)
        sent = 1
    except Exception as e:
        print 'Unable to deliver command to ISU.'
        print str(e)

    return sent


def send_iridium_command_binary(params, command):
    """Send a command to the ISU (onboard Iridium modem)."""

    ##########################
    # UNTESTED
    ##########################

    iridium = params['iridium']

    cmd = command
    print cmd
    print type(cmd)
    print binascii.hexlify(cmd)

    cmd = "\x68\x65\x6c\x6c\x6f\x02\x14"

    # initialize status of write command
    sent = 0

    try:
        iridium.write(cmd)
        sent = 1
    except Exception as e:
        print 'Unable to deliver command to ISU.'
        print str(e)

    return sent


def check_iridium_response(params, successcode):
    """Check the response from the Iridium modem."""

    # initialize status of command execution
    result = 0
    response = ''

    # get the response if one is expected
    if successcode != '':
        response = get_iridium_response(params)
        params = handle_unsolicited_iridium_messages(params, response)
        # if an unsolicited message is received, check again
        # but give up after 10 tries
        counter = 0
        while params['unsolicited'] and counter < 10:
            # print '******** Unsolicited response received. *********'
            counter += 1
            response = get_iridium_response(params)
            params = handle_unsolicited_iridium_messages(params, response)
        result = get_iridium_resultcode(response, successcode)
    else:
        # we have to assume the command was received
        result = 1

    if response != '':
        if result:
            print 'Response matches.'
        else:
            print 'Response not a match.'
    elif successcode != '':
        print 'No response from the Iridium modem.'

    params['iridiumresponsecode'] = result

    return params


def check_iridium_response_multi(params, successcode):
    """Check the response from the Iridium modem."""

    # initialize status of command execution
    result = 0

    # get the response if one is expected
    if successcode != '':
        response = get_iridium_response(params)
        params = handle_unsolicited_iridium_messages(params, response)
        # if an unsolicited message is received, check again
        # but give up after 10 tries
        counter = 0
        while params['unsolicited'] and counter < 10:
            print '********************************************'
            print '********************************************'
            print 'Unsolicited response received.'
            print 'Listening for command response again.'
            print '********************************************'
            print '********************************************'
            counter += 1
            response = get_iridium_response(params)
            params = handle_unsolicited_iridium_messages(params, response)
        result = get_iridium_resultcode_multi(response, successcode)
    else:
        # we have to assume the command was received
        result = 1

    if result > 0:
        print 'Response matches.'
    else:
        print 'Response not a match.'

    params['iridiumresponsecode'] = result

    return params


def send_iridium_command_loop(params, command, successcode):
    """Sends an Iridium command a maximum of three times
    in the event that the command fails the first few tries."""

    counter = 0
    sent = 0
    result = 0

    while counter < 1 and not sent:
        counter += 1
        print 'Sending Iridium command to ISU: ' + command
        sent = send_iridium_command(params, command)
        if sent:
            params = check_iridium_response(params, successcode)
            result = params['iridiumresponsecode']
            for i in range(4):
                if not result:
                    time.sleep(0.1)
                    params = check_iridium_response(params, successcode)
                    result = params['iridiumresponsecode']

    if not sent:
        print 'Failed to send command to Iridium modem. Aborting.'
    elif not result:
        print 'Failed to get expected response from Iridium modem. Aborting.'

    return result

################################################################
# IRIDIUM UNSOLICITED ########################################
################################################################


def handle_unsolicited_iridium_messages(params, message):
    """Respond to unsolicited messages from the ISU or Iridium gateway."""

    # initialize to true
    unsolicited = 1

    # kayak commands
    kayakprefix = kayakname + ' '
    kayakprelen = len(kayakprefix)

    if message == 'SBDRING':
        print 'Ring alert received via SBDRING.'
        params = handle_ring_alert(params)
    elif message == '+CIEV:1,0':
        print 'Iridium network service is unavailable.'
        params['iridiumnetwork'] = 0
    elif message == '+CIEV:1,1':
        print 'Iridium network service is available.'
        params['iridiumnetwork'] = 1
    elif message == '+CIEV:0,0':
        print 'Iridium signal strength is 0 out of 5 bars.'
    elif message == '+CIEV:0,1':
        print 'Iridium signal strength is 1 out of 5 bars.'
    elif message == '+CIEV:0,2':
        print 'Iridium signal strength is 2 out of 5 bars.'
    elif message == '+CIEV:0,3':
        print 'Iridium signal strength is 3 out of 5 bars.'
    elif message == '+CIEV:0,4':
        print 'Iridium signal strength is 4 out of 5 bars.'
    elif message == '+CIEV:0,5':
        print 'Iridium signal strength is 5 out of 5 bars.'
    elif message[:16] == 'HARDWARE FAILURE':
        print 'Iridium modem hardware failure.'
    elif message[:7] == '+SBDIX:':
        print 'SBDIX response: ' + message
        params = get_iridium_message(params)
    elif message[:7] == '+SBDSX:':
        print 'SBDSX response: ' + message
        params = number_of_waiting_messages(params, message)
    elif message[:kayakprelen] == kayakprefix:
        print '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
        print '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
        print 'Running kayak command received via Iridium.'
        print 'Command: [' + message + ']'
        params = handle_incoming_message(params, message)
        params = check_for_waiting_messages(params)
        params = iridium_sbd_reply(params, message)
        print '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
        print '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
    elif message == 'READY':
        print 'Modem ready to receive message for sending.'
    elif message == '0':
        print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
        print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
        print '******** Modem ready to send message to satellite. ********'
        transfer_iridium_to_satellite(params)
        print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
        print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
    else:
        unsolicited = 0

    params['unsolicited'] = unsolicited

    return params


def handle_ring_alert(params):
    """ Handles any ring alerts received."""

    print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@***********'
    print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@***********'
    print '******** Ring alert received. ********'
    time.sleep(0.1)
    params['iridiumwaiting'] = 1
    params = request_iridium_message(params)
    print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@***********'
    print '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@***********'

    return params


def get_iridium_resultcode(response, successcode):
    """Parse the Iridium response to calculate a result code (0 or 1)."""

    # Example responses from the ISU
    # OK
    # +SBDREG:2,0

    # Example success codes to be used as arguments to this function
    # NOTE: Use x as a placeholder for values if needed
    # OK
    # +SBDREG:2,x  (the value 2 at that location signifies success)

    # initialize to failure (0)
    resultcode = 0

    # separate the prefix from the values (if there are any)
    splitresp = response.split(':')
    splitsucc = successcode.split(':')

    # make sure the prefixes match
    if splitresp[0] == splitsucc[0]:
        resultcode = 1
        # see if there are also values to check
        if len(splitsucc) > 1 and len(splitresp) > 1:
            # make sure the values match
            valsresp = splitresp[1].split(',')
            valssucc = splitsucc[1].split(',')
            for i in range(len(valssucc)):
                # ignore the placeholder characters 'x'
                if valssucc[i] != 'x':
                    # command failed if values don't match
                    if int(valssucc[i]) != int(valsresp[i]):
                        resultcode = 0

    return resultcode


def get_iridium_resultcode_multi(response, successcode):
    """
    Parse the Iridium response to calculate a result code (0, 1 or 2).
    0: prefix does not match
    1: prefix matches, but numbers don't match desired values
    2: prefix and numbers match desired values
    """

    # Example responses from the ISU
    # OK
    # +SBDREG:2,0

    # Example success codes to be used as arguments to this function
    # NOTE: Use x as a placeholder for values if needed
    # OK
    # +SBDREG:2,x  (the value 2 at that location signifies success)

    # initialize to failure (0)
    resultcode = 0

    # separate the prefix from the values (if there are any)
    splitresp = response.split(':')
    splitsucc = successcode.split(':')

    # make sure the prefixes match
    if splitresp[0] == splitsucc[0]:
        resultcode = 1
        # see if there are also values to check
        if len(splitsucc) > 1 and len(splitresp) > 1:
            resultcode = 2
            # make sure the values match
            valsresp = splitresp[1].split(',')
            valssucc = splitsucc[1].split(',')
            for i in range(len(valssucc)):
                # ignore the placeholder characters 'x'
                if valssucc[i] != 'x':
                    # command failed if values don't match
                    if int(valssucc[i]) != int(valsresp[i]):
                        resultcode = 1

    return resultcode


def send_iridium_message_binary(params, message):
    """Send an SBD message via Iridium."""

    ##########################
    # UNTESTED
    ##########################

    print 'Attempting to send SBD message via Iridium ...'
    print message

    result = send_iridium_command_loop(
        params, 'AT +CSQ', '+CSQ:x')

    # compress the message
    msg_binary = compress_message(message)

    # find the length of the message to be transmitted (number of bytes)
    msg_bytes = len(msg_binary)

    # make sure the message length is within acceptable limits for SBD
    if msg_bytes >= 1 and msg_bytes <= 340:

        # calculate and append the checksum to the binary message
        msg_with_checksum = checksum_message(msg_binary)
        print msg_with_checksum
        print type(msg_with_checksum)

        # prepare the MO buffer on the ISU
        result = send_iridium_command_loop(
            params, 'AT +SBDWB=' + str(msg_bytes), 'READY')

        if result:
            # transfer the message to the ISU buffer
            # it checks the checksum upon receipt
            sent = send_iridium_command_binary(params, msg_with_checksum)
            params = check_iridium_response(params, '0')
            # previous command also sends 'OK'
            # junk = check_iridium_response(params, 'OK')
            if params['iridiumresponsecode']:
                # transfer the message from the ISU to the gateway
                # and do SBD registration
                loc = current_iridium_location(params)
                # result = send_iridium_command_loop(
                #    params, 'AT +SBDIX=' + loc, '+SBDIX:0,x,x,x,x,x')

            # clear the MO message buffer
            # NOTE: this buffer will be overwritten next time it is written to
            # regardless
            send_iridium_command_loop(params, 'AT +SBDD0', 'OK')

    else:
        print 'Invalid SBD message length. Transmission aborted.'
        result = 0

    return result


def send_iridium_message_text(params, message):
    """Send an SBD text message via Iridium."""

    print 'Attempting to send SBD message via Iridium ...'
    print message

    # make sure the message length is within acceptable limits for SBD
    # find the length of the message to be transmitted (number of bytes)
    msg_len = len(message)
    print 'Message length: ' + str(msg_len)

    if msg_len > 340:
        print 'Invalid SBD message length. Message truncated.'
        message = message[:340]

    result = 0
    if msg_len >= 1:
        result = send_iridium_command_loop(params, message, '')
        # print 'This is a test. Message was not transmitted.'
    else:
        print 'Empty message. Transmission aborted.'

    return result


def transfer_iridium_to_satellite(params):

    loc = current_iridium_location(params)
    print loc

    command = 'AT +SBDIX=' + loc
    # command = 'AT +SBDIX=+4433.472,-12317.058'

    print 'Iridium network status: ' + str(params['iridiumnetwork'])

    sent = 0
    print '********************************************************'
    print '********************************************************'
    print '********************************************************'
    print 'Sending Iridium message from ISU to satellite ...'
    print command
    sent = send_iridium_command(params, command)
    print 'Send status: ' + str(sent)
    print '********************************************************'
    print '********************************************************'
    print '********************************************************'


def compress_message(message):
    """Compresses a text message."""

    # compress the message as much as we possibly can
    # msg_compressed = zlib.compress(message, 9)
    msg_compressed = message
    print "Length before compression: " + str(len(message))
    print "Length after compression: " + str(len(msg_compressed))

    return msg_compressed


def checksum_message(message):
    """Appends a two-byte checksum to the end of a message
    as required for Iridium SBD messages."""

    # From the docs:
    # The checksum is the least significant 2-bytes of the summation of the
    # entire SBD message. The high order byte must be sent first. For example
    # if the DTE were to send the word “hello” encoded in ASCII to the 9602
    # the binary stream would be hex 68 65 6c 6c 6f 02 14.

    # calculate the checksum as an integer
    checksum_integer = sum(map(ord, message))

    # convert the checksum to at least two bytes of binary
    checksum_binary = format(checksum_integer, 'b').zfill(16)

    # extract the two least significant bytes of the checksum
    checksum_twobyte = checksum_binary[-16:]

    # convert the message to hex
    msg_hex = binascii.hexlify(message)

    # convert the two-byte checksum to hex
    checksum_hex = '%04X' % int(checksum_twobyte, 2)

    # append the checksum to the message (in hex)
    msg_with_checksum_hex = msg_hex + checksum_hex

    print msg_with_checksum_hex

    # convert the result to binary (ready to send over serial)
    # msg_with_checksum_bin = msg_with_checksum_hex.decode("hex")
    # msg_with_checksum_bin = binascii.unhexlify(msg_with_checksum_hex)
    msg_with_checksum_bin = bytearray.fromhex(msg_with_checksum_hex)

    return msg_with_checksum_bin


def handle_incoming_message(params, msg):
    """Functions to run on the incoming commands."""

    if kayakname in msg:

        # remove the kayak name from the message
        message1 = msg.replace(kayakname + " ", "", 1)
        message = message1.rstrip()

        # date
        date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-2]
        date += "Z"
        print date

        # forward message to appropriate function
        print message
        if "GOTO " in message.upper():
            set_waypoints(params, message)
        elif "MODE " in message.upper():
            set_mode(params, message)
        elif "SETPARAM " in message.upper():
            params = set_parameter(params, message)
        elif "GETPARAM " in message.upper():
            get_parameter(params, message)
        elif "SETKAYAK " in message.upper():
            params = set_kayak(params, message)
        elif "GETKAYAK " in message.upper():
            get_kayak(params, message)
        elif "LISTWP" in message.upper():
            list_waypoints(params, message)
        elif "SETCURWP " in message.upper():
            params = set_current_waypoint(params, message)
        elif "SETSPEED " in message.upper():
            set_groundspeed(params, message)
        elif "WINCH " in message.upper():
            params = initialize_winch_commands(params, message)
        elif "WINCHCAL " in message.upper():
            calibrate_winch(params, message)
        elif "WINCHSET " in message.upper():
            set_winch_cal(params, message)
        elif "SERVO " in message.upper():
            steering_setup(params, message)

    # return the changed params variable
    return params


def set_waypoints(params, command):
    """Set new waypoints on the Pixhawk."""

    print 'Setting waypoints ...'

    pixhawk = params['pixhawk']

    # set some parameters
    frame = mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT
    radius = 6

    # create the waypoint object
    wp = mavwp.MAVWPLoader()

    print 'Adding requested waypoints.'
    print 'The home waypoint will be set to be the same as waypoint 1.'

    # pull out the lats and lons from the command
    latlonlist = command.split(' ')

    # next desired waypoint
    nextwp = int(latlonlist[1])

    # add each waypoint to the waypoint list
    seq = 1  # the waypoint sequence number
    for i in range(1, len(latlonlist)):

        # set the home location to be the first waypoint
        # which is the third item in the command
        if i == 1:
            latlon = latlonlist[2].split(',')
        else:
            latlon = latlonlist[i].split(',')

        lat = float(latlon[0])
        lon = float(latlon[1])
        print 'Adding waypoint ' + str(i-1) + ' : LAT ' + str(lat) + \
            ' LON ' + str(lon)

        # set the current waypoint (the next to visit)
        # if i==1+nextwp:
        if i == 2:
            curr = 1
        else:
            curr = 0

        # add each of the waypoints to wp
        wp.add(mavutil.mavlink.MAVLink_mission_item_message(
            pixhawk.target_system, pixhawk.target_component,
            seq, frame, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
            curr, 1, 0, radius, 0, 0, lat, lon, 0))

        seq += 1

    print 'Next waypoint to visit : ' + str(nextwp)

    # delete all of the existing waypoints
    print 'Clearing waypoints ...'
    try:
        pixhawk.waypoint_clear_all_send()
    except Exception as e:
        print 'Lost connection to PixHawk.'
        print str(e)
        # params = reconnect_serial(params, 'pixhawk')
        # pixhawk = params['pixhawk']
        # pixhawk.waypoint_clear_all_send()

    # tell the pixhawk how many waypoints to expect
    print 'Sending ' + str(wp.count()) + ' waypoints.'
    try:
        pixhawk.waypoint_count_send(wp.count())
    except Exception as e:
        print 'Lost connection to PixHawk.'
        print str(e)
        # params = reconnect_serial(params, 'pixhawk')
        # pixhawk = params['pixhawk']
        # pixhawk.waypoint_count_send(wp.count())

    # send each of the waypoints when requested
    mseq = 0
    while mseq < wp.count() - 1:
        msg = pixhawk.recv_match(type=['MISSION_REQUEST'], blocking=True)
        mseq = msg.seq
        pixhawk.mav.send(wp.wp(msg.seq))
        print 'Sending waypoint {0}'.format(msg.seq)
        # print wp.wp(msg.seq)

    print 'Completed ... waypoints set.'

    return params


def set_current_waypoint(params, command):
    """Set the current waypoint on the Pixhawk."""

    print 'Setting current waypoint ...'

    pixhawk = params['pixhawk']

    # pull out the waypoint number from the command
    commandlist = command.split(' ')
    curwp = int(commandlist[1])

    print 'Next waypoint to visit : ' + str(curwp)
    try:
        pixhawk.waypoint_set_current_send(curwp)
        # params['CURWP'] = str(curwp)
        print 'Completed ... current waypoint set.'
    except Exception as e:
        print 'Lost connection to PixHawk.'
        print str(e)
        # params = reconnect_serial(params, 'pixhawk')

    return params


def list_waypoints(params, message):
    """List the waypoints on the Pixhawk."""

    print 'Listing waypoints ...'

    pixhawk = params['pixhawk']

    # request the number of waypoints
    # print 'Requesting the total number of waypoints ...'
    try:
        pixhawk.waypoint_request_list_send()
    except Exception as e:
        print 'Lost connection to PixHawk.'
        print str(e)
        # params = reconnect_serial(params, 'pixhawk')
        # pixhawk = params['pixhawk']
        # pixhawk.waypoint_request_list_send()

    countmsg = pixhawk.recv_match(
        type='MISSION_COUNT', blocking=True, timeout=5)

    wcounter = 0
    while countmsg is None:
        wcounter += 1
        time.sleep(0.5)
        if wcounter == 10:
            msg = 'ERROR: Number of waypoints unknown.'
            metamessage = meta_message(msg, 'error')
            params = send_serial_message(params, metamessage)
            return
        else:
            print 'Re-requesting the total number of waypoints ...'
            pixhawk.waypoint_request_list_send()
            countmsg = pixhawk.recv_match(
                type='MISSION_COUNT', blocking=True, timeout=5)

    # print str(countmsg)
    wpcount = countmsg.count
    params['wpcount'] = wpcount
    print 'Number of waypoints :' + str(wpcount)
    msg = 'WPCOUNT ' + str(wpcount)
    metamessage = meta_message(msg, 'wpcount')
    params = send_serial_message(params, metamessage)

    # find the current waypoint number
    wpcurrent = pixhawk.recv_match(type=['MISSION_CURRENT'], blocking=True)
    msg = 'CURWP ' + str(wpcurrent.seq)
    metamessage = meta_message(msg, 'curwp')
    params = send_serial_message(params, metamessage)

    # get the info for each waypoint
    for seq in range(wpcount):
        # print 'Waypoint ' + str(seq)
        pixhawk.waypoint_request_send(seq)

        wcounter = 0
        msg = None
        while msg is None:
            wcounter += 1
            if wcounter == 10:
                msg = 'ERROR: Waypoint ' + str(seq) + ' not returned.'
                metamessage = meta_message(msg, 'error')
                params = send_serial_message(params, metamessage)
                break
            else:
                print 'Requesting waypoint ' + str(seq) + ' ...'
                msg = pixhawk.recv_match(
                    type='MISSION_ITEM', blocking=True, timeout=5)
                time.sleep(0.5)

        this_wp = "WP " + str(msg.seq) + " LAT " + str(msg.x) + " LON " + \
            str(msg.y) + " CUR " + str(msg.current)
        metamessage = meta_message(this_wp, 'waypt')
        params = send_serial_message(params, metamessage)

    print 'Done listing waypoints.'

    return params


def current_iridium_location(params):
    """Returns the current location formatted for Iridium comms."""

    ##########################
    # UNTESTED
    ##########################

    # Format : lat, lon
    # +/-DDMM.MMM,+/-dddmm.mmm
    # degrees, minutes, thousands of minutes

    # LAT and LON are stored as decimal values (strings)
    # convert them to floats like DDDMM.MMMMMMMMMMM
    latDM = DD2DM(params['LAT'])
    lonDM = DD2DM(params['LON'])

    # format the output as required
    lat = "%+09.3f" % latDM
    lon = "%+010.3f" % lonDM

    loc = lat + ',' + lon

    return loc


def DD2DM(DD):
    """Converts decimal degrees to degrees and decimal minutes (dddmm.mmm).
    Returns a float."""

    ##########################
    # UNTESTED
    ##########################

    # make sure the input is a float
    decdeg = float(DD)

    # find the sign
    if decdeg < 0:
        degsign = -1
    else:
        degsign = 1

    # calculate the decimal minutes
    absdecdeg = abs(decdeg)
    degs = floor(absdecdeg)
    decmins = (absdecdeg - degs) * 60

    DM = degsign * ((degs * 100) + decmins)

    return DM


def list_parameters(params):
    """List the parameters on the Pixhawk. - UNUSED"""

    print 'Listing parameters ...'

    pixhawk = params['pixhawk']

    # request the list
    try:
        pixhawk.mav.param_request_list_send(
            pixhawk.target_system, pixhawk.target_component)
    except Exception as e:
        print 'Lost connection to PixHawk.'
        print str(e)

    # listen for the list
    msg = ''
    try:
        msg = pixhawk.recv_match(type=['PARAM_VALUE'], blocking=True)
    except Exception as e:
        print 'Lost connection to PixHawk.'
        print str(e)

    print msg

    print 'Done listing parameters.'

    return params


def set_mode(params, command):
    """Set the mode on the Pixhawk."""

    print 'Setting the mode ...'

    pixhawk = params['pixhawk']

    # list of acceptable input arguments
    inputs = ["MANUAL", "HOLD", "AUTO", "GUIDED", "LEARNING", "RTL",
              "INITIALISING", "STEERING"]

    # pull out the desired mode from the command
    modesplit = command.split(' ')
    mode = modesplit[1].upper()

    # continue only if an acceptable argument was received
    if mode in inputs:
        print 'Setting mode to ' + mode

        # set the mode
        if mode == 'MANUAL':
            pixhawk.set_mode_manual()
        elif mode == 'AUTO':
            pixhawk.set_mode_auto()
        elif mode == 'RTL':
            pixhawk.set_mode_rtl()
        elif mode == 'HOLD':
            pixhawk.set_mode('HOLD')
        elif mode == 'INITIALISING':
            pixhawk.set_mode('INITIALISING')
        elif mode == 'STEERING':
            pixhawk.set_mode('STEERING')
        elif mode == 'GUIDED':
            pixhawk.set_mode('GUIDED')
        elif mode == 'LEARNING':
            pixhawk.set_mode('LEARNING')

        params['MODE'] = mode
        print 'Mode set.'

    else:
        print 'Mode unknown: ' + mode

    return params


def set_parameter(params, command):
    """Set a parameter on the Pixhawk;
    possible parameters include WP_RADIUS."""

    print 'Setting the parameter ...'

    pixhawk = params['pixhawk']

    # pull out the parameter and value from the command
    commandsplit = command.split(' ')
    param = commandsplit[1].upper()
    value = commandsplit[2]
    aparam = param.encode('ascii')

    # set the parameter value
    p1 = mavparm.MAVParmDict()
    p1.mavset(pixhawk, aparam, value)

    # store the parameter value in the params variable
    params[param] = value

    print 'Parameter ' + param + ' set to ' + value

    return params


def set_kayak(params, command):
    """Set a parameter used in the main kayak code;
    possible parameters include iridiuminterval."""

    print 'Setting the parameter ...'

    # pull out the parameter and value from the command
    commandsplit = command.split(' ')
    param = commandsplit[1]
    value = commandsplit[2]

    # convert the value (a string) to the appropriate type
    if isinstance(params[param], str):
        value_typed = value
    elif isinstance(params[param], int):
        value_typed = int(value)
    elif isinstance(params[param], float):
        value_typed = float(value)

    # store the parameter value in the params variable
    params[param] = value_typed

    print 'Parameter ' + param + ' set to ' + value

    return params


def get_parameter(params, command):
    """Get a parameter value from the Pixhawk;
    possible parameters include WP_RADIUS"""

    print 'Getting the parameter ...'

    pixhawk = params['pixhawk']

    # pull out the parameter and value from the command
    commandsplit = command.split(' ')
    param = commandsplit[1].upper()
    aparam = param.encode('ascii')

    # read the value
    notreceived = 1
    while notreceived < 10:
        try:
            pixhawk.mav.param_request_read_send(
                pixhawk.target_system, pixhawk.target_component, aparam, -1)
            # time.sleep(0.5)
            value = pixhawk.recv_match(type='PARAM_VALUE', blocking=True)
            gotvalue = str(value.param_value)
            print param + ' value: ' + gotvalue
            msg = param + ' ' + gotvalue
            mm = meta_message(msg, 'param')
            params = send_serial_message(params, mm)
            params[param] = gotvalue
            notreceived = 101
            print 'Parameter retrieval complete.'
        except Exception:
            print 'Waiting for response ...'
            time.sleep(0.5)
            notreceived += 1

    if notreceived == 10:
        print 'Parameter not returned.'

    return params


def get_kayak(params, command):
    """Get the value of a parameter used in the main kayak code;
    possible parameters include iridiuminterval."""

    print 'Getting the parameter value ...'

    # pull out the parameter and value from the command
    commandsplit = command.split(' ')
    param = commandsplit[1]

    # get the parameter value
    value = params[param]

    print param + ' value: ' + str(value)
    msg = param + ' ' + str(value)
    mm = meta_message(msg, 'param')
    params = send_serial_message(params, mm)

    return params


def set_initial_parameters(params):
    """Initialize needed parameter values on the Pixhawk."""

    print 'Setting initial parameter values on the PixHawk ...'

    params = set_parameter(params, 'SETPARAM THR_MAX 60')
    params = set_parameter(params, 'SETPARAM THR_MIN 10')
    params = set_parameter(params, 'SETPARAM WP_RADIUS 2')
    params['currentspeed'] = params['THR_MAX']

    return params


def get_pixhawk_status(params):
    """Get the current status from the pixhawk
    saves it to a file and sends it via serial."""

    pixhawk = params['pixhawk']

    # note: there are a total of 16 different types of messages
    # comment out the messages you don't need
    gps = pixhawk.recv_match(type='GPS_RAW_INT', blocking=True)
    att = pixhawk.recv_match(type='ATTITUDE', blocking=True)
    vfr = pixhawk.recv_match(type='VFR_HUD', blocking=True)
    wpc = pixhawk.recv_match(type='MISSION_CURRENT', blocking=True)
    # sys = pixhawk.recv_match(type='SYS_STATUS', blocking=True)

    # these next few lines are just here for testing purposes,
    # if you want to see all available types of messages
    # for normal operation, all three lines should be commented
    # for j in range(16):
    #   line = pixhawk.recv_match()
    #   print line

    # clear the serial buffer to get ready for next time
    pixhawk.port.flushInput()

    # make sure we got a good message before proceeding
    if (gps is not None) and (att is not None) and (vfr is not None) \
            and (wpc is not None):

        # concatonate the messages into one string to send
        gpssum = ("TIME " + str(gps.time_usec) +
                  " LAT " + str(gps.lat/1.0e7) +
                  " LON " + str(gps.lon/1.0e7) +
                  " ALT " + str(gps.alt/1.0e3) +
                  " SATVIS " + str(gps.satellites_visible))
        attsum = ("ROLL " + "{0:.3f}".format(att.roll) +
                  " PITCH " + "{0:.3f}".format(att.pitch) +
                  " YAW " + "{0:.3f}".format(att.yaw))
        vfrsum = "SP " + str(vfr.groundspeed) + \
                 " HD " + str(vfr.heading) + \
                 " TH " + str(vfr.throttle)
        wpcsum = "CURWP " + str(wpc.seq)
        status = gpssum + " " + attsum + " " + vfrsum + " " + wpcsum
        # add metadata info to the message
        metamessage = meta_message(status, 'stats')

        # save the param values
        params['TIME'] = str(gps.time_usec)
        params['LAT'] = str(gps.lat/1.0e7)
        params['LON'] = str(gps.lon/1.0e7)
        params['ALT'] = str(gps.alt / 1.0e3)
        params['SATVIS'] = str(gps.satellites_visible)
        params['ROLL'] = "{0:.3f}".format(att.roll)
        params['PITCH'] = "{0:.3f}".format(att.pitch)
        params['YAW'] = "{0:.3f}".format(att.yaw)
        params['SP'] = str(vfr.groundspeed)
        params['HD'] = str(vfr.heading)
        params['TH'] = str(vfr.throttle)
        params['CURWP'] = str(wpc.seq)

    else:

        status = 'ERROR: No message received.'
        # add metadata info to the message
        metamessage = meta_message(status, 'error')

    # save the message to a local file
    save_message(metamessage)

    # send the message via serial
    params = send_serial_message(params, metamessage)

    return params


def loop_waypoints(params):
    """Check the current waypoint number and loop when needed."""

    print 'Current waypoint: ' + params['CURWP']
    print 'Number of waypoints: ' + str(params['wpcount']-1)

    if params['CURWP'] == str(params['wpcount']-1):
        print 'Looping waypoints ....'
        print 'Waypoint was: ' + params['CURWP']
        params = set_current_waypoint(params, 'SETCURWP 1')

    return params


def winch_command(params, message):
    """Send the winch command to the winch teensy."""

    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print '###################'
    print 'Sending the winch command ...'
    print message

    winch = params['winch']

    # we first have to split the message into words
    # for the call function to work
    m = message.split()
    print m
    print int(m[1])

    #################
    # Robert's code

    parameters = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    stop = int(m[4])

    if stop == 0:
        # winch do profile
        print stop
        header = 0xDA
        speedout = int(m[1])  # Collect runtime parameters
        speedin = int(m[2])
        depth = int(m[3])
        upperdepthbyte = depth >> 8
        lowerdepthbyte = depth & 0xFF
        # XOR all variables to create checksum
        checksum = (((speedout ^ speedin) ^ upperdepthbyte) ^ lowerdepthbyte)
        footer = 0
        parameters = [255, 255, header, speedout, speedin, upperdepthbyte,
                      lowerdepthbyte, checksum, footer]
    elif stop == 1:
        # winch return fast
        for x in range(len(parameters)):
            parameters[x] = 0xAA
        parameters[7] = 0
    elif stop == 2:
        # winch return slow
        for x in range(len(parameters)):
            parameters[x] = 0xBB
        parameters[7] = 0
    elif stop == 4:
        # winch hold position
        for x in range(len(parameters)):
            parameters[x] = 0xCC
        parameters[7] = 0
    elif stop == 8:
        # boat remote regular (warm) start
        for x in range(len(parameters)):
            parameters[x] = 0xDD
        parameters[7] = 0
    elif stop == 10:
        # boat remote cold start
        for x in range(len(parameters)):
            parameters[x] = 0xDF
        parameters[7] = 0
    elif stop == 16:
        # boat remote stop
        for x in range(len(parameters)):
            parameters[x] = 0xEE
        parameters[7] = 0

    # two bytes to indicate start of message
    parameters[0] = 255
    parameters[1] = 255

    for x in parameters:
        print x
        x = chr(x)  # cast to char to make single byte
        winch.write(x)
        time.sleep(0.1)

    print 'Sent winch command.'
    print parameters

    return params


def steering_setup(params, message):
    """Send steering commands to the servo via the PDB."""
    print 'Preparing the servo command ...'
    print message

    # we first have to split the message into words
    # for the call function to work
    m = message.split()
    servoval = int(m[1])
    print servoval

    params['steeringvalue'] = 70
    params['steeringoverride'] = 0
    if (servoval > 70) & (servoval < 88):
        params['steeringoverride'] = 1
        params['steeringvalue'] = servoval

    return params


def steering_override(params):
    """Send steering commands to the servo via the PDB."""

    if params['steeringoverride']:
        print 'Sending the servo command ...'
        pdb = params['pdb']
        servoval = params['steeringvalue']
        if (servoval > 70) & (servoval < 88):
            x = chr(servoval)  # cast to char to make single byte
            pdb.write(x)
            print servoval

    return params


def get_winch_status(params):
    """Get the current status from the winch.
    Saves it to a file and sends it via serial to the RPi.
    Returns the ready/notready status (1/0) and download status (yes/no)."""

    # STATUS 0 (busy) or 1 (ready)
    # Dir direction (up,down,stationary)
    # Rev (number of revolutions)

    winch = params['winch']

    bytesToRead = winch.inWaiting()

    if bytesToRead > 0:
        print 'Bytes to read: ' + str(bytesToRead)
        statusline = winch.readline()
        print statusline
        status = statusline.rstrip('\r\n')
        splitstatus = status.split(" ")
        # status = "STATUS 0 Dir up Rev 10003"

        # clear the serial buffer to be ready for next time
        winch.flushInput()

        # save all info we get from the winch, even if it's not status info
        # add metadata info to the message
        metamessage = meta_message(status, 'winchinfo')
        # save the message to a local file
        save_message(metamessage)
    else:
        status = ''

    # download status
    ctddownloading = 'no'
    downloadfile = os.path.isfile(
        '/home/ross/CTD/LogFiles/ctddownloadinprogress')
    if downloadfile:
        ctddownloading = 'yes'

    # if status != '':
    if "STATUS" in status:

        # append the download status to the message
        status = status + ' DOWNLOADING ' + ctddownloading

        # add metadata info to the message
        metamessage = meta_message(status, 'winchstatus')

        # save the message to a local file
        save_message(metamessage)

        # send the message via serial
        params = send_serial_message(params, metamessage)

        # determine the winch status (ready to receive next cast command or not
        if "STATUS 1" in status:
            winchstatus = 1
            winchready = 0

            # check to see if the CTD data was downloading and finished
            # don't consider the winch ready for another profile
            # until received three status 1 messages in a row
            # - this is to avoid timing issues between when the winch reacts to
            #   a profile command and when the teensy sends the status message
            if (ctddownloading == 'no'):
                if (params['winchstatuscount'] > 2):
                    winchready = 1
                if params['ctddownload']:
                    ctddownloading = 'start'
                if (params['ctddownloading'] == 'yes'):
                    ctddownloading = 'finish'

            params['winchstatuscount'] = params['winchstatuscount'] + 1
            params['winchstatus'] = winchstatus
            params['winchready'] = winchready
            params['ctddownloading'] = ctddownloading

        if "STATUS 0" in status:
            params['winchstatuscount'] = 0
            params['winchstatus'] = 0
            params['winchready'] = 0
            params['ctddownloading'] = 'no'

    if "Dir" in status:

        if "Dir down" in status:
            params['winchdir'] = 'down'

        if "Dir up" in status:
            params['winchdir'] = 'up'

        if "Dir stationary" in status:
            params['winchdir'] = 'stationary'

    if "Rev" in status:

        params['winchrev'] = splitstatus[5]

    if "Res" in status:

        params['winchres'] = splitstatus[7]

    if "Spd" in status:

        params['winchspd'] = splitstatus[9]

    if "CALBENT" in status:

        params['winchbentres'] = splitstatus[1]
        msg = 'winchbentres ' + params['winchbentres']
        metamessage = meta_message(msg, 'param')
        params = send_serial_message(params, metamessage)

    if "CALSTRAIGHT" in status:

        params['winchstraightres'] = splitstatus[1]
        msg = 'winchstraightres ' + params['winchstraightres']
        metamessage = meta_message(msg, 'param')
        params = send_serial_message(params, metamessage)

    if "SETPOINT" in status:

        params['winchsetpointres'] = splitstatus[1]
        msg = 'winchsetpointres ' + params['winchsetpointres']
        metamessage = meta_message(msg, 'param')
        params = send_serial_message(params, metamessage)

    # return the changed params variable
    return params


def initialize_winch_commands(params, message):
    """Initialize the winch commands so they can be run when appropriate."""

    print 'Initializing the winch command'

    # split the message
    m = message.split()
    numcasts = int(m[5])
    ctddownload = int(m[6])

    params['winchcommand'] = message
    params['winchcastsleft'] = numcasts
    params['winchcasts'] = numcasts
    params['ctddownload'] = ctddownload

    print 'Command: ' + params['winchcommand']
    print 'Casts to run: ' + str(params['winchcastsleft'])

    return params


def calibrate_winch(params, message):
    """Calibrate the fishing rod tension for the winch."""

    print 'Calibrating the winch fishing rod'

    winch = params['winch']

    # split the message
    m = message.split()

    params['winchcommand'] = message
    print 'Command: ' + params['winchcommand']

    parameters = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    if m[1] == 'min':
        for x in range(len(parameters)):
            parameters[x] = 0xCA
        parameters[7] = 0
    elif m[1] == 'max':
        for x in range(len(parameters)):
            parameters[x] = 0xCB
        parameters[7] = 0
    else:
        for x in range(len(parameters)):
            parameters[x] = 0xC0
        parameters[7] = 0

    parameters[0] = 255
    parameters[1] = 255

    for x in parameters:
        print x
        x = chr(x)  # cast to char to make single byte
        print x
        winch.write(x)
        time.sleep(0.1)

    print 'Sent winch command.'

    return params


def set_winch_cal(params, message):
    """Set the fishing rod tension values for the winch."""

    print 'Setting the winch fishing rod calibration values.'

    winch = params['winch']

    # split the message
    m = message.split()

    params['winchcommand'] = message
    print 'Command: ' + params['winchcommand']

    res = int(m[2])
    upperbyte = res >> 8
    lowerbyte = res & 0xFF

    parameters = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    if m[1] == 'bent':
        for x in range(len(parameters)):
            parameters[x] = 0xCD
    elif m[1] == 'straight':
        for x in range(len(parameters)):
            parameters[x] = 0xCE
    else:
        for x in range(len(parameters)):
            parameters[x] = 0xCF

    parameters[0] = 255
    parameters[1] = 255
    parameters[5] = upperbyte
    parameters[6] = lowerbyte
    parameters[7] = 0

    for x in parameters:
        print x
        x = chr(x)  # cast to char to make single byte
        print x
        winch.write(x)
        time.sleep(0.1)

    print 'Sent winch command.'

    return params


def manage_kayak_speed_settings(params):
    """Find the appropriate kayak speed (fast/slow)
    and whether the speed boundary was hit."""

    print 'Finding the appropriate kayak speed (fast/slow)'

    kayakspeedtype = 'fast'
    if params['winchdir'] == 'up':
        kayakspeedtype = 'fast'
    elif params['winchdir'] == 'down':
        kayakspeedtype = 'slow'
    elif params['winchdir'] == 'stationary':
        kayakspeedtype = 'fast'

    kayakchangespeed = 0
    if params['kayakspeedtype'] != kayakspeedtype:
        kayakchangespeed = 1

    # test lines (remove these for deployment)
    # kayakspeedtype = 'slow'
    # kayakchangespeed = 1

    params['kayakspeedtype'] = kayakspeedtype
    params['kayakchangespeed'] = kayakchangespeed

    print 'Kayak speed : ' + kayakspeedtype

    return params


def manage_winch_commands(params):
    """Manage when to run the winch commands."""

    # if need to run the command without a good status
    # uncomment this first line and comment the next
    # if (params['winchcommand'] != '') & (params['winchcastsleft'] > 0):
    if params['winchready'] & (params['winchcommand'] != '') & \
            (params['winchcastsleft'] > 0):

        # first do a stop and hold
        # winch_command(params, 'winch 0 0 0 4')
        # pause 5 seconds to let winch be neutral
        # print 'Sleeping for 5 seconds ...'
        # time.sleep(5)
        # print 'Sleep done.'

        # set the status counter to zero
        # so we don't start the next profile too soon
        params['winchstatuscount'] = 0

        # send the profile command
        winch_command(params, params['winchcommand'])

        params['winchready'] = 0
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print 'Running winch command ...'
        print params['winchcommand']
        # sleep for 2 seconds to make sure profile is underway
        time.sleep(2)
        params['winchcastsleft'] -= 1
        print 'Casts left to run: ' + str(params['winchcastsleft'])
        print '******************************************'
        print '******************************************'
        print '******************************************'
        msg = 'winchcasts ' + str(params['winchcastsleft'])
        metamessage = meta_message(msg, 'param')
        params = send_serial_message(params, metamessage)

    if (params['winchcommand'] != '') & (params['winchcastsleft'] == -1):
        winch_command(params, params['winchcommand'])
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print 'Interrupted winch with command: ' + params['winchcommand']
        print '******************************************'
        print '******************************************'
        print '******************************************'
        params['winchcastsleft'] = 0
        params['winchready'] = 0
        params['winchcommand'] = ''

    # if params['ctddownloading'] == 'start':
    if params['winchready'] & (params['ctddownloading'] == 'start') & \
            (params['winchcastsleft'] == 0):
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print 'Downloading CTD data ...'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        params['ctddownload'] = 0
        params['ctddownloading'] = 'yes'
        params['winchready'] = 0
        ctdnumparams = str(params['ctdnumparams'])
        # create the output CTD folder if it doesn't already exist
        ctdfolder = params['outfolder'] + '/CTD/'
        if not os.path.exists(ctdfolder):
            os.makedirs(ctdfolder)
        # this next line does the actual download - uncomment it to download
        # the argument may be 6 or 8 depending on the number of params
        # that particular CTD outputs
        subprocess.Popen(["/home/ross/kayak/downloadCTDProfile.sh",
                          ctdnumparams, ctdfolder])

    if params['ctddownloading'] == 'finish':
        ctdfile = params['outfolder'] + '/CTD/latestparsedProfile.txt'
        # params = ctd_max_press(params, ctdfile)
        print '******************************************'
        print 'Downloading complete.'
        print '******************************************'
        print '******************************************'
        print '******************************************'
        params['ctddownloading'] = 'no'
        params['winchready'] = 1
        params['winchcastsleft'] = params['winchcasts']

    if params['kayakchangespeed']:
        currentspeed = params['currentspeed']
        neededspeed = currentspeed
        if params['kayakspeedtype'] == 'fast':
            neededspeed = params['THR_MAX']
        if params['kayakspeedtype'] == 'slow':
            neededspeed = params['THR_MIN']
        if currentspeed != neededspeed:
            print 'Changing kayak speed.'
            command = 'SETPARAM THR_MAX ' + str(neededspeed)
            print command
            try:
                # don't keep the params from the next line
                # as they will change THR_MAX
                paramsjunk = set_parameter(params, command)
                params['currentspeed'] = neededspeed
                msg = 'SPEED ' + str(params['currentspeed'])
                metamessage = meta_message(msg, 'kayak')
                params = send_serial_message(params, metamessage)
            except Exception as e:
                print 'Unable to change kayak speed.'
                print str(e)
        else:
            print 'Kayak travelling at desired speed - no speed change made.'

    return params


def set_groundspeed(params, command):
    """Change the desired boat groundspeed setting on the Pixhawk."""

    print 'Setting ground speed ...'

    pixhawk = params['pixhawk']

    # pull out the desired groundspeed from the command
    scommand = command.split(' ')
    gspeed = float(scommand[1])

    pixhawk.mav.command_long_send(
        pixhawk.target_system, pixhawk.target_component,
        mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED, 0, 1, gspeed, 50, 1, 0, 0, 0)

    print 'Completed ... groundspeed changed.'

    return params


def handle_ctd_data(params, ctdfile):
    """Pull out a subset of the CTD data to decrease transfer size
    this is currently not used - instead I am transmitting only
    the max depth using ctd_max_press."""

    # the interval of datapoints to use
    # set this to 1 if you wish to send every data point
    step = 5

    # the data columns to transmit
    # note that datetime will always be transmitted
    # 1: conductivity  : COND
    # 2: temperature  : TEMP
    # 3: pressure (uncorrected) : UNPRESS
    # 4: air pressure  : AIRP
    # 5: pressure (corrected) : PRESS
    # 6: salinity  : SAL
    colhead = ['PRESS', 'TEMP', 'SAL']
    colnum = [5, 2, 6]

    # read the CTD file
    f = open(ctdfile, 'r')

    # loop over the file
    counter = 0
    for line in f:
        counter += 1
        m = line.split('\t')
        if counter == step:
            counter = 0
            datesplit = m[0].split(' ')
            outline = 'CTDDATE ' + datesplit[0] + ' CTDTIME ' + datesplit[1]
            for c in range(len(colhead)):
                outline = outline + ' ' + colhead[c] + ' ' + m[colnum[c]]
            msg = meta_message(outline, 'ctd')
            params = send_serial_message(params, msg)

    # close the CTD file
    f.close()

    return params


def ctd_max_press(params, ctdfile):
    """Pull out only the maximum pressure from the CTD data
    to decrease transfer size."""

    print 'Finding the maximum CTD pressure value from the last download.'

    try:
        # read the CTD file
        f = open(ctdfile, 'r')

        # initialize parameters
        outdatekeep = ''
        outtimekeep = ''
        outpresskeep = 0
        outtempkeep = 0
        outsalkeep = 0

        # loop over the file
        for line in f:
            m = line.split('\t')
            datesplit = m[0].split(' ')
            outdate = datesplit[0]
            outtime = datesplit[1]
            outpress = m[5]
            outtemp = m[2]
            outsal = m[6]
            if outpress > outpresskeep:
                outdatekeep = outdate
                outtimekeep = outtime
                outpresskeep = outpress
                outtempkeep = outtemp
                outsalkeep = outsal

        outline = ('CTDDATE ' + outdatekeep +
                   ' CTDTIME ' + outtimekeep +
                   ' PRESS ' + outpresskeep +
                   ' TEMP ' + outtempkeep +
                   ' SAL ' + outsalkeep)
        msg = meta_message(outline, 'ctdpressmax')

        # wait before sending message so not too much info is sent at once
        time.sleep(1)
        params = send_serial_message(params, msg)

        # close the CTD file
        f.close()

    except Exception:
        print 'No CTD data found.'

    return params


def send_keelctd_data(params):
    """Transmit the latest keel CTD data line."""

    # the data columns to transmit
    # note that datetime will always be transmitted
    # 2018-02-28 21:43:20.333, ...
    #            -0.0142, 20.6531, 9.9966, -0.1358, -0.1350, 0.0000
    # Column 1: conductivity (mS/cm) : COND
    # Column 2: temperature (degrees C) : TEMP
    # Column 3: pressure-uncorrected (dbar) : UNPRESS
    # Column 4: sea pressure (dbar)  (atmospheric pressure removed) : PRESS
    # Column 5: depth (m) : DEPTH
    # Column 6: salinity (PSU) (PSS-78) : SAL

    colhead = ['Temp', 'Sal']
    colnum = [2, 6]

    # read the keel CTD latest file
    latest = params['outfolder'] + '/keelctd/keelctd_latest.txt'
    line = ''
    try:
        f = open(latest, 'r')
        line = f.readline()
        f.close()
    except Exception:
        print 'Unable to read keelCTD file ...'
        print 'KeelCTD still initializing?'

    if line != '':

        # create the message
        linestrip = line.rstrip('\r\n')
        m = linestrip.split(',')
        datesplit = m[0].split(' ')
        outline = 'KDATE ' + datesplit[0] + ' KTIME ' + datesplit[1]
        params['KDATE'] = datesplit[0]
        params['KTIME'] = datesplit[1]
        for c in range(len(colhead)):
            outline = outline + ' ' + colhead[c] + ' ' + m[colnum[c]]
            params[colhead[c]] = m[colnum[c]]

        # send the message
        msg = meta_message(outline, 'keelctd')
        params = send_serial_message(params, msg)

    return params


def get_adcp_data(params):
    """Grab the latest ADCP data line."""

    # format of original data (single line):
    # yyyymmddHHMMSS,
    # u,12.3456,12.3456,12.3456,12.3456,12.3456,
    # 12.3456,12.3456,12.3456,12.3456,12.3456,
    # v,12.3456,12.3456,12.3456,12.3456,12.3456,
    # 12.3456,12.3456,12.3456,12.3456,12.3456,
    # w,12.3456,12.3456,12.3456,12.3456,12.3456,
    # 12.3456,12.3456,12.3456,12.3456,12.3456

    # read the ADCP latest file
    latest = params['outfolder'] + '/ADCP/adcp_latest.txt'
    line = ''
    try:
        f = open(latest, 'r')
        line = f.readline()
        f.close()
    except Exception:
        print 'Unable to read ADCP file ...'
        print 'ADCP still initializing?'

    if line != '':

        # create the message
        linestrip = line.rstrip('\r\n')
        uindex = linestrip.find(',u,')
        vindex = linestrip.find(',v,')
        windex = linestrip.find(',w,')
        params['ADATE'] = linestrip[0:8]
        params['ATIME'] = linestrip[8:14]
        params['ADCPu'] = linestrip[uindex+3:vindex]
        params['ADCPv'] = linestrip[vindex+3:windex]
        params['ADCPw'] = linestrip[windex+3:]

    return params


def send_adcp_data(params):
    """Transmit the latest ADCP data line."""

    # the data columns to transmit
    # note that datetime will always be transmitted

    outline = 'ADATE ' + params['ADATE'] \
        + ' ATIME ' + params['ATIME'] \
        + ' u ' + params['ADCPu'] \
        + ' v ' + params['ADCPv'] \
        + ' w ' + params['ADCPw']

    # send the message
    msg = meta_message(outline, 'adcp')
    params = send_serial_message(params, msg)

    return params


def set_system_time_direct(msg):
    """Set the system time based on the GPS clock.
    No output text or messages.
    This function can be used from anywhere."""

    timeset = 0
    if "$GPRMC" in msg:
        # parse the data string
        rmc_exists = False
        try:
            rmc = pynmea2.parse(msg)
        except Exception:
            pass
        else:
            rmc_exists = True

        if rmc_exists:
            if (rmc.datestamp is not None) and (rmc.timestamp is not None):
                rmc.dateandtime = datetime.combine(
                    rmc.datestamp, rmc.timestamp)
                os.system('sudo date --set="%s"' % str(rmc.dateandtime))
                timeset = 1

    return timeset


def set_system_time(dateandtime):
    """Set the system time to the input datestamp from the GPS."""

    # change the system clock only if the time difference is
    # greater than 2 seconds in either direction
    save_message('Setting the system time ...')

    print 'Setting the system time ...'
    print 'Current system time : ' + str(datetime.now())
    print 'Current GPS time    : ' + str(dateandtime)

    # set the system time
    os.system('sudo date --set="%s"' % str(dateandtime))

    print 'Updated system time : ' + str(datetime.now())
    print 'System time set.'

    save_message('Set server time to GPS time : ' + str(dateandtime))


def parse_gps_rmc(params):
    """Parse the GPS data.
    Output includes RMC.timestamp, RMC.datestamp,
    RMC.latitude, RMC.longitude"""

    # initialize the parameter logging whether the time has been set
    timeset = 0

    msg = params['gpsline']

    # make sure it's the right kind of GPS data first
    if "$GPRMC" in msg:

        save_message(msg)

        # parse the data string
        rmc_exists = False
        try:
            rmc = pynmea2.parse(msg)
        except Exception:
            errmsg = 'Unable to parse GPRMC string from GPS.'
            print errmsg
            save_message(errmsg)
        else:
            rmc_exists = True

        if rmc_exists:
            if (rmc.datestamp is not None) and (rmc.timestamp is not None):
                rmc.dateandtime = datetime.combine(
                    rmc.datestamp, rmc.timestamp)
                set_system_time(rmc.dateandtime)
                timeset = 1
            else:
                errmsg = 'Date and/or time empty in GPRMC string from GPS.'
                print errmsg
                save_message(errmsg)
            if (rmc.latitude != 0.0) and (rmc.longitude != 0.0):
                print 'GPS lat: ' + str(rmc.latitude)
                print 'GPS lon: ' + str(rmc.longitude)
            else:
                errmsg = 'Lat/lon empty in GPRMC string from GPS.'
                print errmsg
                save_message(errmsg)

    params['timeset'] = timeset

    return params


def get_gps_data(params):
    """Get the gps data."""

    gps = params['gps']

    # clear the serial buffer
    # to make sure we're not reading old data from the buffer
    gps.flushInput()

    # wait until a $ is found before reading the rest of the line
    gpsbyte = ''
    gcount = 0
    while gpsbyte != '$':
        gpsbyte = gps.read()
        if gpsbyte == '':
            gcount += 1
        if gcount == 20:
            break
    if gcount == 20:
        gpsline = "ERROR"
    else:
        gpsline = gps.readline()
        gpsline = '$' + gpsline.rstrip('\r\n')

    params['gpsline'] = gpsline

    return params


def handle_system_time(params):
    """Set the system time based on the GPS clock."""

    gcount = 0
    params['gpsline'] = 'ERROR'
    while params['timeset'] == 0:
        gcount += 1
        if gcount == 1:
            gpserr = 'Waiting for GPS data to set system time.'
            gmsg = meta_message(gpserr, 'gps')
            params = send_serial_message(params, gmsg)
        if gcount == 100:
            gpserr = 'Unable to set system time from GPS data.'
            gmsg = meta_message(gpserr, 'gps')
            params = send_serial_message(params, gmsg)
            break
        params = get_gps_data(params)
        print params['gpsline']
        if params['gpsline'] != 'ERROR':
            # Set the system time if its the right message
            params = parse_gps_rmc(params)
        else:
            gpserr = 'Unable to set system time from GPS data.'
            gmsg = meta_message(gpserr, 'gps')
            params = send_serial_message(params, gmsg)
            break

    return params


def mount_thumbdrive():
    """ mounts the thumbdrive USB port """

    # it is assumed the thumbdrive will be at this location
    # sdpath = '/dev/sda1'

    flashdrivedir = glob.glob('/dev/sd??')
    flashdrivedir.sort()

    if flashdrivedir[0][-1:].isdigit():
        print flashdrivedir[0]
    else:
        print "No Flashdrive Found..."

    sdpath = flashdrivedir[0]

    # the desired thumbdrive path
    thumbpath = '/home/ross/thumbdrive/'

    # create the folder if it doesn't already exist
    if not os.path.exists(thumbpath):
        os.makedirs(thumbpath)

    # check if already mounted before mounting
    cmd1 = 'mountpoint -q ' + thumbpath
    notmounted = os.system(cmd1)
    if notmounted:
        cmd2 = 'sudo mount ' + sdpath + ' ' + thumbpath
        try:
            os.system(cmd2)
            print 'Thumbdrive mounted.'
        except Exception as e:
            print 'Unable to mount thumbdrive at ' + \
                sdpath + ' to ' + thumbpath + '.'
            print str(e)
    else:
        print 'Thumbdrive already mounted.'


def make_output_folder():
    """Create a folder to hold data for this deployment.
    Creates a new folder with a date and timestamp."""

    now = datetime.now().strftime('%Y%m%d_%H%M%S')
    kayakname = readconfig('kayakname')

    outpath = '/home/ross/data/'
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    newfolder = outpath + kayakname + '_deploy_' + now

    # if the system time is bad, the folder may already exist
    count = 0
    while os.path.exists(newfolder):
        count = count + 1
        scount = str(count).zfill(3)
        newfolder = outpath + kayakname + '_deploy_' + now + '_' + scount

    os.makedirs(newfolder)

    print 'Deployment data folder: ' + newfolder

    return newfolder


def make_output_folder_old():
    """Create a folder to hold data for this deployment.
    Creates a new folder with an iterated number one higher
    than the existing folders."""

    # outpath = '/home/ross/thumbdrive/'
    outpath = '/home/ross/data/'
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    try:
        dirlist = glob.glob(outpath + 'deploy???')
        # print dirlist
        dirlist.sort()
        lastdeploy = dirlist[-1]
        # print dirlist
        print lastdeploy
        strnum = string.replace(lastdeploy, outpath + 'deploy', '')
        print strnum
        thisdeploynum = int(strnum) + 1
    except Exception:
        thisdeploynum = 1

    newfolder = outpath + 'deploy' + str(thisdeploynum).zfill(3)

    os.makedirs(newfolder)

    print 'Deployment data folder: ' + newfolder

    return newfolder


def get_pdb_status(params):
    """Get the current status from the PDB.
    Saves it to a file and sends it via serial to the modem.
    """

    pdb = params['pdb']

    # flush the input so we get the latest info
    pdb.flushInput()

    # read up to
    statusline = pdb.readline()
    # read it again to make sure we get a complete line
    # do we need our timeout to be at least the pdbinfo update frequency?
    # or is timeout ignored?
    statusline = pdb.readline()

    status = statusline.rstrip('\r\n')
    # 11.8323|0.0000|26.7174|1.6646|0.7350|0.7060|100|0
    # splitstatus = status.split("|")
    # add a parameter name so the GUI can parse it
    status = 'PDB ' + status

    # add metadata info to the message
    metamessage = meta_message(status, 'pdbinfo')
    # save the message to a local file
    save_message(metamessage)

    # send the message via serial
    params = send_serial_message(params, metamessage)

    return params

##############################################
# Keel CTD
##############################################


def send_keelctd_command(params, command):
    """Send a command to the keel CTD."""

    keelctd = params['keelctd']

    # add a line feed to the end of the message
    cmd = command + '\n'
    print cmd

    # send the command
    try:
        keelctd.flushInput()
        keelctd.flushOutput()
        keelctd.write(cmd)
        time.sleep(0.01)
        msg = keelctd.readline()
        print msg
    except Exception as e:
        print 'Unable to deliver command to keel CTD.'
        print str(e)

    return msg


def get_keelctd_data(params, numbytes):
    """Read the data from the keel CTD and save it to a file."""

    keelctd = params['keelctd']

    # output path
    basefolder = params['outfolder'] + '/keelctd'
    if not os.path.exists(basefolder):
        os.makedirs(basefolder)

    # open the output file
    outfile = basefolder + '/keelctd_internal_' + \
        datetime.now().strftime("%Y%m%d%H%M%S") + '.bin'
    print outfile
    fout = open(outfile, 'w', 1)

    # read the data
    for i in range(numbytes):
        try:
            data = keelctd.read(1)
            fout.write(data)
        except Exception as e:
            print 'Unable to save data from the keel CTD.'
            print str(e)

    # close the output file
    fout.close()

    # read the 2-byte CRC
    try:
        crc = keelctd.read(2)
    except Exception as e:
        print 'Unable to read the CRC.'
        print str(e)


def start_keelctd(params):
    """Send the necessary commands to configure and start the keel CTD."""

    # empty command to get the CTD's attention
    send_keelctd_command(params, '')
    # must wait 10 ms for it to wake up
    time.sleep(0.01)
    # stop any logging currently in progress
    send_keelctd_command(params, 'stop')
    # repeat to make sure it got the command
    send_keelctd_command(params, 'stop')
    # returns the instrument specs
    send_keelctd_command(params, 'id')
    # show the power status
    send_keelctd_command(params, 'powerstatus')
    # show the current sampling mode
    send_keelctd_command(params, 'sampling')
    # set the sampling mode (167 is 6 Hz)
    send_keelctd_command(params, 'sampling mode = continuous, period = 167')
    # gating condition - this command gives an error, so don't use it
    # send_keelctd_command(params, 'sampling gate = none')
    # allow the settings command to be run
    send_keelctd_command(params, 'permit settings')
    # cast detection
    send_keelctd_command(params, 'settings castdetection = off')
    # download any existing data
    meminfo = send_keelctd_command(params, 'meminfo used')
    splitmeminfo = meminfo.split('=')
    numbytes = int(splitmeminfo[1])
    print 'Number of bytes to read:  ' + str(numbytes)
    send_keelctd_command(params, 'read data 1 ' + str(numbytes) + ' 0')
    get_keelctd_data(params, numbytes)
    # serial setup
    send_keelctd_command(params, 'serial baudrate = 115200')
    send_keelctd_command(params, 'outputformat type = caltext01')
    send_keelctd_command(params, 'streamserial state = on')
    # set the current, start, and end times
    # set the start time to be the current time for immediate start
    now = datetime.now()
    timediff = timedelta(days=7)
    future = now + timediff
    nowtime = now.strftime('%Y%m%d%H%M%S')
    starttime = now.strftime('%Y%m%d%H%M%S')
    endtime = future.strftime('%Y%m%d%H%M%S')
    send_keelctd_command(params, 'now = ' + nowtime)
    send_keelctd_command(params, 'starttime = ' + starttime)
    send_keelctd_command(params, 'endtime = ' + endtime)
    send_keelctd_command(params, 'verify')
    # start the actual logging
    send_keelctd_command(params, 'enable erasememory = true')
    # don't check the status - this interferes with data collection
    # send_keelctd_command(params, 'status')
    # wait 10 ms before we proceed with collecting data
    # time.sleep(0.01)

    return params
