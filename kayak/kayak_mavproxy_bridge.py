#!/usr/bin/python
# Nick McComb
import subprocess
import sys


#Test if command line args are used
if (len(sys.argv) < 2):
	print "Please use the syntax ( " + sys.argv[0] + " start ) or (" + sys.argv[0] + " stop )."
	quit()

#If they are, check for (start) or (stop)
if sys.argv[1] == "start":
	print "Starting MAVLink Bridge."
	subprocess.call(["screen", "-dm", "-S", "mavproxy", "/home/ross/kayak/util/do_MAVProxyConnect.py"])

if sys.argv[1] == "stop":
	print "Stopping MAVLink Bridge."
	subprocess.call(["screen", "-X", "-S", "mavproxy", "kill"])

