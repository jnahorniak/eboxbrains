#!/usr/bin/python
"""Collect and save ADCP data to a file.

Read in serial ADCP data.

Output two files:
1) raw
2) timestamped with server time.
"""

import sys
import os
from datetime import datetime 
import binascii
import kayak


# open the connection to the ADCP
try:
    conn = kayak.adcp_connect()
except:
    print 'Unable to connect to ADCP.'
    exit()

# create the output folder if it doesn't already exist
basefolder = sys.argv[1] + '/ADCP'
if not os.path.exists(basefolder):
    os.makedirs(basefolder)

# set some initial parameters
current_time = datetime.now()
old_hour = -1
new_hour = current_time.hour

# loop continuously
while True:

    # create new output files approximately hourly
    if new_hour != old_hour:

        # clear the serial buffer to make sure we're not reading old data from the buffer
        # conn.flushInput()

        # create new output files
        current_time = datetime.now()
        outfile_bulk = basefolder + '/ADCP_bulk_' + current_time.strftime("%Y%m%d%H%M%S") + '.bin'
        print outfile_bulk
        f_bulk = open(outfile_bulk, 'wb', 1)

    # for testing purposes, read in 2000 bytes at a time 
    # to see if we are getting real data
    databulk = conn.read(2000)
    f_bulk.write(databulk)

    old_hour = new_hour
    new_hour = current_time.hour

    # close the files every hour
    if new_hour != old_hour:
        f_bulk.close()






