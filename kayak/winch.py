﻿#!/usr/bin/python
'''
winch module
to be run on the winch arduino
for communication with the RPi

jasmine s nahorniak
feb 29 2016
oregon state university
'''

# import modules
import re  # regular expression
import sys
from datetime import datetime
import serial
import subprocess

def readconfig(type):
    '''returns the relevant info from the config file.
	   type needs to be one of the parameters listed in the config file, such as
	   portlabel or baudrate'''	

    # read the config file	
    f=open('/home/ross/kayak/winch_config.txt','r')
    for line in f:
       m = re.match('^' + type + ':(.+)',line)
       if m:
           match = m.group(1)
    f.close()
    #print type + ": " + match
    return match

# this kayak name (in case there are multiple kayaks communicating) 
kayakname = readconfig('kayakname')

def rpi_connect():
    ''' opens the USB (serial) port to communicate with the RPi '''
    
    # get the port label 
    portlabel = readconfig('portlabel')
    baudrate = int(readconfig('baudrate'))

    # get the port number
    port = find_usb_port(portlabel)

    # set up the serial port
    ser=serial.Serial()
    ser.port=port
    ser.baudrate=baudrate
    ser.parity=serial.PARITY_NONE
    ser.stopbits=serial.STOPBITS_ONE
    ser.bytesize=serial.EIGHTBITS
    ser.timeout=5
    
    try:  
        ser.open() 
    except Exception, e:
        print 'Error opening serial port ' + port
        exit()   

    return ser


def meta_message(message,msgtype):
    ''' adds metadata to a message '''    

    # current date and time
    now = datetime.now().strftime('%Y/%m/%d %H:%M:%S UTC')

    # message
    # NOTE: The message must start with the kayak name (e.g. kayak1) 
    # so that the receiving code recognizes it.
    metamessage = kayakname + " -- " + msgtype + " -- " + now + " -- " + message 
    return metamessage 

def find_usb_port(portlabel):
    ''' finds the usb port for the given label (e.g. pl2303) (the ports are not static)'''

    # first see if serial/USB is in dmesg
    s1=subprocess.Popen(["dmesg"],stdout=subprocess.PIPE)
    s2=subprocess.Popen(["grep",portlabel+" converter now attached to ttyUSB"],stdin=s1.stdout,stdout=subprocess.PIPE)
    s1.stdout.close()
    sout=s2.communicate()[0]
    sasc=sout.decode("ascii")
    if not sasc:
       print "*** ERROR ***"
       print "USB not found! Please check that the cable is plugged in and that the label is " + portlabel + "."
       print "Check the portlabel using : dmesg | grep tty"
       print "*** ABORTING ***"
       sys.exit()
    ssplit=sasc.split()
    USBport_serial="/dev/"+ssplit[-1]

    # check to see if it is currently attached
    a1=subprocess.Popen(["sudo","cat","/proc/tty/driver/usbserial"],stdout=subprocess.PIPE)
    a2=subprocess.Popen(["grep",portlabel],stdin=a1.stdout,stdout=subprocess.PIPE)
    a1.stdout.close()
    aout=a2.communicate()[0]
    aasc=aout.decode("ascii")
    if not aasc:
       print "*** ERROR ***"
       print "USB not found! Please check that the cable is plugged in and that the label is " + portlabel + "."
       print "Check the portlabel using : dmesg | grep tty"
       print "*** ABORTING ***"
       sys.exit()

    # return the port
    #print 'USB serial port: '+USBport_serial
    return USBport_serial

def get_serial_message(ser):
    ''' reads messages from a serial port '''
   
    # read the message arriving on the serial port 
    rcv=ser.readline()
    msg=rcv.decode("utf-8")
    msg="kayak1 winch 24 5 10 255"
    if msg=="": 
       print "No message received."
    else: 
       # proceed only if the tag we need is in the command
       if (kayakname in msg): 
          command1=msg.replace(kayakname + " winch ","",1)
          command=command1.rstrip()
          handle_incoming_message(ser,command) 

def save_message(message):
    ''' saves a message to a file '''
    
    # date 
    fulldate=datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-2]
    fulldate=fulldate+"Z"
    #print fulldate
    shortdate=datetime.now().strftime("%Y%m%dT%H")
   
    # create the filename
    outfile='/home/ross/kayak/winch_out/winch_' + shortdate + '.txt'
    
    # open and write to the file	
    f=open(outfile,'a')
    f.write(fulldate + ' ' + message + '\n')
    f.close()
    

def send_serial_message(ser,message):
    ''' sends a message via serial/USB to the RPi '''
   
    print message
 
    # add a line feed to the end of the message
    msg = message + '\n'
 
    # send the message 
    ser.write(msg.encode('utf-8'))
    print 'Message sent.' 

    
def handle_incoming_message(ser,message):
    ''' functions to run on the incoming commands '''
    
    # date 
    date=datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-2]
    date=date+"Z"
    print date
   
    # forward message to appropriate function
    print message
    if (" WINCH " in message.upper()):
       winch_command(ser,message)

def winch_command(ser,message):
    ''' sends the winch command to the relevant function on the arduino
      
    # we first have to split the message into words for the call function to work 
    m = message.split()
    m.insert(0,"winchControl.py")
    subprocess.call(m)


def winch_status(ser):
    ''' gets the current status from the winch 
        saves it to a file and sends it via serial to the RPi '''
  
    ############ INCOMPLETE ###################
    # need to modify to get real status from winch
    # LIN line distance (0-65535) SPD speed (0-254) DIR direction (0:out,1:in) LIM limit (0 no, 1 yes) STP stop (0/1)
    status = "LIN 10003 SPD 55 DIR 0 LIM 0 STP 0"
        
    # add metadata info to the message 
    metamessage = meta_message(status,'winchstatus')

    # save the message to a local file
    save_message(metamessage)
    
    # send the message via serial 
    send_serial_message(ser,metamessage) 
    
