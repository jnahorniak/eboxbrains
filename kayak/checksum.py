#!/usr/bin/python
"""
Checksum test.

jasmine s nahorniak
dec 29 2017
oregon state university
"""

import binascii
import zlib

# From the docs:
# The checksum is the least significant 2-bytes of the summation of the
# entire SBD message. The high order byte must be sent first. For example
# if the DTE were to send the word hello encoded in ASCII to the 9602
# the binary stream would be hex 68 65 6c 6c 6f 02 14.

# the message in ASCII (is stored as a binary string)
msg_text = "hello"
# print msg_text

# compress the message as much as we possibly can
msg_compressed = zlib.compress(msg_text, 9)
print "Before compression: " + str(len(msg_text))
print "After compression: " + str(len(msg_compressed))

# keep whichever is smaller, the compressed or uncompressed version
if len(msg_compressed) < len(msg_text):
    msg = msg_compressed
    print "Message has been compressed."
else:
    msg = msg_text
    print "Message has not been compressed."

print msg

# the length of the message to be transmitted (number of bytes)
msg_len = len(msg)
# print msg_len

# make sure the message length is within acceptable limits for SBD
if msg_len >= 1 and msg_len <= 340:
    # calculate the checksum as an integer
    checksum_integer = sum(map(ord,msg))
    # print checksum_integer

    # convert the checksum to at least two bytes of binary
    checksum_binary = format(checksum_integer,'b').zfill(16)
    # print checksum_binary

    # extract the two least significant bytes of the checksum
    checksum_twobyte = checksum_binary[-16:]
    # print checksum_twobyte

    # convert the message to hex
    msg_hex = binascii.hexlify(msg)
    # print msg_hex

    # convert the two-byte checksum to hex
    checksum_hex = '%04X' % int(checksum_twobyte, 2)
    # print checksum_hex

    # append the checksum to the message (in hex)
    msg_final_hex = msg_hex + checksum_hex
    print msg_final_hex

    # convert the result to binary (ready to send over serial)
    msg_final_binary = msg_final_hex.decode("hex")
    print msg_final_binary

else:
    print "Invalid message length. Transmission aborted."
