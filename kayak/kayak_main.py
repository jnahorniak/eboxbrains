#!/usr/bin/python
"""Main kayak script.

 1) STATUS
    gets the status from various components (PixHawk, winch, ...)
    saves the status to a file
    sends the status to a remote PC
 2) COMMANDS
    receives commands sent from a remote PC
    delivers the commands to the appropriate component (PixHawk, winch, ...)
    returns any results to the PC

 This script runs continuously.

 usage:
   kayak_main.py

 WARNING: this script should NOT be run at the same time as mavproxy;
    it will confuse the pixhawk and require a reboot


 jasmine s nahorniak
 january 29, 2016
 last updated: november 27, 2017
 oregon state university
"""


# import modules
import subprocess
import time
import sys
import kayak
import os
from datetime import datetime
import serial

print ''
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'
print 'STARTING KAYAK MAIN SCRIPT'
print '!!!!!!!!!!!!!!!!!!!!!!!!!!'

# sleep first to give the pixhawk time to warm up
time.sleep(2)

# user can select which connections are to be made
# just comment out the ones we don't need
conntypes = [
    "pixhawk",
    "winch",
    "modem",
    "gps",
    "adcp",
    "pdb",
    "keelctd",
    # "iridium",
    "imu",
    # "met",
]

# initialize the dictionary of parameters to store
# we can add any new parameters we want here
params = {}
params['winchcastsleft'] = 0
params['winchcasts'] = 0
params['winchcommand'] = ''
params['winchready'] = 0
params['winchstatus'] = 0
params['winchrev'] = 0
params['winchres'] = 0
params['winchspd'] = 0
params['ctddownload'] = 0
params['ctddownloading'] = 'no'
params['winchdir'] = 'stationary'
params['currentspeed'] = 0
params['outfolder'] = ''
params['gpsline'] = 'ERROR'
params['timeset'] = 0
params['kayakspeedtype'] = 'fast'
params['kayakchangespeed'] = 0
params['winchstatuscount'] = 0
params['steeringoverride'] = 0
params['steeringvalue'] = 70  # PDB expects 70 - 86, with 70 being stop
params['iridiumnetwork'] = 0  # network available (1) or not (0)
params['iridiumlastsent'] = datetime.now()
params['iridiuminterval'] = 5  # minutes between status messages
params['iridiumresponsecode'] = 0  # matching code for iridium response
params['iridiumwaiting'] = 1  # iridium messages possibly waiting if set to 1
params['unsolicited'] = 0  # unsolicited Iridium message (1) or not (0)
params['ctdnumparams'] = 8  # can be 6 or 8, depends on the CTD, see below
params['wpcount'] = 2  # initial value of 2 rather than 0 just in case
params['CURWP'] = '0'  # initial value in case pixhawk status not avail
params['KDATE'] = '1970-01-01'  # keelCTD date
params['KTIME'] = '00:00:00'  # keelCTD time
params['Temp'] = '0'  # keelCTD temperature
params['Sal'] = '0'  # keelCTD salinity
params['ADATE'] = '19700101'  # ADCP date (YYYYMMDD)
params['ATIME'] = '000000'  # ADCP time (HHMMSS)
params['ADCPu'] = '0'  # ADCP u velocities (comma separated list)
params['ADCPv'] = '0'  # ADCP v velocities (comma separated list)
params['ADCPw'] = '0'  # ADCP w velocities (comma separated list)

# ctdnumparams
# CTD S/N 060080 has 8 parameters (lost?)
# CTD S/N 060379 has 6 parameters
# CTD S/N 060380 has 8 parameters
# CTD S/N 060701 has 8 parameters
# CTD S/N 060702 has 8 parameters
# CTD S/N 060703 has 8 parameters
# CTD S/N 060704 has 8 parameters
# CTD S/N needs to be specified in wpa_supplicant.conf

# open the connections
for conntype in conntypes:
    print conntype
    try:
        conn = eval('kayak.' + conntype + '_connect()')
        params[conntype] = conn
    except Exception:
        print 'No connection found for: ' + conntype

# mount the thumbdrive
# kayak.mount_thumbdrive()

# remove the ctddownloadinprogress file if it is still there
downfile = '/home/ross/CTD/LogFiles/downloadinprogress'

if os.path.exists(downfile):
    os.system("rm " + downfile)

# set the system time based on the first GPS datetime received
if 'gps' in params:
    params = kayak.handle_system_time(params)
    # close the GPS serial connection
    # - the next GPS function will open it again
    params['gps'].close()

# set up the deployment folder
params['outfolder'] = kayak.make_output_folder()

# save the GPS data (do not forward over antenna)
# if ('gps' in params) & (params['gpsline'] != 'ERROR'):
if 'gps' in params:
    print 'Saving the GPS data to a file in the background ...'
    with open('/home/ross/logs/GPS.log', 'w') as foutgps:
        subprocess.Popen(
            ['/home/ross/kayak/kayak_gps.py', params['outfolder']],
            stdout=foutgps, stderr=foutgps)

# save the ADCP data (do not forward over antenna)
if 'adcp' in params:
    print 'Saving the ADCP data in the background ...'
    with open('/home/ross/logs/ADCP.log', 'w') as foutadcp:
        subprocess.Popen(
            ['/home/ross/kayak/kayak_adcp.py', params['outfolder']],
            stdout=foutadcp, stderr=foutadcp)
    print 'Processing the ADCP data in the background ...'
    with open('/home/ross/logs/ADCP_processing.log', 'w') as foutadcpproc:
        subprocess.Popen(
            ['/home/ross/ADCP/pyROSE_nothread.py', '10'],
            stdout=foutadcpproc, stderr=foutadcpproc)

# save the IMU data (do not forward over antenna)
if 'imu' in params:
    print 'Saving the IMU data in the background ...'
    with open('/home/ross/logs/IMU.log', 'w') as foutimu:
        subprocess.Popen(
            ['/home/ross/kayak/kayak_imu.py', params['outfolder']],
            stdout=foutimu, stderr=foutimu)

# save the Met Station data (do not forward over antenna)
if 'met' in params:
    print 'Saving the Met Station data in the background ...'
    with open('/home/ross/logs/met.log', 'w') as foutmet:
        subprocess.Popen(
            ['/home/ross/kayak/kayak_met.py', params['outfolder']],
            stdout=foutmet, stderr=foutmet)

# save the keel CTD data (do not forward over antenna)
if 'keelctd' in params:
    print 'Saving the keel CTD data in the background ...'
    # close the connection temporarily;
    # we will open this again in the background script
    params['keelctd'].close()
    with open('/home/ross/logs/keelctd.log', 'w') as foutkeelctd:
        subprocess.Popen(
            ['/home/ross/kayak/kayak_keelctd.py', params['outfolder']],
            stdout=foutkeelctd, stderr=foutkeelctd)

# get/set the initial parameter values from the PixHawk
if 'pixhawk' in params:
    params = kayak.set_initial_parameters(params)
    params = kayak.get_pixhawk_status(params)

# configure and register the Iridium modem, and check for messages
if 'iridium' in params:
    kayak.configure_iridium(params)
    kayak.wait_for_iridium_network(params)
    kayak.register_iridium(params)

# main loop to get the kayak status and run any commands received
while True:
    try:

        # get the pixhawk status
        if 'pixhawk' in params:
            try:
                params = kayak.check_pixhawk_connection(params)
                params = kayak.get_pixhawk_status(params)
                params = kayak.loop_waypoints(params)
            except Exception as e:
                print 'PixHawk communication failed.'
                print str(e)
        else:
            print 'No pixhawk connection ... exiting.'
            sys.exit()

        # run commands sent via XTend
        if 'modem' in params:
            try:
                params = kayak.check_serial_connection(params, 'modem')
                params = kayak.get_serial_message(params)
            except Exception as e:
                print 'Modem communication failed.'
                print str(e)

        # run commands received via Iridium and send status messages
        if 'iridium' in params:
            try:
                params = kayak.check_serial_connection(params, 'iridium')
                params = kayak.check_iridium_response(params, 'junk')
                params = kayak.request_iridium_message(params)
                params = kayak.manage_iridium_status_messages(params)
            except Exception as e:
                print 'Iridium communication failed.'
                print str(e)

        # get the winch status
        if 'winch' in params:
            try:
                params = kayak.check_serial_connection(params, 'winch')
                params = kayak.get_winch_status(params)
                params = kayak.manage_kayak_speed_settings(params)
            except Exception as e:
                print 'Winch communication failed.'
                print str(e)

            try:
                params = kayak.manage_winch_commands(params)
            except Exception as e:
                print 'Unable to manage winch command.'
                print str(e)

        # transmit the latest keel CTD data
        if 'keelctd' in params:
            try:
                params = kayak.send_keelctd_data(params)
            except Exception as e:
                print 'Unable to transmit keel CTD data.'
                print str(e)

        # transmit the latest ADCP data
        if 'adcp' in params:
            try:
                params = kayak.get_adcp_data(params)
                params = kayak.send_adcp_data(params)
            except Exception as e:
                print 'Unable to transmit ADCP data.'
                print str(e)

        # pdb
        if 'pdb' in params:
            try:
                params = kayak.check_serial_connection(params, 'pdb')
                params = kayak.steering_override(params)
                params = kayak.get_pdb_status(params)
            except Exception as e:
                print 'PDB communication failed.'
                print str(e)

    except KeyboardInterrupt:
        print '\n *** Interrupt received ... exiting. ***'

        for conntype in conntypes:
            conn = params[conntype]
            try:
                conn.close()
            except serial.serialutil.SerialException as e:
                print 'Unable to close connection for : ' + conntype
                print str(e)

        sys.exit()
