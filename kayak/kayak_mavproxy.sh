#!/bin/bash
#
# starts the communication between the RPi and the PixHawk
# using MavProxy
# performs whatever commands are in the mavinit.scr for that kayak
#
# WARNING: This script should NOT be run at the same time as the kayak custom script (kayak_receive_commands_serial, kayak_get_status)
#
# usage:
#    kayak_mavproxy.sh
#
# output logs will go to /home/pi/kayak/kayak_mavproxy_out
#
# jasmine s nahorniak
# november 18, 2015
# oregon state university
#############################################################


# start mavproxy
# arguments used here are:
# --aircraft : output folder
# --state-basedir : the main directory (do not change)
# --daemon : tells it to run in the background
# --master : specifies the USB port name
# --out : specifies an (IP address and port) or (serial port and baudrate) 
#         to pass the information on to

# ethernet
#/usr/local/bin/mavproxy.py --aircraft="kayak_mavproxy_out" --state-basedir="/home/pi/kayak/" --master=/dev/ttyACM0 --out=udp:128.193.70.29:14550

# XTend modem
/usr/local/bin/mavproxy.py --aircraft="kayak_mavproxy_out" --state-basedir="/home/pi/kayak/" --master=/dev/ttyPixHawk --out=/dev/ttyModem,57600

# no output, just local
#/usr/local/bin/mavproxy.py --aircraft="kayak_mavproxy_out" --state-basedir="/home/pi/kayak/" --master=/dev/ttyPixHawk

#############################################################

