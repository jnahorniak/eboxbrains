"""
    This file contains the Kayak Class
    This class handles inter-thread communications for the kayak subsystems as well as general kayak management.
"""

__author__ = ["Jasmine Nahorniak", "Corwin Perren"]
__credits__ = [""]
__email__ = ["jasmine@coas.oregonstate.edu", "caperren@caperren.com"]


#####################################
# Imports
#####################################
# Python native imports
from PyQt4 import QtCore
import logging

# Custom imports
from LoggerCore import KayakLogger
from SettingsCore import KayakSettings
from PixhawkCore import Pixhawk
from ModemCore import Modem
from WinchCore import Winch
from BatteryMonitorCore import BatteryMonitor


#####################################
# Kayak Class Definition
#####################################
class Kayak(QtCore.QThread):
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        # ########## Setup and instantiate an instance of the logger ##########
        self.logger_setup = KayakLogger(console_output=False)
        self.logger = logging.getLogger("Kayak")

        # ########## Setup and instantiate and instance of the settings class ##########
        self.settings_setup = KayakSettings()
        self.settings = QtCore.QSettings()

        # ########## Thread flags ##########
        self.not_abort_flag = True
        self.instantiate_subsystems_flag = True
        self.connect_signals_flag = True

        # ########## Subsystem Variables ##########
        self.pixhawk = None
        self.winch = None
        self.modem = None
        self.battery_monitor = None

        # ########## Start thread ##########
        self.start()

    def run(self):
        self.logger.debug("Kayak Thread Starting...")

        while self.not_abort_flag:

            if self.instantiate_subsystems_flag:
                self.instantiate_subsystems()
                self.instantiate_subsystems_flag = False
            elif self.connect_signals_flag:
                self.connect_signals_to_slots()
                self.connect_signals_flag = False
            else:
                pass

            self.msleep(100)  # This is not zero just so we don't eat unnecessary cpu cycles waiting

        self.logger.debug("Kayak Thread Exiting...")

    def connect_signals_to_slots(self):
        self.pixhawk.queue_outbound_modem_message_signal.connect(self.modem.on_queue_modem_message_signal_slot)
        self.modem.queue_inbound_pixhawk_message_signal.connect(self.pixhawk.on_queue_pixhawk_message_signal_slot)

    def instantiate_subsystems(self):
        # pass
        self.pixhawk = Pixhawk()
        self.winch = Winch()
        self.modem = Modem()
        self.battery_monitor = BatteryMonitor()

