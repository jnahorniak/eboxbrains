"""
    This file contains the Modem Class
    This class handles communications to and from the base station
"""

__author__ = ["Jasmine Nahorniak", "Corwin Perren"]
__credits__ = [""]
__email__ = ["jasmine@coas.oregonstate.edu", "caperren@caperren.com"]


#####################################
# Imports
#####################################
# Python native imports
from PyQt4 import QtCore
import logging
import serial

#####################################
# Global Variables
#####################################
message_to_class = {
    'listwp': 'pixhawk',
    'goto': 'pixhawk',
    'getparam': 'pixhawk',
    'setparam': 'pixhawk',
    'setcurwp': 'pixhawk',
    'winch': 'winch'
}


#####################################
# Modem Class Definition
#####################################
class Modem(QtCore.QThread):

    queue_inbound_pixhawk_message_signal = QtCore.pyqtSignal(str, int)
    queue_inbound_winch_message_signal = QtCore.pyqtSignal(str, int)

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        # ########## Get an instance of the logger ##########
        self.logger = logging.getLogger("Kayak")

        # ########## Create Instance of settings ##########
        self.settings = QtCore.QSettings()

        # ########## Thread flags ##########
        self.not_abort_flag = True
        self.connect_to_modem_flag = True
        self.flush_and_setup_queue_flag = True

        # ########## Class Variables ##########
        self.modem_conn = serial.Serial()  # I instantiate it here for autocomplete in my editor...

        self.outbound_message_queue = {}

        self.priority_range = self.settings.value("priorities/num_priorities").toInt()[0]

        # ########## Make signal/slot connections ##########
        self.connect_signals_to_slots()

        # ########## Start thread ##########
        self.start()

    def run(self):
        self.logger.debug("Modem Thread Starting...")

        while self.not_abort_flag:

            if self.connect_to_modem_flag:
                self.connect_to_modem()
                self.connect_to_modem_flag = False
            elif self.flush_and_setup_queue_flag:
                self.flush_and_setup_queue()
                self.flush_and_setup_queue_flag = False
            else:
                if self.modem_conn.inWaiting() > 0:
                    self.read_and_queue_serial_message()
                elif not self.is_queue_empty():
                    self.send_one_line_from_queue()

            self.msleep(50)  # This is not zero just so we don't eat unnecessary cpu cycles waiting

        self.logger.debug("Modem Thread Exiting...")

    def connect_signals_to_slots(self):
        pass

    def connect_to_modem(self):
        port = str(self.settings.value("modem/port").toString())
        baud = self.settings.value("modem/baud").toInt()[0]

        try:
            self.modem_conn = serial.Serial(port=port, baudrate=baud)  # Other defaults are correct
        except:
            self.logger.error("Failed to connect to modem. Please check connections and try again.")
            self.not_abort_flag = False

    def flush_and_setup_queue(self):
        del self.outbound_message_queue
        self.outbound_message_queue = {}

        for i in range(self.priority_range):
            self.outbound_message_queue[i] = []

    def is_queue_empty(self):
        for i in range(self.priority_range):
            if self.outbound_message_queue[i]:
                return False
        return True

    def send_one_line_from_queue(self):
        message_to_send = None

        for i in range(self.priority_range):
            if self.outbound_message_queue[i]:
                message_to_send = self.outbound_message_queue[i][0]
                del self.outbound_message_queue[i][0]
                break

        self.modem_conn.write((message_to_send + '\n').encode('utf-8'))
        self.logger.debug(message_to_send)

    def read_and_queue_serial_message(self):
        kayak_name = self.settings.value("kayak/name").toString()
        message = self.modem_conn.readline().decode("utf-8")

        if message == "":
            self.logger.info("No message received.")
        else:
            if kayak_name in message:
                clean_message = str(message.replace(kayak_name + " ", "", 1).rstrip())
                self.logger.debug("MESSAGE: " + clean_message)
                self.signal_class_from_message(clean_message)

    def signal_class_from_message(self, message):
        class_name = message_to_class[message.split(" ")[0]]

        if class_name == 'pixhawk':
            self.queue_inbound_pixhawk_message_signal.emit(message, 5)
        elif class_name == 'winch':
            pass
        else:
            self.logger.error("Unknown message type.")
            self.logger.error(message)

    def on_queue_modem_message_signal_slot(self, message, priority):
        self.outbound_message_queue[priority].append(str(message))