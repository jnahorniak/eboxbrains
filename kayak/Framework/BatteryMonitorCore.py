"""
    This file contains the Winch Class
    This class handles communications and management of the winch
"""

__author__ = ["Jasmine Nahorniak", "Corwin Perren"]
__credits__ = [""]
__email__ = ["jasmine@coas.oregonstate.edu", "caperren@caperren.com"]


#####################################
# Imports
#####################################
# Python native imports
from PyQt4 import QtCore
import logging
from nut2 import PyNUTClient
from time import time
from subprocess import call

#####################################
# Global Variables
#####################################
UPS_STRING = "openups"
CHARGING_STRING = "OL CHRG"
DISCHARGING_STRING = "OB DISCHRG"


#####################################
# Winch Class Definition
#####################################
class BatteryMonitor(QtCore.QThread):
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        # ########## Get an instance of the logger ##########
        self.logger = logging.getLogger("Kayak")

        # ########## Create Instance of settings ##########
        self.settings = QtCore.QSettings()

        # ########## Thread flags ##########
        self.not_abort_flag = True
        self.check_ups_connection = True

        # ########## Class Variables ##########
        self.nut_client = PyNUTClient()

        # Variables for holding values read in from the client
        self.battery_charge = None
        self.battery_voltage = None
        self.battery_current = None
        self.openups_temperature = None
        self.input_voltage = None
        self.input_current = None
        self.output_voltage = None
        self.output_current = None
        self.ups_status = None

        # Automatic shutdown variables
        self.on_batt_start_time = 0
        self.seconds_before_shutdown = 30

        # ########## Make signal/slot connections ##########
        self.connect_signals_to_slots()

        # ########## Start thread ##########
        self.start()

    def run(self):
        self.logger.debug("Battery Monitor Thread Starting...")

        while self.not_abort_flag:
            if self.check_ups_connection:
                self.check_openups_connection()
                self.check_ups_connection = False
                continue
            else:
                self.update_status_variables()
                self.check_if_shutdown_needed()
            self.msleep(100)

        self.logger.debug("Battery Monitor Thread Exiting...")

    def connect_signals_to_slots(self):
        pass

    def check_openups_connection(self):
        try:
            self.nut_client.list_vars(UPS_STRING)
            self.logger.debug("Successful connection to Open UPS.")
        except:
            self.logger.error("Unable to connect to Open UPS!!!")
            self.not_abort_flag = False

    def update_status_variables(self):
        try:
            ups_vars = self.nut_client.list_vars(UPS_STRING)
        except:
            self.logger.error("Lost Connection to Open UPS!!!")
            self.not_abort_flag = False
            return

        self.battery_charge = ups_vars['battery.charge']
        self.battery_voltage = ups_vars['battery.voltage']
        self.battery_current = ups_vars['battery.current']
        self.openups_temperature = ups_vars['battery.temperature']
        self.input_voltage = ups_vars['input.voltage']
        self.input_current = ups_vars['input.current']
        self.output_voltage = ups_vars['output.voltage']
        self.output_current = ups_vars['output.current']
        self.ups_status = ups_vars['ups.status']

        # self.logger.debug(self.battery_charge)
        # self.logger.debug(self.battery_voltage)
        # self.logger.debug(self.battery_current)
        # self.logger.debug(self.openups_temperature)
        # self.logger.debug(self.input_voltage)
        # self.logger.debug(self.input_current)
        # self.logger.debug(self.output_voltage)
        # self.logger.debug(self.output_current)
        # self.logger.debug(self.ups_status)

    def check_if_shutdown_needed(self):
        if self.ups_status == DISCHARGING_STRING:
            if self.on_batt_start_time == 0:
                self.on_batt_start_time = time()
                self.logger.info("NUC running on battery power! Beginning shutdown countdown!")

            if (time() - self.on_batt_start_time) >= self.seconds_before_shutdown:
                self.logger.info("On battery for " + str(self.seconds_before_shutdown) + " seconds! Shutting down!")
                call(["sudo", "poweroff"])
                self.not_abort_flag = False
        else:
            if self.on_batt_start_time != 0:
                self.logger.info("NUC returned to main power. Aborting shutdown.")
            self.on_batt_start_time = 0

