"""
    This file contains the Winch Class
    This class handles communications and management of the winch
"""

__author__ = ["Jasmine Nahorniak", "Corwin Perren"]
__credits__ = [""]
__email__ = ["jasmine@coas.oregonstate.edu", "caperren@caperren.com"]


#####################################
# Imports
#####################################
# Python native imports
from PyQt4 import QtCore
import logging


#####################################
# Winch Class Definition
#####################################
class Winch(QtCore.QThread):
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        # ########## Get an instance of the logger ##########
        self.logger = logging.getLogger("Kayak")

        # ########## Make signal/slot connections ##########
        self.connect_signals_to_slots()

        # ########## Start thread ##########
        self.start()

    def run(self):
        self.logger.debug("Winch Thread Starting...")

        while True:
            #self.logger.debug("This is the Winch Class")
            self.msleep(100)

        self.logger.debug("Winch Thread Exiting...")

    def connect_signals_to_slots(self):
        pass
