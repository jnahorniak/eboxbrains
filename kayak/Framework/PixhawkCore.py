"""
    This file contains the Pixhawk Class
    This class handles communications to and from as well as management of the Pixhawk controller
"""

__author__ = ["Jasmine Nahorniak", "Corwin Perren"]
__credits__ = [""]
__email__ = ["jasmine@coas.oregonstate.edu", "caperren@caperren.com"]

#####################################
# Imports
#####################################
# Python native imports
from PyQt4 import QtCore
import logging
from pymavlink import mavutil, mavwp, mavparm
from datetime import datetime
import time
import os

#####################################
# Global Variables
#####################################
message_save_path = os.getenv("HOME") + "/kayak/kayak_custom_out"


#####################################
# Pixhawk Class Definition
#####################################
class Pixhawk(QtCore.QThread):

    queue_outbound_modem_message_signal = QtCore.pyqtSignal(str, int)

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        # ########## Get an instance of the logger ##########
        self.logger = logging.getLogger("Kayak")

        # ########## Create Instance of settings ##########
        self.settings = QtCore.QSettings()

        # ########## Thread flags ##########
        self.not_abort_flag = True
        self.connect_to_pixhawk_flag = True
        self.flush_and_setup_queue_flag = True

        # ########## Class Variables ##########
        self.pixhawk_conn = None
        self.inbound_message_queue = {}

        self.priority_range = self.settings.value("priorities/num_priorities").toInt()[0]

        # ########## Make signal/slot connections ##########
        self.connect_signals_to_slots()

        # ########## Start thread ##########
        self.start()

    def run(self):
        self.logger.debug("Pixhawk Thread Starting...")

        while self.not_abort_flag:

            if self.connect_to_pixhawk_flag:
                self.connect_to_pixhawk()
                self.connect_to_pixhawk_flag = False
            elif self.flush_and_setup_queue_flag:
                self.flush_and_setup_queue()
                self.flush_and_setup_queue_flag = False
            else:
                if not self.is_queue_empty():
                    self.process_one_line_from_queue()
                self.get_pixhawk_status()

        self.logger.debug("Pixhawk Thread Exiting...")

    def connect_signals_to_slots(self):
        pass

    def connect_to_pixhawk(self):
        port = str(self.settings.value("pixhawk/port").toString())
        baud = self.settings.value("pixhawk/baud").toInt()[0]

        self.logger.debug("Attempting to connect to pixhawk...")
        self.pixhawk_conn = mavutil.mavlink_connection(port, baud=baud, retries=3)
        self.pixhawk_conn.wait_heartbeat()
        self.logger.debug("Heartbeat received. Pixhawk connected!")

    def flush_and_setup_queue(self):
        del self.inbound_message_queue
        self.inbound_message_queue = {}

        for i in range(self.priority_range):
            self.inbound_message_queue[i] = []

    def is_queue_empty(self):
        for i in range(self.priority_range):
            if self.inbound_message_queue[i]:
                return False
        return True

    def process_one_line_from_queue(self):
        message_to_process = None

        for i in range(self.priority_range):
            if self.inbound_message_queue[i]:
                message_to_process = self.inbound_message_queue[i][0]
                del self.inbound_message_queue[i][0]
                break

        message_command = message_to_process.split(" ")[0]

        if message_command == "listwp":
            self.list_waypoints()
        elif message_command == "goto":
            self.set_waypoints(message_to_process)
        elif message_command == "getparam":
            self.get_parameter(message_to_process)
        elif message_command == "setparam":
            self.set_parameter(message_to_process)
        elif message_command == "setcurwp":
            self.set_current_waypoint(message_to_process)

    def get_pixhawk_status(self):
        """ Gets the current status from the pixhawk, saves it to a file, and sends it via serial. """


        # note: there are a total of 16 different types of messages
        # comment out the messages you don't need
        rec_gps = self.pixhawk_conn.recv_match(type='GPS_RAW_INT', blocking=True)
        rec_att = self.pixhawk_conn.recv_match(type='ATTITUDE', blocking=True)
        rec_vfr = self.pixhawk_conn.recv_match(type='VFR_HUD', blocking=True)
        rec_wpc = self.pixhawk_conn.recv_match(type='MISSION_CURRENT', blocking=True)
        # SYS=pixconn.recv_match(type='SYS_STATUS',blocking=True)

        # make sure we got a good message before proceeding
        if rec_gps and rec_att and rec_vfr and rec_wpc:
            # Concatenate the messages into one string to send
            gps_sum = "TIME " + str(rec_gps.time_usec) + " LAT " + str(rec_gps.lat / 1.0e7) + " LON " + \
                      str(rec_gps.lon / 1.0e7) + " ALT " + str(rec_gps.alt / 1.0e3) + " SATVIS " + \
                      str(rec_gps.satellites_visible)

            att_sum = "ROLL " + "{0:.3f}".format(rec_att.roll) + " PITCH " + "{0:.3f}".format(
                rec_att.pitch) + " YAW " + "{0:.3f}".format(rec_att.yaw)

            vfr_sum = "SP " + str(rec_vfr.groundspeed) + " HD " + str(rec_vfr.heading) + " TH " + str(rec_vfr.throttle)

            wpc_sum = "CURWP " + str(rec_wpc.seq)

            status = gps_sum + " " + att_sum + " " + vfr_sum + " " + wpc_sum

            # add metadata info to the message
            meta_message = self.meta_message(status, 'stats')

        else:
            status = 'ERROR: No message received.'
            # add metadata info to the message
            meta_message = self.meta_message(status, 'error')

        # save the message to a local file
        self.save_message(meta_message)

        # send the message via serial
        self.queue_outbound_modem_message_signal.emit(meta_message, 5)

    def meta_message(self, message, message_type):
        """ adds metadata to a message """
        # Get the kayak name
        kayak_name = self.settings.value("kayak/name").toString()

        # current date and time
        now = datetime.now().strftime('%Y/%m/%d %H:%M:%S UTC')

        # message
        # NOTE: The message must start with the kayak name (e.g. kayak1)
        # so that the receiving code on the PC recognizes it.
        return kayak_name + " -- " + message_type + " -- " + now + " -- " + message

    @staticmethod
    def save_message(message):
        """ saves a message to a file """

        # date
        full_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-2]
        full_date += "Z"

        short_date = datetime.now().strftime("%Y%m%dT%H")

        # create the filename
        if not os.path.exists(message_save_path):
            os.makedirs(message_save_path)

        outfile = message_save_path + '/kayak_' + short_date + '.txt'

        # open and write to the file
        f = open(outfile, 'a')
        f.write(full_date + ' ' + message + '\n')
        f.close()

    def set_waypoints(self, command):
        """ sets new waypoints on the Pixhawk  """
        # set some parameters
        seq = 1  # the waypoint sequence number
        frame = mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT
        radius = 6

        # create the waypoint object
        wp = mavwp.MAVWPLoader()

        # add the first (dummy) waypoint - the PixHawk ignores this anyway
        # and sets it to the current (home) location
        # wp.add(mavutil.mavlink.MAVLink_mission_item_message(pixconn.target_system, pixconn.target_component, seq, frame, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0,1,0,radius,0,0,0,0,0))
        wp.add(mavutil.mavlink.MAVLink_mission_item_message(self.pixhawk_conn.target_system,
                                                            self.pixhawk_conn.target_component, seq, frame,
                                                            mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 1, 1, 0, radius, 0, 0,
                                                            0, 0, 0))
        # pull out the lats and lons from the command
        latlonlist = command.split(' ')

        # next desired waypoint
        nextwp = int(latlonlist[1]);

        # add each waypoint to the waypoint list
        for i in range(2, len(latlonlist)):
            seq += 1

            # separate the lat and lon values
            latlon = latlonlist[i].split(',')
            lat = float(latlon[0])
            lon = float(latlon[1])
            self.logger.debug('Adding waypoint ' + str(i - 1) + ' : LAT ' + str(lat) + ' LON ' + str(lon) + "to list.")

            # set the current waypoint (the next to visit)
            if i == 1 + nextwp:
                curr = 1
            else:
                curr = 0

                # add each of the waypoints to wp
            # wp.add(mavutil.mavlink.MAVLink_mission_item_message(pixconn.target_system, pixconn.target_component, seq, frame, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, curr,1,0,radius,0,0,lat,lon,0))
            wp.add(mavutil.mavlink.MAVLink_mission_item_message(self.pixhawk_conn.target_system, self.pixhawk_conn.target_component, seq, frame,
                                                                mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 1, 0, radius, 0, 0,
                                                                lat, lon, 0))

        # delete all of the existing waypoints
        self.logger.debug('Clearing pixhawk waypoints ...')
        self.pixhawk_conn.waypoint_clear_all_send()

        # tell the pixhawk how many waypoints to expect
        self.logger.debug('Sending ' + str(wp.count()) + ' waypoints to pixhawk.')

        self.pixhawk_conn.waypoint_count_send(wp.count())

        # send each of the waypoints when requested
        # for i in range(wp.count()):
        seq = 0
        #while seq < wp.count() - 1:
        for i in range(wp.count()):
            msg = self.pixhawk_conn.recv_match(type=['MISSION_REQUEST'], blocking=True)
            self.pixhawk_conn.mav.send(wp.wp(msg.seq))
            self.logger.debug('Sending waypoint {0}'.format(msg.seq))

        self.logger.debug('Completed ... waypoints set.')

    def list_waypoints(self):
        """ lists the waypoints on the Pixhawk  """
        listwp_priority = self.settings.value("priorities/listwp").toInt()[0]

        # request the number of waypoints
        self.pixhawk_conn.waypoint_request_list_send()
        countmsg = self.pixhawk_conn.recv_match(type='MISSION_COUNT', blocking=True, timeout=5)

        wcounter = 0
        while countmsg is None:
            wcounter += 1
            time.sleep(0.5)
            if wcounter == 5:
                msg = 'ERROR: Number of waypoints unknown.'
                metamessage = self.meta_message(msg, 'error')
                self.queue_outbound_modem_message_signal.emit(metamessage, listwp_priority)
                return
            else:
                self.logger.debug('Re-requesting the total number of waypoints ...')
                self.pixhawk_conn.waypoint_request_list_send()
                countmsg = self.pixhawk_conn.recv_match(type='MISSION_COUNT', blocking=True, timeout=5)

        # print str(countmsg)
        wpcount = countmsg.count
        msg = 'WPCOUNT ' + str(wpcount)
        metamessage = self.meta_message(msg, 'wpcount')
        self.queue_outbound_modem_message_signal.emit(metamessage, listwp_priority)

        # find the current waypoint number
        wpcurrent = self.pixhawk_conn.recv_match(type=['MISSION_CURRENT'], blocking=True)
        msg = 'CURWP ' + str(wpcurrent.seq)
        metamessage = self.meta_message(msg, 'curwp')
        self.queue_outbound_modem_message_signal.emit(metamessage, listwp_priority)

        # get the info for each waypoint
        for seq in range(wpcount):
            # print 'Waypoint ' + str(seq)
            self.pixhawk_conn.waypoint_request_send(seq)

            wcounter = 0;
            msg = None
            while msg is None:
                wcounter = wcounter + 1;
                if wcounter == 10:
                    msg = 'ERROR: Waypoint ' + str(seq) + ' not returned.'
                    metamessage = self.meta_message(msg, 'error')
                    self.queue_outbound_modem_message_signal.emit(metamessage, listwp_priority)
                    break
                else:
                    msg = self.pixhawk_conn.recv_match(type='MISSION_ITEM', blocking=True, timeout=5)
                    time.sleep(0.5)

            this_wp = "WP " + str(msg.seq) + " LAT " + str(msg.x) + " LON " + str(msg.y) + " CUR " + str(msg.current)
            metamessage = self.meta_message(this_wp, 'waypt')
            self.queue_outbound_modem_message_signal.emit(metamessage, listwp_priority)

    def set_mode(pixconn, command):
        """ sets the mode on the Pixhawk  """

        # print 'Setting the mode ...'

        # list of acceptable input arguments
        inputs = ["MANUAL", "HOLD", "AUTO", "GUIDED", "LEARNING", "RTL", "INITIALISING", "STEERING"]

        # pull out the desired mode from the command
        modesplit = command.split(' ')
        mode = modesplit[1].upper()

        # continue only if an acceptable argument was received
        if mode in inputs:
            # print 'Setting mode to ' + mode

            # set the mode
            if mode == 'MANUAL':
                pixconn.set_mode_manual()
            elif mode == 'AUTO':
                pixconn.set_mode_auto()
            elif mode == 'RTL':
                pixconn.set_mode_rtl()
            elif mode == 'HOLD':
                pixconn.set_mode('HOLD')
            elif mode == 'INITIALISING':
                pixconn.set_mode('INITIALISING')
            elif mode == 'STEERING':
                pixconn.set_mode('STEERING')
            elif mode == 'GUIDED':
                pixconn.set_mode('GUIDED')
            elif mode == 'LEARNING':
                pixconn.set_mode('LEARNING')

            # print 'Mode set.'

        else:
            pass
            # print 'Mode unknown: ' + mode

    def get_parameter(self, command):
        """ gets a parameter value from the Pixhawk possible parameters include WP_RADIUS """

        getparam_priority = self.settings.value("priorities/getparam").toInt()[0]

        # pull out the parameter and value from the command
        commandsplit = command.split(' ')
        param = commandsplit[1].upper()
        aparam = param.encode('ascii')

        # read the value
        notreceived = 1;
        while notreceived < 10:
            try:
                self.pixhawk_conn.mav.param_request_read_send(self.pixhawk_conn.target_system,
                                                              self.pixhawk_conn.target_component, aparam, -1)

                value = self.pixhawk_conn.recv_match(type='PARAM_VALUE', blocking=True)
                gotvalue = str(value.param_value)
                self.logger.debug(param + ' value: ' + gotvalue)
                msg = param + ' ' + gotvalue
                mm = self.meta_message(msg, 'param')
                self.queue_outbound_modem_message_signal.emit(mm, getparam_priority)
                notreceived = 101
            except Exception:
                time.sleep(0.5)
                notreceived = notreceived + 1

        if notreceived == 10:
                self.logger.debug('Parameter not returned.')

    def set_parameter(self, command):
        """ sets a parameter on the Pixhawk possible parameters include WP_RADIUS """

        # pull out the parameter and value from the command
        commandsplit = command.split(' ')
        param = commandsplit[1].upper()
        value = commandsplit[2]
        aparam = param.encode('ascii')

        # set the parameter value
        p1 = mavparm.MAVParmDict()
        p1.mavset(self.pixhawk_conn, aparam, value)

        self.logger.debug('Parameter set.')

    def set_current_waypoint(self, command):
        """ sets the current waypoint on the Pixhawk  """

        # pull out the waypoint number from the command
        commandlist = command.split(' ')
        curwp = int(commandlist[1]);

        self.logger.debug('Next waypoint to visit : ' + str(curwp))
        self.pixhawk_conn.waypoint_set_current_send(curwp)

    def on_queue_pixhawk_message_signal_slot(self, message, priority):
        self.inbound_message_queue[priority].append(str(message))


