"""
    This file contains the KayakSettings Class
    This class handles instantiating the settings system for the Kayak
    It also imports the config files that are easy to edit
    Lastly, it initializes default settings if no settings files exist
"""

__author__ = ["Jasmine Nahorniak", "Corwin Perren"]
__credits__ = [""]
__email__ = ["jasmine@coas.oregonstate.edu", "caperren@caperren.com"]


#####################################
# Imports
#####################################
# Python native imports
from PyQt4 import QtCore
import re
import logging
import os

# Custom imports

#####################################
# Global Variables
#####################################
config_file_path = os.path.expanduser("~/kayak/kayak_config.txt")


#####################################
# PickAndPlateLogger Definition
#####################################
class KayakSettings(QtCore.QObject):
    def __init__(self):
        QtCore.QObject.__init__(self)

        # ########## Get an instance of the logger ##########
        self.logger = logging.getLogger("Kayak")

        # ########## Set up settings for program ##########
        self.initialize_settings()

        # ########## Create Instance of settings ##########
        self.settings = QtCore.QSettings()

        # ########## Load settings, overwriting with defaults if no settings exist ##########
        self.load_settings_or_overwrite_with_defaults()

    @staticmethod
    def initialize_settings():
        QtCore.QCoreApplication.setOrganizationName("OSU COAS")
        QtCore.QCoreApplication.setOrganizationDomain("http://ceoas.oregonstate.edu/")
        QtCore.QCoreApplication.setApplicationName("Kayak")

    def load_settings_or_overwrite_with_defaults(self):
        # ########## Import existing settings or override with defaults ##########
        self.settings.setValue("kayak/name", self.settings.value("kayak/name", "kayak1").toString())

        self.settings.setValue("pixhawk/port", self.settings.value("pixhawk/port", "/dev/ttyPixHawk").toString())
        self.settings.setValue("pixhawk/baud", self.settings.value("pixhawk/baud", 57600).toInt()[0])

        self.settings.setValue("modem/port", self.settings.value("modem/port", "/dev/ttyModem").toString())
        self.settings.setValue("modem/baud", self.settings.value("modem/baud", 57600).toInt()[0])

        self.settings.setValue("winch/port", self.settings.value("winch/port", "/dev/ttyWinch").toString())
        self.settings.setValue("winch/baud", self.settings.value("winch/baud", 57600).toInt()[0])

        # Priority settings
        self.settings.setValue("priorities/num_priorities",
                               self.settings.value("priorities/num_priorities", 10).toInt()[0])
        self.settings.setValue("priorities/listwp", self.settings.value("priorities/listwp", 5).toInt()[0])
        self.settings.setValue("priorities/getparam", self.settings.value("priorities/getparam", 5).toInt()[0])

        # ########## Overwrite settings with config file values ##########
        if os.path.isfile(config_file_path):
            self.settings.setValue("kayak/name", self.read_config('kayakname'))

            self.settings.setValue("pixhawk/port", self.read_config('pix_port'))
            self.settings.setValue("pixhawk/baud", self.read_config('pix_baud'))

            self.settings.setValue("modem/port", self.read_config('mod_port'))
            self.settings.setValue("modem/baud", self.read_config('mod_baud'))

            self.settings.setValue("winch/port", self.read_config('win_port'))
            self.settings.setValue("winch/baud", self.read_config('win_baud'))

            self.settings.setValue("priorities/num_priorities", self.read_config('number_of_priority_settings'))
            self.settings.setValue("priorities/listwp", self.read_config('listwp_outgoing_priority'))
            self.settings.setValue("priorities/getparam", self.read_config('getparam_outgoing_priority'))

        self.settings.sync()

    @staticmethod
    def read_config(type_string):
        """ Returns the relevant info from the config file. Type needs to be one of the parameters listed in the config
        file, such as portlabel or baudrate"""

        config_file = open(config_file_path, 'r')

        match = None

        for line in config_file:
            matches = re.match('^' + type_string + ':(.+)', line)
            if matches:
                match = matches.group(1)

        config_file.close()

        return match
