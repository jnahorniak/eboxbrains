"""
    This file contains the KayakLogger Class
    This class handles setting up the central logging system information for the QT instance
"""

__author__ = ["Jasmine Nahorniak", "Corwin Perren"]
__credits__ = [""]
__email__ = ["jasmine@coas.oregonstate.edu", "caperren@caperren.com"]


#####################################
# Imports
#####################################
# Python native imports
from PyQt4 import QtCore
import logging
from os import makedirs
from os.path import expanduser, exists
import datetime

# Custom imports

#####################################
# Global Variables
#####################################
current_date = str(datetime.date.today())
application_hidden_path_root = expanduser("~") + "/.kayak"  # Change /.kayak_logs to whatever the short program name is
application_logging_path = application_hidden_path_root + "/logs"
application_log_full_path = application_logging_path + "/" + current_date + ".txt"


#####################################
# KayakLogger Class Definition
#####################################
class KayakLogger(QtCore.QObject):
    def __init__(self, console_output=True):
        QtCore.QObject.__init__(self)

        # ########## Local class variables ##########
        self.console_output = console_output

        # ########## Get the Pick And Plate instance of the logger ##########
        self.make_logging_paths_if_nonexistent()
        self.logger = logging.getLogger("Kayak")
        self.logger_file = open(application_log_full_path, 'a')

        # ########## Set up logger with desired settings ##########
        self.setup_logger()

        # ########## Place divider in log file to see new program launch ##########
        self.add_startup_log_buffer_text()

    @staticmethod
    def make_logging_paths_if_nonexistent():
        if not exists(application_logging_path):
            makedirs(application_logging_path)

    def setup_logger(self):
        self.logger.setLevel(logging.DEBUG)

        # Modify this line to change how the data looks when logged
        formatter = logging.Formatter(fmt='%(levelname)s : %(asctime)s : %(threadName)s : %(message)s',
                                      datefmt='%m/%d/%y %H:%M:%S')

        file_handler = logging.FileHandler(filename=application_log_full_path)  # This creates a file handler
        file_handler.setFormatter(formatter)  # This tells the logger instance to use our custom format
        file_handler.setLevel(logging.DEBUG)  # This sets the logger to log anything from DEBUG or worse
        self.logger.addHandler(file_handler)  # This tells the logger to use our custom file handler

        # If console logging is set to true, print the data being logged to a file to the console as well
        if self.console_output:
            console_handler = logging.StreamHandler()
            console_handler.setFormatter(formatter)
            console_handler.setLevel(logging.DEBUG)
            self.logger.addHandler(console_handler)

    def add_startup_log_buffer_text(self):
        self.logger_file.write("\n########## New Instance of Application Started ##########\n\n")
        self.logger_file.close()

# TODO: Add in logging cleanup so we don't run out of space over time
