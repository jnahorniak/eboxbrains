#!/usr/bin/env python3
import sys
import os
import time
import re
import threading
import datetime
import numpy as np
from struct import pack, unpack
from glob import glob

BASEDIR = '/home/ross/data/'
NENS = int(sys.argv[1])

class ROSEMonitor:

    ##################################################################
    # Define some variables and set up initialization
    ##################################################################

    # Define filename patterns per instrument
    fileFormat = {"GPS": "GPS*.log", "ADCP": "ADCP_raw*.bin"}

    def __init__(self, baseDir, nEns, hOffset):
        # Init function - locate folders/files etc
        self.nEns = nEns
        self.hOffset = hOffset
        self.ensemblesFound = 0

        print("Initializing ROSEMonitor in " + baseDir)
        print("Averaging interval: {} ensembles".format(self.nEns))

        # Find newest deployment folder and files within
        deployments = glob(os.path.join(baseDir, "*"))
        deployment = max(deployments, key=os.path.getctime)
        print("Latest deployment: " + deployment)

        # Initialize instrument output file variables
        insts = ["GPS", "ADCP"]
        self.dirs = {k: os.path.join(baseDir, deployment, k) for k in insts}
        self.fname = {k: None for k in insts}
        self.files = {k: None for k in insts}
        self.fnamePrev = {k: None for k in insts}
        self.newFile = {k: False for k in insts}
        self.summaryFile = os.path.join(deployment, 'ADCP', 'adcp_summary.txt')
        self.latestSummaryFile = os.path.join(deployment, 'ADCP', 'adcp_latest.txt')

        # Find and open latest files
        print("Waiting for initial instrument files...")
        while not all(v for v in self.files.values()):
            self.getLatestFile(insts)
            time.sleep(1.0)
        self.newFile = {k: False for k in insts}

        # Skip to end of files on startup - disable for testing to read from
        # the beginning of instrument files
        for inst in ["GPS", "ADCP"]:
            self.files[inst].seek(0, 2)
        print("Skipped to end of datafiles")

        # Initialize gps state variables
        self.gpsTime = "YYYYMMDDhhmmss"
        self.gpsHeading = 0
        self.gpsVel = [0, 0]

        # Define some constants
        self.D2R = np.pi/180

    ##################################################################
    # Functions for tracking latest files
    ##################################################################

    def getLatestFile(self, insts):
        # Monitor new instrument files and update file objects
        readMode = {"GPS": "r", "ADCP": "rb"}
        for inst in insts:
            fnames = glob(os.path.join(self.dirs[inst],
                          ROSEMonitor.fileFormat[inst]))
            if len(fnames) > 0:
                self.fname[inst] = max(fnames, key=os.path.getctime)
                if self.fname[inst] != self.fnamePrev[inst]:
                    print("New " + inst + " file detected: " +
                          os.path.basename(self.fname[inst]))
                    self.files[inst] = open(self.fname[inst], readMode[inst])
                    self.fnamePrev[inst] = self.fname[inst]
                    self.newFile[inst] = True

    ##################################################################
    # Functions for reading latest GPS information
    ##################################################################

    def updateGPS(self,BUFSIZE=128):
        # Read GPS log in reverse without loading the whole thing into memory.
        # Read until a GPRMC and HEHDT line are found and update GPS state

        # Define regular expressions for parsing GPS data
        dec = "[-+]?\d+\.?\d+"
        nmea_re = {"GPRMC": re.compile("\$GPRMC,(\d{2})(\d{2})(\d{2}\.\d{2}),.," + \
                                       dec + ",[NS]," + dec + ",[EW]," + "(" + dec + ")," + \
                                       "(" + dec + ")," + "(\d{2})(\d{2})(\d{2}).*"),
                   "GPVTG": re.compile("\$GPVTG,(" + dec + "),T," + dec + ",M," + "(" + dec + "),.*"),
                   "HEHDT": re.compile("\$HEHDT,(" + dec + "),.*")}
        fldnames = {"GPRMC": ('hour', 'min', 'sec', 'speed', 'course', 'day', 'month', 'year'),
                    "GPVTG": ('course', 'speed'),
                    "HEHDT": ('heading',)}

        # Start reading file
        found = {"HEHDT": False, "GPVTG": False}
        self.files["GPS"].seek(0, 2)
        p = self.files["GPS"].tell()
        remainder = ""
        while not all(v for v in found.values()):
            sz = min(BUFSIZE, p)
            p -= sz
            self.files["GPS"].seek(p)
            buf = self.files["GPS"].read(sz) + remainder
            if '\n' not in buf:
                remainder = buf
            else:
                i = buf.index('\n')
                for L in buf[i+1:].split("\n")[::-1]:
                    if not found["HEHDT"] and nmea_re["HEHDT"].match(L):
                        flds = dict(zip(fldnames["HEHDT"],nmea_re["HEHDT"].match(L).groups()))
                        self.gpsHeading = float(flds['heading'])
                        found["HEHDT"] = True
                    elif not found["GPVTG"] and nmea_re["GPVTG"].match(L):
                        flds = dict(zip(fldnames["GPVTG"],nmea_re["GPVTG"].match(L).groups()))
                        spd = float(flds['speed']) * 0.514444              # convert knots to m/s
                        th = (90.0 - float(flds['course']))%360 * np.pi/180  # convert true course to cartesian radians
                        self.gpsVel = [spd*np.cos(th), spd*np.sin(th)]
                        found["GPVTG"] = True
                remainder = buf[:i]
            if p == 0:
                break

    ##################################################################
    # Functions for handling ADCP data
    ##################################################################

    def adcpBytes(self):
    # Generator for looping over ADCP bytes
        while True:
            b = self.files["ADCP"].read(1)
            if not b:
                # Wait, then check for a new file
                time.sleep(0.1)
                self.getLatestFile(["ADCP"])
                continue
            yield b

    def nextAdcpBytes(self,n):
    # When we encounter an ADCP header, we'll want to read a certain number of bytes
        while n>0:
            b = self.files["ADCP"].read(1)
            if not b:
                # Wait, then check for a new file
                time.sleep(0.1)
                self.getLatestFile(["ADCP"])
                continue
            n -= 1
            yield b

    def isValidEnsemble(self,ens):
    # Compute an ensemble's checksum and check against output checksum.
    # Checksum is [sum of data] % 65535.
        chk_recorded = unpack("H",ens["checksum"])[0]
        chk_computed = (sum(ens["data"]) % 65535)
        # The Sentinel and Workhorse seem to have slightly different checksum
        # formats... The difference between the recorded and computed checksums
        # should be 0 (Workhorse) or 1 (Sentinel), so the check below flags
        # either as valid.
        return abs(chk_recorded - chk_computed) < 2

    def nextEnsemble(self,n):
    # Return the next n ADCP data ensembles
        hBytes = bytearray([0,0])
        while n > 0:
            # Start scanning ADCP bytes for ADCP headers
            for b in self.adcpBytes():
                hBytes[1] = hBytes[0]
                hBytes[0] = int.from_bytes(b,byteorder="little")
                # If we find a header:
                if (hBytes[0] == hBytes[1] == 127):

                    # Save current position
                    pos = self.files["ADCP"].tell()
                    # print('Found header')
                    # print(pos)

                    # Get latest GPS information and store with ADCP ensemble
                    ens = {}
                    self.updateGPS()
                    if self.gpsTime == "YYYYMMDDhhmmss":
                        ens["gpsTime"] = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
                    else:
                        ens["gpsTime"] = self.gpsTime
                    ens["gpsHeading"] = self.gpsHeading
                    ens["gpsVel"] = self.gpsVel

                    # extract number of bytes
                    nbBytes = b''.join([b for b in self.nextAdcpBytes(2)])
                    # print(nbBytes)
                    nb = unpack("<h",nbBytes)
                    ens["nb"] = nb
                    # print(nb)
                    # Read the rest of the ensemble: (nb - 4 becase we've
                    # already read header & nbytes). Attach the header to the
                    # beginning so we can verify the checksum.
                    header = b'\x7f\x7f'
                    ens["data"] = b''.join([header,nbBytes, \
                                            b''.join([b for b in self.nextAdcpBytes(nb[0]-4)])])
                    # read the two-byte checksum
                    ens["checksum"] = b''.join([b for b in self.nextAdcpBytes(2)])

                    if self.isValidEnsemble(ens):
                        # If the checksum is valid, return this ensemble and continue scanning
                        n -= 1
                        # go to just before the end of the ensemble before continuing the search
                        # seems to be a 1 byte offset somewhere in the code; this corrects for that
                        self.files["ADCP"].seek(pos+nb[0]-5,0)
                        # print('Good checksum')
                        yield ens
                        break
                    elif self.newFile["ADCP"]:
                        # Otherwise, if we have a new ADCP file, resume from its beginning
                        self.files["ADCP"].seek(0,0)
                        self.newFile["ADCP"] = False
                        # print('New file')
                    else:
                        # Otherwise, continue scanning from just after what we thought was a valid header
                        self.files["ADCP"].seek(pos,0)
                        # print('Failed checksum')

    def parseEnsemble(self,ens):
    # Turn raw ADCP ensemble data into something usable
        nCells = 0
        nBeams = 0

        # Define data type prefixes, start bytes, lengths, and data types. We care
        # about 3 different data prefixes:
        # - 00 00: Fixed leader. This contains information about beam/bin configuration.
        # - 80 00: Variable leader. This contains pitch, roll.
        # - 00 01: Velocity.
        #
        # The start byte, end byte, and data types are defined below for each
        # field of interest.
        dataFields = {b'\x00\x00': {"nBeams":    {"start":  8, "end":  9, "type": "B"},
                                    "nCells":    {"start":  9, "end": 10, "type": "B"},
                                    "binLen":    {"start": 12, "end": 14, "type": "h"},
                                    "bin1dist":  {"start": 32, "end": 34, "type": "h"},
                                    "beamangle": {"start": 58, "end": 59, "type": "B"}},
                      b'\x80\x00': {"num":   {"start":  2, "end":  4, "type": "H"},
                                    "head":  {"start": 18, "end": 20, "type": "h"},
                                    "tilt1": {"start": 20, "end": 22, "type": "H"},
                                    "tilt2": {"start": 22, "end": 24, "type": "H"}},
                      b'\x00\x01': {"vel": {"start": 2, "end": None, "type": None}}}
        # Note: start and end bytes for velocity are left undefined because
        # these depend on information in the fixed leader data which we will
        # read soon.

        # Compute the byte offset of each data type
        nDataTypes = ens["data"][5] # This byte tells us how many data types we have
        offsets = [unpack("<H",ens["data"][6+2*nd:8+2*nd])[0] for nd in range(nDataTypes)]
        offsets.append(-1) # The last data type ends at the last byte

        # Loop over data types and extract fields we care about. We'll always
        # read a fixed leader first, allowing us to re-define the start/end
        # bytes for velocity data.
        pEns = {}
        pEns["gpsTime"] = ens["gpsTime"]
        pEns["gpsHeading"] = ens["gpsHeading"]
        pEns["gpsVel"] = ens["gpsVel"]

        for nd in range(nDataTypes):
            data = ens["data"][offsets[nd]:offsets[nd+1]]
            dataType = data[0:2]
            if dataType in dataFields.keys():
                for fld in dataFields[dataType].keys():
                    pEns[fld] = unpack("<" + dataFields[dataType][fld]["type"],
                                       data[dataFields[dataType][fld]["start"]:\
                                            dataFields[dataType][fld]["end"]])

            # Define vel byte locations after reading the fixed leader for bin/beam
            # configuration.
            if nCells==0 and "nCells" in pEns.keys():
                nCells = pEns["nCells"][0]
                nBeams = pEns["nBeams"][0]
                dataFields[b'\x00\x01'] = {"vel": {"start": 2,
                                                   "end": 3+2*nBeams*nCells,
                                                   "type": "h"*nBeams*nCells}}

        # Now that we've read the data into tuples, get them into nice formats for
        # doing COOL MATH STUFF.
        # Note: I'm ignoring the Sentinel V's 5th beam for now...

        # Convert velocity tuple into matrix. Cells x Beams
        vel = np.reshape(np.matrix(pEns["vel"]), (nCells,4))/1000

        # Convert tilt1 & tilt2 to pitch and roll
        t1 = pEns["tilt1"][0]/100 * self.D2R
        t2 = pEns["tilt2"][0]/100 * self.D2R
        pr = np.arctan(np.tan(t1) * np.cos(t2)) # pitch, radians
        rr = t2 # roll, radians
        hr = (pEns["gpsHeading"] + self.hOffset) * self.D2R

        # Build orientation correction matrices
        rotH = np.matrix([[+np.cos(hr), +np.sin(hr), 0],
                          [-np.sin(hr), +np.cos(hr), 0],
                          [ 0,           0,          1]])

        rotP = np.matrix([[1,           0,           0],
                          [0, +np.cos(pr), -np.sin(pr)],
                          [0, +np.sin(pr), +np.cos(pr)]])

        rotR = np.matrix([[+np.cos(rr), 0, +np.sin(rr)],
                          [0,           1,           0],
                          [-np.sin(rr), 0, +np.cos(rr)]])

        # Build beam to instrument rotation matrix
        beamangle = np.pi/180 * pEns["beamangle"][0]
        b = 1/(4*np.cos(beamangle))
        a = 1/(2*np.sin(beamangle))
        b2i = np.matrix([[a, -a, 0, 0], # x
                         [0, 0, -a, a], # y
                         [b, b, b, b]]) # z

        # Rotate beam velocities to earth coordinates
        vel = (rotH*rotP*rotR*b2i*vel.transpose()).transpose()

        # Remove vessel velocity
        vel[:,0] = vel[:,0] + self.gpsVel[0]
        vel[:,1] = vel[:,1] + self.gpsVel[1]

        # Average using 4 depth bins
        maxBin = int(np.floor(nCells/4))*4  # truncate some bins for averaging into groups of 4 bins
        pEns["vel"] = np.array((vel[0:maxBin:4] + vel[1:maxBin:4] + vel[2:maxBin:4] + vel[3:maxBin:4])/4.0)
        # print("Processed ensemble {}".format(pEns["num"][0]))

        return pEns

    def writeAdcpSummaries(self):
        while True:
            ens = [self.parseEnsemble(e) for e in self.nextEnsemble(self.nEns)]
            vels = [e["vel"] for e in ens]
            velAvg = np.mean(np.stack(vels,axis=2), axis=2)
            print(ens[-1]["gpsTime"] +
                  ",u," + ','.join("{:02.4f}".format(u) for u in velAvg[:,0]) + "," +
                  "v," + ','.join("{:02.4f}".format(v) for v in velAvg[:,1]) + "," +
                  "w," + ','.join("{:02.4f}".format(w) for w in velAvg[:,2]) + "," +
                  "h,{:02.2f}".format(ens[-1]["gpsHeading"]) + "," +
                  "p,{:d}".format(ens[-1]["num"][0]) + "," +
                  "vx,{:02.2f}".format(ens[-1]["gpsVel"][0]) + "," +
                  "vy,{:02.2f}".format(ens[-1]["gpsVel"][1]),
                  file=open(self.summaryFile,"a"))
            print(ens[-1]["gpsTime"] +
                  ",u," + ','.join("{:02.4f}".format(u) for u in velAvg[:,0]) + "," +
                  "v," + ','.join("{:02.4f}".format(v) for v in velAvg[:,1]) + "," +
                  "w," + ','.join("{:02.4f}".format(w) for w in velAvg[:,2]),
                  file=open(self.latestSummaryFile,"w"))
            print("Averaged ensembles {} to {}".format(ens[0]["num"][0],ens[-1]["num"][0]))
            # print("Averaged {} ensembles between {} {}".format(self.nEns, str(ens[0]["gpsTime"]), str(ens[-1]["gpsTime"])))

    def logGPS(self):
        while True:
            self.updateGPS()
            print("h,{:02.2f}".format(self.gpsHeading) +
                  "," + "u,{:02.2f}".format(self.gpsVel[0]) +
                  "," + "v,{:02.2f}".format(self.gpsVel[1]))
            time.sleep(1)



    ##################################################################
    # Main function for running everything in separate threads
    ##################################################################
    def run(self):
        self.writeAdcpSummaries() # parse and output ADCP data in chunks

Mon = ROSEMonitor(baseDir=BASEDIR,nEns=NENS,hOffset=45.0)
Mon.run()
